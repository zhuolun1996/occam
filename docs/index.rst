.. Occam documentation master file, created by
   sphinx-quickstart on Mon Feb  6 01:50:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Occam's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Code Modules
============

.. include:: modules.rst

.. include:: _templates/footer.rst
