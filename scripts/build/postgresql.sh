#!/bin/bash
set -eu

mkdir -p ./vendor

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:${PATH-}

pkgver=11.5

cd vendor

if [ ! -d postgresql-${pkgver} ]; then
	wget https://ftp.postgresql.org/pub/source/v${pkgver}/postgresql-${pkgver}.tar.bz2
	tar xvf postgresql-${pkgver}.tar.bz2
	cd postgresql-${pkgver}
	./configure --prefix=$PWD/.. --mandir=$PWD/../share/man --datadir=$PWD/../share/postgresql --sysconfdir=$PWD/../etc --with-gssapi --with-libxml --with-system-tzdata=/usr/share/zoneinfo --with-openssl --with-pam --with-system---with-uuid=e2fs --with-icu --enable-nls --enable-thread-safety --disable-rpath
	make world
  make install
	cd ..
fi

cd ..

# Initialize the database under the current user
./vendor/bin/initdb -D ./vendor/db --locale=en_US.UTF-8 -E UTF8
