#!/bin/bash
set -eu

mkdir -p ./vendor

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:${PATH-}

cd vendor

if [ ! -d Python-3.6.3 ]; then
	wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz
	tar xvf Python-3.6.3.tgz
	cd Python-3.6.3
	./configure --with-ensurepip=install --prefix=$PWD/.. --enable-loadable-sqlite-extensions LDFLAGS="-L$PWD/../lib" CPPFLAGS="-I$PWD/../include"
	make -j4
	make install
	cd ..
fi

cd ..
