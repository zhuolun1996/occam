#!/bin/bash
set -eu

mkdir -p ./vendor

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:$PWD/vendor/go/bin:${PATH-}

cd vendor

if [ ! -d sqlite-src-3200100 ]; then
  wget https://occam.software/QmWEv2RQ2ZPFNY9a4fizHcyXgHkYeN7VsnLwnk9sjTyiKf/5dtzd5SxHqWcFnEV14X4dVpUidLj1F/raw/sqlite-src-3200100.zip
  unzip sqlite-src-3200100.zip
	cd sqlite-src-3200100
	./configure --prefix=$PWD/.. --disable-tcl
	make -j4
	make install
	cd ..
fi

cd ..
