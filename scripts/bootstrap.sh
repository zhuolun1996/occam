#!/bin/bash

# Discover a known node
./bin/occam nodes discover occam.software

# Pull something... (Vicki's presentation)
./bin/occam objects pull QmVkLCL14jaZyKS7wWxs146XTJkCPTGCwDiQLFdprFAAp5 -f

# Pull a PDF viewer... (PDF.js)
./bin/occam objects pull QmNooP5Lxn3jGbFvUtMzphJeTBUykVix9SDSSkT8rzeNt1 -f

# Pull a text editor... (Ace Editor)
./bin/occam objects pull QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g -f
