# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import os
import json

from occam.log            import loggable
from occam.git_repository import GitRepository
from occam.object         import Object
from occam.config         import Config

from occam.manager import uses, manager

from occam.network.manager  import NetworkManager
from occam.storage.manager  import StorageManager
from occam.backends.manager      import BackendManager

from urllib.parse import urlparse

@loggable
@uses(NetworkManager)
@uses(StorageManager)
@uses(BackendManager)
@manager("nodes")
class NodeManager:
  """
  This OCCAM manager handles the Node listing, Node discovery, and Node lookup.
  """

  CERTIFICATE_FILENAME = 'ssl.crt'

  def __init__(self):
    """
    Initialize the node manager.
    """

  def partialFromURL(self, url):
    """
    This method will take a fully realized URL to a resource on an OCCAM node
    and return the canonical name (domain,port) for that node. Port will be
    omitted when it is a normal port (80, 443)
    """
    if url is None:
      url = ""

    # Make sure it *is* a full URL
    url = self.urlFromPartial(url)

    # urlparse works great on real URLs
    urlparts = urlparse(url)

    # we want the host:port (unless port is 80 or 443)
    url = urlparts.hostname

    if urlparts.port and urlparts.port != 80 and urlparts.port != 443:
      url = url + "," + str(urlparts.port)

    return url

  def urlFromPartial(self, url, scheme="https"):
    """
    This method will return a full url from any partial url given as a Node uri.

    You can give the method a 'scheme' which will be applied to the URL if it
    does not have a scheme already.
    """
    if url is None:
      url = ""
    urlparts = urlparse(url)
    if urlparts.scheme == "":
      url = "%s://%s" % (scheme, url)
      urlparts = urlparse(url)

    if urlparts.netloc == "":
      return None

    return url

  def hostAndPortFromURL(self, url):
    """ Returns a pair representing the host and pair respectively.
    """
    urlparts = urlparse(url)
    host = urlparts.netloc.split(':')[0]

    port = "80"

    if ':' in urlparts.netloc:
      port = urlparts.netloc.split(':')[1]
    elif urlparts.scheme == "https":
      port = "443"

    return [host, port]

  def search(self, url):
    """ Returns the database entry for the given node url.
    """

    url = self.urlFromPartial(url)
    host, port = self.hostAndPortFromURL(url)

    return self.datastore.retrieve(url, host, port)

  def retrieveInfo(self, url):
    """ Polls and retrieves the node info from the Node.
    """
    import pdb
    # pdb.set_trace()
    url = self.urlFromPartial(url)
    nodeInfoURL = url + "/system"

    # Get certificate
    if not self.retrieveCertificate(url):
      return None

    cert = self.certificatePathFor(url)

    # Query the url and retrieve node information
    try:
      node_info = self.network.getJSON(nodeInfoURL, scheme='https', suppressError=True, cert=cert)
    except:
      node_info = None

    scheme = "https"

    # Negotiate for HTTPS
    # TODO: Flag on the system that never allows non-HTTPS
    if node_info is None:
      scheme = "http"
      try:
        node_info = self.network.getJSON(nodeInfoURL, scheme='http', suppressError=True, cert=None)
      except:
        node_info = None

      if node_info is None:
        return None

    if not isinstance(node_info, dict):
      # We can't query node information, but that's OK
      node_info = {}

    node_info['httpSSL'] = scheme == "https"

    return node_info

  def pathFor(self, url):
    """ Retrieves the system path for the given node.
    """
    nodePath = self.configuration.get('path', os.path.join(Config.root(), "nodes"))
    nodePath = os.path.join(nodePath, self.partialFromURL(url))

    return nodePath

  def createPathFor(self, url):
    """
      Retrieves the configuration root path and create a directory with it doesnt exist
      Then, join this path with the host and port of given URL and if it doesn't exist
      create a new directory with it.
    """
    nodePath = self.configuration.get('path', os.path.join(Config.root(), "nodes"))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    nodePath = os.path.join(nodePath, self.partialFromURL(url))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    return nodePath

  def retrieveCertificate(self, url):
    """ Retrieves the public key from the given node.
    """

    import ssl, socket
    urlparts = urlparse(url)
    host = urlparts.hostname

    socket.setdefaulttimeout(1)
    verified = False
    try:
      verified = ssl.get_server_certificate((host, 443,))
    except:
      verified = None

    if verified:
      return True

    url = self.urlFromPartial(url)
    url = url + "/public_key"

    # Download that key
    try:
      response, content_type, size = self.network.get(url, scheme="https", doNotVerify=True, suppressError=True)
    except:
      response = None

    if response is None:
      try:
        response, content_type, size = self.network.get(url, scheme="http", doNotVerify=True, suppressError=True)
      except:
        response = None
        return None

    if response is None:
      return None

    reader = codecs.getreader('utf-8')
    data = reader(response).read()

    if not data:
      return False

    certPath = os.path.join(self.createPathFor(url), self.CERTIFICATE_FILENAME)
    with open(certPath, 'w+') as f:
      f.write(data)

    return True

  def certificatePathFor(self, url):
    """ Gets the certificate path for the given node url.
    """

    nodePath = self.pathFor(url)
    nodePath = os.path.join(nodePath, self.CERTIFICATE_FILENAME)

    if not os.path.exists(nodePath):
      return None

    return nodePath

  def discover(self, url, untrusted=True, quiet=False):
    """ This method will lookup or append a new Node record for the given address.

    Returns None when the node cannot be reached.
    """

    url = self.urlFromPartial(url)
    host, port = self.hostAndPortFromURL(url)

    existing_node = self.search(url)

    NodeManager.Log.noisy("attempting to discover node at %s" % (url))

    node_info = self.retrieveInfo(url)
    if node_info is None:
      if not quiet:
        NodeManager.Log.error("No response from the node specified")
      return None

    # Look up any information we have on the Node

    # Create a record for the Node, if it doesn't exist
    if existing_node:
      # Update Node
      if not quiet:
        NodeManager.Log.write("Updating a known node")
      db_node = self.datastore.update(existing_node, node_info, host, port)
    else:
      # Create Node
      NodeManager.Log.write("Found a new node (%s)" % (url))

      db_node = self.datastore.insert(node_info, host, port)

    # TODO: add untrusted boolean to node table

    # Tell subsystems that might care
    if 'storage' in node_info:
      self.storage.discoverNode(node_info['storage'])

    # Return the Node record
    return db_node

  def urlForNode(self, node, path):
    """ Returns a URL for the given path for the given node.
    """

    scheme = "https"
    if node.http_ssl == 0:
      scheme = "http"

    host = node.host
    port = node.http_port

    return "%s://%s:%s/%s" % (scheme, host, port, path)

  def urlForObject(self, node, uuid, revision=None, path=None):
    """ Returns a URL for the given object id for the given node.
    """

    fullPath = uuid

    if not revision is None:
      fullPath = "%s/%s" % (fullPath, revision)

    if path:
      fullPath = "%s/%s" % (fullPath, path)

    return self.urlForNode(node, fullPath)

  def findIdentity(self, uri, person = None):
    """ Reports nodes that hold an identity by looking at random known nodes.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.noisy("looking for identity in %s (%s)" % (node.name, node.host))

      result = self.identityFrom(node, uri, person = person)

      if result is not None:
        NodeManager.Log.noisy("found identity at %s (%s)" % (node.name, node.host))
        return [node]

    return []

  def findObject(self, uuid, revision = None):
    """
    When we are in a bind to find an object, we can call this method to go
    and query for which node has this object. Will return either an empty
    array when the search comes up empty, or at least one Node which has the
    requested object.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.noisy("looking in %s (%s)" % (node.name, node.host))

      url = self.urlForObject(node, uuid, revision = revision)
      cert = self.certificatePathFor(url)
      result = self.network.getJSON(url, cert=cert)

      if result is not None:
        NodeManager.Log.write("found object at %s (%s)" % (node.name, node.host))
        return [node]

    return []

  def retrieveObjectInfoFrom(self, node, uuid, revision=None, person=None):
    """ This will retrieve a stream of the object info from the server.
    """

    objectPath = self.urlForObject(node, uuid, revision=revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    objectInfoPath = objectPath

    response, content_type, size = self.network.get(objectInfoPath, 'application/json', cert=cert, suppressError=True)

    return response

  def pullObjectInfoFrom(self, node, uuid, revision=None):
    """ This will pull the object info from the server.
    """

    objectPath = self.urlForObject(node, uuid, revision=revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    objectInfoPath = objectPath

    response, content_type, size = self.network.get(objectInfoPath, 'application/json', cert=cert, suppressError=True)

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      objectInfo = json.load(reader(response))
    except:
      objectInfo = {}

    return objectInfo

  def pullBuildsFrom(self, node, uuid, revision):
    """ This will pull the build information known about this revision of an object from the given node.
    """

    objectPath = self.urlForObject(node, uuid, revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    # Retrieve version information
    objectBuildsPath = objectPath + "/builds"

    response, content_type, size = self.network.get(objectBuildsPath, 'application/json', cert=cert, suppressError=True)

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      objectBuilds = json.load(reader(response))
    except:
      objectBuilds = {}

    return objectBuilds

  def pullVersionsFrom(self, node, uuid):
    """ This will pull the version information known about this object from the given node.
    """

    objectPath = self.urlForObject(node, uuid)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    # Retrieve version information
    objectVersionsPath = objectPath + "/versions"

    response, content_type, size = self.network.get(objectVersionsPath, 'application/json', cert=cert, suppressError=True)

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      objectVersions = json.load(reader(response))
    except:
      objectVersions = {}

    return objectVersions

  def repositoryFrom(self, node, uuid, revision=None):
    objectPath = self.urlForObject(node, uuid, revision=revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    NodeManager.Log.noisy(f"Cloning {uuid} at revision {revision} from {objectPath}")

    # Clone a temporary copy of this object
    return GitRepository(objectPath, cert=cert)

  def buildLogFrom(self, node, uuid, revision, buildId, destination):
    """ Pull the build log from the given node to the given destination.
    """

    # Get the URL for the build
    buildLogPath = self.urlForObject(node, uuid, revision=revision)
    buildLogPath = buildLogPath + "/builds/" + buildId + "/log"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(buildLogPath)

    # Pull down an archive of the build
    reader, content_type, size = self.network.get(buildLogPath, 'application/octet-stream', cert=cert, suppressError=True)

    logfile = os.path.join(destination, "log")

    bytesTotal = size
    bytesSoFar = 0
    bytesRead = 1
    with open(logfile, 'wb+') as f:
      while bytesRead > 0:
        chunk = reader.read(8196*4)
        bytesRead = len(chunk)
        bytesSoFar += bytesRead
        #NodeManager.Log.write(f"Read {bytesRead} bytes of log...")
        f.write(chunk)

    return logfile

  def buildFrom(self, node, uuid, revision, buildId, identity, destination):
    """ Pull the given build for the given object from the given node.
    """

    # Get the URL for the build
    buildPath = self.urlForObject(node, uuid, revision=revision)
    buildPath = buildPath + "/builds/" + buildId + "/files/"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(buildPath)

    # Pull down an archive of the build
    reader, content_type, size = self.network.get(buildPath, 'application/x-tar+xz', cert=cert, suppressError=True)

    # Extract it as we download it to a temporary space
    import tarfile
    import zipfile
    import tempfile
    import time

    tmpdir = os.path.realpath(tempfile.mkdtemp(prefix="occam-", dir=Config.tmpPath()))
    tmpfile = os.path.join(tmpdir, "build.tar.xz")

    bytesTotal = size
    bytesSoFar = 0
    bytesRead = 1
    with open(tmpfile, 'wb+') as f:
      while bytesRead > 0:
        chunk = reader.read(8196*4)
        bytesRead = len(chunk)
        bytesSoFar += bytesRead
        #NodeManager.Log.write(f"Read {bytesRead} bytes...")
        f.write(chunk)

    # What the HECK, ZipFile does not extract with permissions
    # So executables don't have execute bits... what THE HECK.
    # INCONCEIVABLE!! (That reminds me, Gina Gershon is a VERY
    # underrated actress imho... I've been watching Riverdale
    # and Brooklyn 99. I know, I know... but she was also good
    # in Elementary! Also, Lucy Liu's on set fashion person or
    # whatever they are called is amazing.)
    # Advised from https://stackoverflow.com/questions/39296101/python-zipfile-removes-execute-permissions-from-binaries
    # This feels flimsy, but, yanno.
    # Like, what happens when _extract_member changes? Yikes!
    class ZipFileX(zipfile.ZipFile):
      """ Custom ZipFile class handling file permissions.
      """
      def _extract_member(self, member, targetpath, pwd):
        if not isinstance(member, zipfile.ZipInfo):
          member = self.getinfo(member)

        # Normally extract
        targetpath = super()._extract_member(member, targetpath, pwd)

        # Handle symlinks
        # With help from: https://mail.python.org/pipermail/python-list/2005-June/322179.html
        if (member.external_attr & 0xA0000000) == 0xA0000000:
          # In this case, the newly created file will contain the target
          # path of the symbolic link
          with open(targetpath, 'rb') as f:
            linkpath = f.read()
          os.remove(targetpath)
          os.symlink(linkpath, targetpath)
        else:
          attr = member.external_attr >> 16
          if attr != 0:
            os.chmod(targetpath, attr)

        # Preserve modification time
        date_time = time.mktime(member.date_time + (0, 0, -1))
        os.utime(targetpath, (date_time, date_time), follow_symlinks = False)
        return targetpath

    # IF IT IS A ZIP FILE
    #with ZipFileX(tmpfile, 'r') as zipStream:
    #  zipStream.extractall(destination)

    # OTHERWISE, TAR FILE
    with tarfile.open(tmpfile) as tf:
      tf.extractall(destination)

    # Delete the temporary path
    import shutil
    shutil.rmtree(tmpdir)

    return destination

  def pullInvocationDataFrom(self, node, obj, categories=None):
    """
    Pulls invocation data from the given node and object. This will pull
    invocation data from the given set. By default, it will pull "builds",
    "output", and "generated" from the node.
    """

    if categories is None:
      categories = ['builds', 'output', 'generated']

    uuid = obj.id

    # Determine the URL for the invocations
    objectPath = self.urlForObject(node, uuid) + "/invocations/"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    # Pull each category in turn
    for category in categories:
      invocationPath = objectPath + category

      response, content_type, size = self.network.get(invocationPath, 'application/json', cert=cert, suppressError=True)

      # Parse the json from the stream
      reader = codecs.getreader('utf-8')
      try:
        invocation = json.load(reader(response))
      except:
        invocation = {}

      # Write out response to invocations path
      self.occam.objects.mergeInvocation(uuid, category, invocation)

  def pullBackendFrom(self, node, uuid, revision=None):
    """
    This will pull any pre-built backend images for the given object
    from the given node.
    """
    # TODO: pull a backend list from the node to know what exists
    
    # Right now, assume a docker pre-built image exists
    dockerHandler = self.backends.handlerFor('docker')
    objectPath = self.urlForObject(node, uuid, revision)
    cert = self.certificatePathFor(objectPath)

    return dockerHandler.pull(objectPath, uuid, revision=revision, cert=cert)

  def taskFor(self, fromEnvironment=None, fromArchitecture=None, toEnvironment=None, toArchitecture=None, toBackend=None, obj=None):
    """ Queries for a task from known OCCAM nodes.

    This will ask known nodes how it would construct a VM for the given object
    for the given backend. It returns a VM object with enough metadata to
    discover required resources to replicate the VM on this node.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.noisy("looking in %s (%s)" % (node.name, node.host))

      return self.taskForFrom(node, fromEnvironment, fromArchitecture, toEnvironment, toArchitecture, toBackend=toBackend, obj=obj)

    return None

  def taskForFrom(self, node, fromEnvironment=None, fromArchitecture=None, toEnvironment=None, toArchitecture=None, toBackend=None, obj=None):
    """
    This will ask the given node how it would construct a VM for the given
    object for the given backend. It returns a VM object with enough metadata
    to discover required resources to replicate the VM on this node.
    """

    if obj is None:
      taskPath = self.urlForNode(node,
        "task?fromEnvironment=%s&fromArchitecture=%s&toEnvironment=%s&toArchitecture=%s" % (
          fromEnvironment, fromArchitecture, toEnvironment, toArchitecture
        )
      )
    else:
      # TODO: pass along revision and env/arch goals?
      taskPath = self.urlForNode(node,
        "task?fromObject=%s" % (
          obj.objectInfo().get('id')
        )
      )

    if not toBackend is None:
      taskPath = "%s&toBackend=%s" % (taskPath, toBackend)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def retrieveDirectoryFrom(self, node, option, person):
    """ Retrieves the given file from the given node.
    """

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/files/%s" % (
        token,
        option.path or ""
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    items = self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

    if not items:
      return None

    return items

  def retrieveFileStatFrom(self, node, option, person):
    """ Retrieves the given file from the given node.
    """

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/files/%s" % (
        token,
        option.path or "object.json"
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def retrieveFileFrom(self, node, option, person):
    """ Retrieves the given file from the given node.
    """

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/raw/%s" % (
        token,
        option.path or "object.json"
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    response, content_type, size = self.network.get(taskPath, cert=cert, suppressError=True)

    return response

  def historyFrom(self, node, option, person):
    """ Returns the object status for the given object at the given node.
    """

    if option.path:
      return self.retrieveFileStatFrom(node, option, person)

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/history" % (
        token
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def statusFrom(self, node, option, person):
    """ Returns the object status for the given object at the given node.
    """

    if option.path:
      return self.retrieveFileStatFrom(node, option, person)

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/status" % (
        token
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def identityFrom(self, node, id):
    """ Retrieve identity information from the given node.
    """

    identityPath = self.urlForNode(node, f"identities/{id}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(identityPath)

    identityInfo = self.network.getJSON(identityPath, 'application/json', cert=cert, suppressError=True)

    return identityInfo

  def viewersFor(self, node, type, subtype, person=None):
    """ Retrieve viewers information for the given node given the type and subtype of the viewer
    """

    taskPath = self.urlForNode(node,
      "viewers?type=%s&subtype=%s" % (
        type,
        subtype
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def providersFor(self, node, environment, architecture, person=None):
    """ Retrieve provider information for the given node given the environment and architecture requested.
    """

    taskPath = self.urlForNode(node,
      "providers?environment=%s&architecture=%s" % (
        environment,
        architecture 
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)
