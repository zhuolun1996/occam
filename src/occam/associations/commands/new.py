# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.associations.write_manager import AssociationWriteManager
from occam.objects.manager            import ObjectManager

from occam.commands.manager import command, option, argument

@command('associations', 'new',
  category      = 'Association Management',
  documentation = "Creates an association")
@argument("object", type="object")
@argument("kind",   help = "the type of association. (viewer, editor, provider)")
@argument("major",  help = "the major matcher for this assocation (type or architecture)")
@option("-n", "--minor",       action  = "store",
                               help    = "the minor matcher for this assocation, if desired (subtype or environment)")
@option("-s", "--system-wide", action  = "store_true",
                               dest    = "system_wide",
                               help    = "will associate the object as a default for the entire system (requires admin role)")
@uses(ObjectManager)
@uses(AssociationWriteManager)
class AssociationNewCommand:
  def do(self):
    person = self.person

    if self.options.system_wide:
      if not self.person:
        Log.error("Must be logged in")
        return -1

      if not 'administrator' in self.person.roles:
        Log.error("Must be an administrator")
        return -1

      person = None

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Object could not be resolved")
      return -1

    self.associations.write.create(obj, associationType = self.options.kind,
                                        major           = self.options.major,
                                        minor           = self.options.minor,
                                        identity        = person and person.identity)

    return 0
