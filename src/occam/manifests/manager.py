# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict

from occam.log import loggable
from occam.config import Config
from occam.object import Object

from occam.manager import uses, manager

from occam.objects.write_manager import ObjectWriteManager
from occam.versions.manager      import VersionManager
from occam.storage.manager       import StorageManager
from occam.builds.manager        import BuildManager
from occam.notes.manager         import NoteManager
from occam.backends.manager      import BackendManager
from occam.nodes.manager         import NodeManager
from occam.databases.manager     import DatabaseManager
from occam.permissions.manager   import PermissionManager
from occam.system.manager        import SystemManager
from occam.services.manager      import ServiceManager

import os
import json
import copy
import shutil
import time
import datetime

from occam.semver import Semver

from occam.key_parser import KeyParser

@loggable
@uses(ObjectWriteManager)
@uses(VersionManager)
@uses(StorageManager)
@uses(BuildManager)
@uses(BackendManager)
@uses(NoteManager)
@uses(NodeManager)
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(SystemManager)
@uses(ServiceManager)
@manager("manifests")
class ManifestManager:
  """ This manages the creation and querying of tasks to build or run objects.

  The manifest manager can take an Object and, based on its metadata, produce a
  task to build or run that object. This task is a manifest that describes what
  other objects should be present on the virtual machine which ultimately runs
  the artifact.

  Task generation goes through a few phases. The first phase takes the object
  and queries for a path through other artifacts that can be used to run it. For
  instance, a Linux-based application describes itself as such, and then the
  query will find a Linux environment to be used as a base for that object. In
  other examples, the artifact may require a particular software environment.
  Such as a jar file, which may need a Java runtime. In this case, the path is
  found through querying the BackendManager which keeps track of the these
  relationships between objects and "Provider" objects in order to facilitate
  execution diversity.

  The second phase is resolving dependencies for each object in the chain and
  adding those dependencies (and their dependencies and so on) to the task. Many
  times, these are described in terms of tags (or versions) which will use a
  style of semantic versioning to resolve to specific artifact revisions for
  each dependency (for example, a software library at version >= 2.0 will
  resolve to revision "fa63ea96cb258b110d70eb") The revisions are written to the
  task (along with the version) to lock in that particular version of the object
  used.

  The third phase resolves the rules written within object metadata. Most
  objects are simple and do not have such rules, but others may write entries in
  their metadata that are to be filled once the task is generated. These are of
  the form {{ x }} where x is some key within the task, delimited by dots (.).
  When these are encountered, they are parsed and replaced with the value
  indicated by the key written within. For example, {{ input.0.file }} would be
  replaced with the file attached as the first input of that object. In this
  case, you can see why this is powerful. This is only known at the time of
  task generation. Furthermore, it can be filled in differently at any point,
  allowing tasks to be somewhat generic where input can be given later.

  When tasks are generated, they might be stored as an Object themselves and
  assigned an identifier. They can be pushed to federated storage and queried
  from other machines. In some cases, taskFor() can be called to simply return
  a generated task without storing it as an object.
  """

  @staticmethod
  def deepMerge(destination, source):
    """
    """

    # TODO: lists should probably be appended

    for key, value in source.items():
      if isinstance(value, dict):
        # get node or create one
        node = destination.setdefault(key, {})
        ManifestManager.deepMerge(node, value)
      else:
        destination[key] = value

    return destination

  def __init__(self):
    self.parser = KeyParser()

  def buildAll(self, object, local=False, penalties=None, tasks = None, seen = None, originalPenalties = None, desiredBy = None, person=None):
    """ Build an object and recursively builds any necessary objects.

    This will employ a strategic system to build only what is necessary. It will
    avoid pitfalls such as cyclic dependencies that can happen when version matches
    resolve unexpectedly.
    """

    # This will be the returned list
    if tasks is None:
      tasks = []

    if originalPenalties is None:
      originalPenalties = copy.deepcopy(penalties)

    # Penalties is a hash consisting of arrays of revision strings per uuid keys
    # This is passed around to object resolvers to disallow objects of certain
    #   revisions being chosen as version resolves and such.
    if penalties is None:
      penalties = {}

    # This simply keeps track of the objects we have current seen
    if seen is None:
      seen = {}

    # Issue a build of this object
    while True:
      try:
        info = self.objects.infoFor(object)
        ownerInfo = info
        ManifestManager.Log.write("Building %s %s @ %s" % (info.get('type', 'object'), info.get('name', 'unknown'), object.revision))

        if 'build' not in info:
          ownerInfo = self.objects.ownerInfoFor(object)
          if 'build' in ownerInfo:
            object = self.objects.ownerFor(object, person = self.person)

        # If this is already in penalties, then we are already building it
        # Just fall out as unresolved
        if object.id in penalties and object.revision in penalties[object.id]:
          return tasks, False

        if not object.id in penalties:
          penalties[object.id] = []
        if not object.revision in penalties[object.id]:
          penalties[object.id].append(object.revision)

        if not object.id in seen:
          seen[object.id] = {}
        if not object.revision in seen[object.id]:
          seen[object.id][object.revision] = desiredBy

        # Keep a copy of the penalties
        #oldPenalties = copy.deepcopy(penalties)

        task = self.build(object    = object,
                          id        = object.id,
                          revision  = object.revision,
                          local     = local,
                          penalties = penalties,
                          person    = person)
        tasks.append(task)
        break
      except BuildRequiredError as e:
        info = self.objects.infoFor(e.requiredObject)
        ManifestManager.Log.write("Cannot find built copy of %s %s" % (info.get('type', 'object'), info.get('name', 'unknown')))
        tasks, resolved = self.buildAll(e.requiredObject, penalties = penalties, tasks = tasks, seen = seen, originalPenalties = originalPenalties, desiredBy = object, person = person)
        if not isinstance(resolved, bool) or not resolved == True:
          return tasks, resolved
      except DependencyUnresolvedError as e:
        ManifestManager.Log.write("Cyclic dependency detected on %s %s" % (e.objectInfo.get('type', 'object'), e.objectInfo.get('name', 'unknown')))
        for k in list(penalties.keys()):
          if not k in originalPenalties:
            del penalties[k]
        penalties.update(originalPenalties)

        # Add all objects that requested this object to penalties
        owner_id = self.notes.resolveOwner(e.id)
        for revision, requestor in seen[owner_id].items():
          info = self.objects.infoFor(requestor)
          ownerInfo = info

          if 'build' not in info:
            ownerInfo = self.objects.ownerInfoFor(requestor)
            if 'build' in ownerInfo:
              requestor = self.objects.ownerFor(requestor, person = self.person)
          ManifestManager.Log.write("Penalizing %s %s @ %s" % (info.get('type', 'object'), info.get('name', 'unknown'), object.revision))

          # Add the item that added this object originally
          if not object.id in penalties:
            penalties[object.id] = []
          if not requestor.revision in penalties[object.id]:
            penalties[object.id].append(requestor.revision)

        return tasks, 1

    return tasks, True

  def build(self, object=None, id=None, revision=None, local=False, penalties=None, backend=None, locks=None, person=None):
    """ Builds the given object by any means.
    """

    objectInfo = {}

    if object:
      objectInfo = self.objects.infoFor(object)
      id = object.id

      if not revision:
        revision = object.revision

    cachedTasks = None
    if not local:
      cachedTasks = self.notes.retrieve(id, "tasks/build", key="object", revision=revision, fallback=False)

    task = None

    if cachedTasks:
      for taskObject in cachedTasks:
        taskObject = taskObject.get('value')
        if self.backends.isAvailable(taskObject.get('backend')):
          task = self.objects.retrieve(id=taskObject.get('id'), revision=taskObject.get('revision'), person=person)
          if task:
            task = self.objects.infoFor(task)
            break

    if task is None:
      if object is None:
        object = self.objects.retrieve(id=id, revision=revision, person=person)

      if object is None:
        ManifestManager.Log.error("Object not found.")
        return -1

      objectInfo = self.objects.infoFor(object)
      id = object.id

      if not "build" in objectInfo:
        ManifestManager.Log.write("No build information for this object.")
        return None

      task = self.taskFor(object, section="build", penalties=penalties, backend=backend, locks=locks, person=person)

    if task is None:
      ManifestManager.Log.write("Could not produce a task to build this object.")
      return None

    taskName = "Task to %s %s on %s" % ("build", objectInfo.get('name'), task.get('backend'))
    taskObject = self.objects.write.create(name        = taskName,
                                           object_type = "task",
                                           subtype     = "build",
                                           info        = task)
    if person:
      db_obj = self.objects.write.store(taskObject, identity = person.identity)
      taskObject.id  = db_obj.id
      taskObject.uid = db_obj.uid

    # Set build task permissions to be permissive
    self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    return taskObject

  def run(self, object=None, id=None, revision=None, local=False, arguments=None, inputs=None, penalties=None, backend=None, generator=None, service=None, services=None, preferred=None, locks=None, person=None):
    """ Runs the given object.

    Args:
      service (string): The service to run for this object, given that it provides that service. Can be None.
    """

    ownerInfo = self.objects.ownerInfoFor(object)
    info      = self.objects.infoFor(object)

    if id is None:
      id = self.objects.idFor(object, person.identity)
      object.id = id

    if revision is None:
      revision = object.revision

    # TODO: Pull out the build infos if this object requires a build
    #       and use that task to run this object with the built version.
    buildTask = None
    if local == False and ("build" in info or "build" in ownerInfo):
      buildTasks = self.builds.retrieveAll(object)
      if buildTasks is None:
        buildTasks = []

      if "build" in ownerInfo and "build" not in info:
        buildTasks.extend(self.builds.retrieveAll(self.objects.ownerFor(object, person = person)) or [])

      for buildRecord in buildTasks:
        # Build the object
        # TODO: issue build command
        buildTask = self.objects.retrieve(buildRecord.build_id, buildRecord.build_revision, person=person)

        if buildTask is None:
          continue

        # If we found a suitable build, just quit looking
        buildTask = self.objects.infoFor(buildTask)
        buildTask['id'] = buildRecord.build_id
        break

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error("Object %s %s must be built first." % (info.get('type'), info.get('name')))
        return None

    if local == True and "build" in self.objects.infoFor(object):
      # Look for the buildtask in the local path
      buildTaskPath = os.path.join(object.path, ".occam", "local", "object.json")
      import json

      try:
        with open(buildTaskPath, "r") as f:
          buildTask = json.load(f)
      except:
        pass

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error("Object must be built locally first.")
        return None

    cachedTasks = None
    if not local and inputs is None:
      cachedTasks = self.notes.retrieve(id, "tasks/run", key = "object" + ("-" + service if service else ""), revision=revision, fallback=False)

    task = None

    if cachedTasks:
      for taskObject in cachedTasks:
        if taskObject.get('id') is None:
          continue
        taskObject = taskObject.get('value')
        if self.backends.isAvailable(taskObject.get('backend')):
          taskObject = self.objects.retrieve(taskObject.get('id'), taskObject.get('revision'), person=person)
          if taskObject:
            task = self.objects.infoFor(taskObject)
            break

    if task is None:
      # Get the object to run
      if object is None:
        ManifestManager.Log.error("Object not found.")
        return None

      task = self.taskFor(object, buildTask=buildTask, penalties = penalties, backend = backend, generator=generator, inputs=inputs, service=service, services=services, preferred=preferred, locks=locks, person=person)

      # Store task
      objectInfo = self.objects.infoFor(object)

      environment = task.get('environment')
      architecture = task.get('architecture')

      taskName = "Task to %s %s on %s" % ("run", objectInfo.get('name'), task.get('backend'))
      if environment and architecture:
        taskName = "Task to %s %s on %s/%s" % ("run", objectInfo.get('name'), environment, architecture)

      taskObject = self.objects.write.create(name        = taskName,
                                             object_type = "task",
                                             subtype     = "run",
                                             info        = task)
      if person:
        db_obj = self.objects.write.store(taskObject, identity = person.identity)
        taskObject.id  = db_obj.id
        taskObject.uid = db_obj.uid

      # Cache task
      if environment != "native" and architecture != "native":
        self.notes.store(id, "partialTasks/%s" % ("run"), key = "%s-%s" % (environment, architecture), value= { "revision": taskObject.revision, "id": taskObject.id, "uid": taskObject.uid }, revision=revision)
      else:
        self.notes.store(id, "tasks/%s" % ("run"), key = "object", value={ "backend": task.get('backend'), "revision": taskObject.revision, "id": taskObject.id, "uid": taskObject.uid }, revision=revision)

      # For now the permissions will be permissive:
      # This task should belong to the object, however, and inherit permissions.
      self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    if task is None:
      ManifestManager.Log.error("Could not determine how to run this object.")
      return None

    return taskObject

  def isBuildable(self, object):
    """ Returns True if the given object is buildable.
    """

    dependencyInfo = self.objects.infoFor(object)
    dependencyOwnerInfo = self.objects.ownerInfoFor(object)
    return 'build' in dependencyInfo or 'build' in dependencyOwnerInfo

  def retrieveBuildTaskFor(self, object, preferred, person = None):
    """ Retrieve the task object that built the given object.
    """

    buildId = None
    buildRevision = None
    buildTask = None
    if not self.isBuildable(object):
      return None

    # Look up a build or report that we need to build this object
    objectInfo = self.objects.infoFor(object)
    buildingObject = object
    if 'build' not in objectInfo:
      buildingObject = self.objects.ownerFor(object, person = person)

    buildingObjectInfo = self.objects.infoFor(buildingObject)
    buildIds = [{"id": x.build_id, "revision": x.build_revision} for x in self.builds.retrieveAll(buildingObject, preferred)]

    # For every build we know, we attempt to find that build's content
    for build in buildIds:
      path = self.objects.buildPathFor(buildingObject, build["id"])

      if path is not None:
        buildTask = self.objects.retrieve(id = build["id"], revision = build["revision"], person = person)

        if buildTask:
          # TODO: check to make sure locked versions match our expectations
          buildId = buildTask.id
          buildRevision = buildTask.revision
          buildTask = self.objects.infoFor(buildTask)
          break

    # TODO: when buildTask is None!
    if buildId is None:
      raise BuildRequiredError(objectInfo, buildingObject, "cannot find a built task for %s %s" % (objectInfo.get('type'), objectInfo.get('name')))

    buildTask['id'] = buildId
    buildTask['revision'] = buildRevision

    return buildTask

  def completeInitSection(self, currentInit, currentProvider):
    """ Will finalize the given init section based on the given provider object info.

    Modifies currentInit in place.

    Returns:
      currentInit
    """

    providesInfo = currentProvider.get('provides', {})
    if isinstance(providesInfo, list):
      # TODO: handle multiple environments
      providesInfo = providesInfo[0]

    providerEnvVars = providesInfo.get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in currentInit.get('env', {}).items():
      asdf = value
      separator = value.get('separator')
      unique    = value.get('unique', False)

      if separator:
        # TODO: handle escapes
        items = separator.join(value.get('items', []))
      else:
        value = value.get('items', [])
        if len(value) == 0:
          items = ""
        else:
          items = value[-1]

      currentInit['env'] = currentInit.get('env', {})
      currentInit['env'][key] = items

    return currentInit

  def combineEnvSection(self, currentEnv, secondEnv):
    """ Will produce a combined 'env' metadata section from two given ones.

    Will add any addition items to the 'currentEnv' in place and return
    the same 'currentEnv'.
    """

    if not isinstance(currentEnv, dict) or not isinstance(secondEnv, dict):
      return currentEnv

    for key, value in secondEnv.items():
      # Canonize value as a dict with an items field as a list
      if not isinstance(value, dict):
        value = { "items": value }

      if not 'items' in value:
        value['items'] = []

      if not isinstance(value['items'], list):
        value['items'] = [value['items']]

      # If the key already exists, merge environment metadata properly
      if key in currentEnv:
        if not isinstance(currentEnv[key], dict):
          currentEnv[key] = {
            "items": currentEnv[key]
          }

        if not 'items' in currentEnv[key]:
          currentEnv[key]['items'] = []

        if not isinstance(currentEnv[key]["items"], list):
          currentEnv[key]["items"] = [currentEnv[key]["items"]]

        for subKey, subValue in value.items():
          # For items, which is a list of values, just extend the list
          if subKey == 'items':
            currentEnv[key]['items'].extend(value.get('items', []))
          elif not subKey in currentEnv[key]:
            # For other metadata, add it only if it doesn't already exist
            currentEnv[key][subKey] = subValue
      else:
        # Otherwise, just add it
        currentEnv[key] = value

    return currentEnv

  def parseInitSection(self, objectInfo, currentInit, currentProvider):
    """ Will modify the given current init section with the contents of the given init section.

    It will use currentProvider as a basis of understanding what certain environment variables mean.
    """

    # Retrieve the 'init' section of the given object
    objectInit = objectInfo.get('init')

    # Bail if the init section is not a dictionary
    if objectInit is None or (not isinstance(objectInit, OrderedDict) and not isinstance(objectInit, dict)):
      return currentInit

    # Retrieve 'env' section
    objectEnvVars = objectInit.get('env', {})

    # Bail if env section is invalid
    if objectEnvVars is None or (not isinstance(objectEnvVars, OrderedDict) and not isinstance(objectEnvVars, dict)):
      return currentInit

    providerEnvVars = currentProvider.get('provides', {}).get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in objectEnvVars.items():
      separator = None
      unique    = False

      if key in currentInit.get('env', {}):
        separator = currentInit['env'][key].get('separator')
        unique = currentInit['env'][key].get('unique', False)

      if isinstance(value, dict):
        separator = value.get('separator')
        unique = value.get('unique', False)

      if key in providerEnvVars:
        separator = providerEnvVars[key].get('separator', separator)
        unique    = providerEnvVars[key].get('unique', unique)

      if isinstance(value, dict):
        value = value.get('items', [])

      if isinstance(value, list):
        items = value
      elif separator:
        # TODO: handle escapes
        items = value.split(separator)
      else:
        items = [value]

      currentInit['env'] = currentInit.get('env', {})
      newItems = currentInit['env'].get(key, {}).get('items', [])
      newItems.extend(items)

      if unique:
        newItems = list(set(newItems))

      currentInit['env'][key] = {
        "unique":    unique,
        "separator": separator,
        "items":     newItems
      }

    return currentInit

  def resolveDependency(self, dependencyList, dependencyIndex, context, locks, person):
    """ Resolves the given dependency and returns its contents.
    """

    dependency = dependencyList[dependencyIndex]
    context['dependencies'][dependency['id']] = context['dependencies'].get(dependency['id'], {})
    priorDependencyInfo = context['dependencies'][dependency['id']]
    penalties = context['penalties']
    dependencyInfo = None
    dependencyObject = None

    resolved = False

    # Retain the version requirement that was satisfied

    if locks and locks.get(dependency.get('id')):
      dependency['version'] = dependency.get('version', locks[dependency.get('id')]) + "," + locks[dependency.get('id')]
      dependency['version'] = ",".join(list(set(dependency['version'].split(","))))

    if 'version' in dependency:
      dependency['requested'] = dependency['version']

    targetVersion = dependency.get('version')
    if targetVersion and priorDependencyInfo.get('version'):
      if targetVersion != priorDependencyInfo['version']:
        targetVersion = targetVersion + "," + priorDependencyInfo['version']
        targetVersion = ",".join(list(set(targetVersion.split(","))))
      else: # targetVersion is the same as the previously resolved version
        # Just return a clone of the prior dependency
        dependencyInfo = copy.deepcopy(priorDependencyInfo['last'])
        resolved = True

    while resolved is not True:
      # Resolve each dependency and pull out subDependencies
      ManifestManager.Log.noisy("looking for dependency %s %s @ %s" % (dependency.get('type'), dependency.get('name'), dependency.get('revision', targetVersion)))

      dependencyObject = self.objects.retrieve(id        = dependency.get('id'),
                                               version   = targetVersion,
                                               revision  = dependency.get('revision'),
                                               penalties = penalties,
                                               person    = person)
      if dependencyObject is None:
        ManifestManager.Log.noisy("failed to find %s %s @ %s" % (dependency.get('type'), dependency.get('name'), dependency.get('revision', targetVersion)))
        resolved = dependency

        # Break out if we have tried all we can
        if targetVersion == dependency.get('version'):
          break

        # Lessen the restriction
        targetVersion = dependency.get('version')
        continue

      # Get the actual realized dependency information
      dependencyObjectInfo = self.objects.infoFor(dependencyObject)

      # Create the dependency information and copy things we want to keep
      dependencyInfo = {}
      dependencyInfo.update(dependency)
      dependencyInfo['revision'] = dependencyObject.revision
      if dependencyObject.version:
        dependencyInfo['version'] = dependencyObject.version

        # If there is a lock, then apply it
        if dependencyInfo.get('lock') in ['major', 'minor']:
          dependencyInfo['lock'] = dependencyInfo['requested'] + "," + Semver.generalize(dependencyObject.version, dependencyInfo.get('lock'))
        elif dependencyInfo.get('lock') in ['>=']:
          dependencyInfo['lock'] = dependencyInfo['requested'] + ",>=" + dependencyObject.version

      if dependencyObject.uid:
        dependencyInfo['uid'] = dependencyObject.uid

      for key in ['init', 'dependencies', 'install', 'owner', 'file', 'network',
                  'metadata', 'environment', 'architecture']:
        if key in dependencyObjectInfo:
          dependencyInfo[key] = dependencyObjectInfo[key]

      # If we are not building, we are running.
      # If we are running, we need a build to use to know where those binaries are.
      if self.isBuildable(dependencyObject) and not dependency.get('build', {}).get('id'):
        # Get a built copy
        try:
            buildTask = self.retrieveBuildTaskFor(dependencyObject, preferred=None, person = person)
        except BuildRequiredError as e:
            buildTask = None

        # If we can't find a build... we fail to resolve this dependency
        if buildTask is None:
          # Penalize this choice of resolving the version
          dependencyOwner = self.objects.ownerFor(dependencyObject, person = person)
          penalties[dependencyOwner.id] = penalties.get(dependencyOwner.id, []) + [dependencyObject.revision]
          ManifestManager.Log.write(f"Could not find a build task... rejecting {dependency.get('name', 'unknown')} {dependencyObject.version or dependencyObject.revision}.")
          continue

        # Set the build id for the build we will use
        dependencyInfo['build'] = {}
        dependencyInfo['build']['id'] = buildTask.get('id')
        dependencyInfo['build']['revision'] = buildTask.get('revision')
        dependencyInfo['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

      # In case of recursive dependencies, we need to indicate how we are
      # resolving ourselves:
      lastPrior = None
      if not priorDependencyInfo:
        lastPrior = priorDependencyInfo.copy()
        priorDependencyInfo['resolved'] = priorDependencyInfo.get('resolved', []) + [dependencyInfo]
        priorDependencyInfo['version'] = targetVersion
        priorDependencyInfo['last'] = dependencyInfo

      # Resolve the dependencies of the dependencies
      resolved = self.resolveDependenciesFor(dependencyObject, dependencyInfo, context, locks, person, ignoreInject = (dependencyInfo.get('inject') == "ignore"))

      # Restore our context
      if lastPrior:
        priorDependencyInfo = lastPrior

      # If we could not resolve those dependencies... we didn't resolve the main
      # dependency this function was responsible for.
      if resolved is not True:
        if 'version' in dependency:
          ManifestManager.Log.noisy("failed to use %s %s version %s" % (dependency.get('type'), dependency.get('name'), dependency.get('version')))

        # Penalize this choice of resolving the version
        dependencyOwner = self.objects.ownerFor(dependencyObject, person = person)
        penalties[dependencyOwner.id] = penalties.get(dependencyOwner.id, []) + [dependencyObject.revision]
        ManifestManager.Log.noisy("penalizing %s at revision %s" % (dependency.get('name'), dependencyObject.revision))

    # Check that existing resolved dependencies match the resolved version
    if resolved is True and 'version' in dependency and dependencyObject:
      # Look at every instance of this dependency that had a version that was
      # resolved previously.
      for priorDependency in priorDependencyInfo.get('resolved', []):
        if priorDependency.get('version') != targetVersion:
          if Semver.resolveVersion(targetVersion, [dependencyObject.version]):
            priorDependency['version'] = dependencyObject.version

      # Add ourselves to the tracking list of resolved dependencies
      priorDependencyInfo['resolved'] = priorDependencyInfo.get('resolved', []) + [dependencyInfo]

      # Remember the last target version we had
      priorDependencyInfo['version'] = targetVersion

      # And what that resolved to
      priorDependencyInfo['last'] = dependencyInfo

    # Update the dependency list with the resolved dependency
    if dependencyInfo and resolved:
      dependencyList[dependencyIndex] = dependencyInfo

    return resolved

  def resolveDependenciesFor(self, object, objectInfo, context, locks, person, ignoreInject = None):
    """ Resolves the dependency list for the given object metadata.
    """

    dependenciesIndex = context['dependenciesIndex']

    # Resolve the list of dependencies
    resolved = True

    dependencies = objectInfo.get('dependencies', [])
    if 'init' in objectInfo:
      dependencies.extend(objectInfo['init'].get('dependencies', []))
      objectInfo['dependencies'] = dependencies

    dependencyLists = [(dependencies, "general")]

    # Also rake dependencies that are to be injected by the build task
    if not ignoreInject and objectInfo.get('build', {}).get('id'):
      # Get the dependencies that were used when the object was built
      buildTimeDependencies = objectInfo.get('build', {}).get('dependencies', [])

      # Do not consider if the inject parameter doesn't meet the given value
      buildTimeDependencies = list(
        filter(
          lambda x: x.get('inject') in ["init", "run"],
          buildTimeDependencies
        )
      )

      for buildDependency in buildTimeDependencies:
        # Remove existing build id so it will choose a new build if necessary
        if 'build' in buildDependency:
          del buildDependency['build']

        # Assert version requirements for the dependencies based on locks
        if 'lock' in buildDependency:
          if buildDependency not in ['major', 'minor']:
            buildDependency['version'] = buildDependency['lock']

        # Do not propagate the inject requirement into the new task
        del buildDependency['inject']

      # Reassert this list
      objectInfo.get('build', {})['dependencies'] = buildTimeDependencies

      # Append list of filtered build-time dependencies
      dependencyLists.append((buildTimeDependencies, "run-time",))
    else:
      if 'dependencies' in objectInfo.get('build', {}):
        del objectInfo['build']['dependencies']

    # Go through each dependency source
    for dependencies, listType in dependencyLists:
      # And then through each individual dependency
      for i, dependency in enumerate(dependencies):
        # Determine the effective id for the dependency (allow it to inherit the
        # proper id/revision from its parent)
        if 'id' not in dependency:
          # This will occur if the dependency is provided by the object in a
          # sub-object, and therefore has no specified 'id'

          # First check if it is referring to its own owner
          if dependency.get('type') == objectInfo.get('type') and dependency.get('name') == objectInfo.get('name'):
            dependency['id'] = object.id
            dependency['uid'] = object.uid
          else:
            # The 'id' in this case is determined by the system relative to the
            # owning object.
            dependency['id'] = self.objects.idFor(object, object.identity, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))
            dependency['uid'] = self.objects.uidFor(object, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))

          # When a version is not explicit, we need to define a revision
          # In this case, we want the revision of the owning object
          if 'version' not in dependency:
            dependency['revision'] = dependency.get('revision', object.revision)

          # The 'version' should also be inherited by this dependency with the
          # version given by the owning object, when known.
          if 'version' not in dependency:
            if 'version' in objectInfo:
              dependency['version'] = objectInfo['version']
            if object.version:
              dependency['version'] = object.version

        if ignoreInject and 'inject' in dependency:
          del dependency['inject']

        ManifestManager.Log.noisy("Raking %s dependency: %s %s %s%s" % (listType, dependency.get('type'), dependency.get('name'), dependency.get('version', ''), ('@' + dependency.get('revision', '')) if 'revision' in dependency else ''))
        overwriteInfo = None

        resolved = self.resolveDependency(dependencies, i, context = context,
                                                           locks   = locks,
                                                           person  = person)
        if resolved is not True:
          break
      if resolved is not True:
        break

    return resolved

  def resolveServicesFor(self, objectInfo, services, context):
    # Get the list of requested services
    requires = objectInfo.get('requires', {})
    for required, options in objectInfo.get('run', {}).get('requires', {}).items():
      if required not in requires:
        requires[required] = options
      else:
        ManifestManager.deepMerge(requires[required], options)

    # We can resolve some services with those given by a client.
    if objectInfo.get('environment') == "linux":
      for required in (services or []):
        if required not in requires:
          ManifestManager.Log.write("Adding client requested service requirement '%s'" % (required))
          requires[required] = {}

    # Adds each required service
    for required, options in requires.items():
      environment  = objectInfo.get('environment')
      architecture = objectInfo.get('architecture')
      ManifestManager.Log.write("Fulfilling service requirement '%s' for %s/%s" % (required, environment, architecture))

      knownServices = self.services.retrieve(environment, architecture, required)
      serviceObject = None
      for knownService in knownServices:
        serviceObject = self.objects.retrieve(id=knownService.internal_object_id, revision=knownService.revision, person=person)
        if serviceObject:
          serviceObjectInfo = self.objects.infoFor(serviceObject)
          ManifestManager.Log.write("Fulfilling service requirement '%s' with %s %s" % (required, serviceObjectInfo.get('type'), serviceObjectInfo.get('name')))

          serviceObjectInfo['id'] = serviceObject.id
          serviceObjectInfo['uid'] = serviceObject.uid
          serviceObjectInfo['revision'] = serviceObject.revision

          serviceInfo = serviceObjectInfo.get('services', {}).get(required, {})
          ManifestManager.deepMerge(serviceObjectInfo, serviceInfo)

          # Adds a process to the current 'running' listing
          processInfo = self.addProcessToTask(currentInput, serviceObject, serviceObjectInfo, interactive=interactive, context=context, locks = locks, person=person)
          objectIndex = context['objectIndex']

          # Add the init section of the subprocess to the main process
          break
      if not knownServices or not serviceObject:
        # Cannot resolve a required service
        raise "cannot find service"

  def reduceDependencyInitSection(self, dependencyInfo):
    """ This will return a revised init section based on the one provided by a dependency.

    Dependencies do not need their entire init sections contained within the task manifest.
    This function will truncate the init section to only what is needed by the deployment
    backend.
    """

    # We will start from an empty dictionary
    ret = {}

    # Keep the 'link' section so the deployment backend knows to mount/copy directories
    # from objects into the root filesystem
    links = dependencyInfo.get('init', {}).get('link', [])
    if links:
      ret["link"] = links

    copies = dependencyInfo.get('init', {}).get('copy', [])
    if copies:
      ret["copy"] = copies

    env = dependencyInfo.get('init', {}).get('env', {})
    if env:
      ret["env"] = env

    # Return the minimized init section
    return ret

  def reduceDependency(self, process, dependencyCache, dependencyList, dependencyIndex):
    """ Shrinks the dependency information.
    """

    # Get the dependency list
    dependency = dependencyList[dependencyIndex]
    dependencies = dependency.get('dependencies', [])

    # Append all run-time dependencies
    buildTimeDependencies = dependency.get('build', {}).get('dependencies', [])
    if buildTimeDependencies:
      dependencies.extend(buildTimeDependencies)

    # Do we add ourselves to the initial list?
    token = "%s@%s" % (dependency.get('id'), dependency.get('revision'))
    if token in dependencyCache:
      # Revise with lock/inject when necessary
      # Locks add an extra constraint
      if 'lock' in dependency or 'inject' in dependency:
        dependencyOrder = dependencyCache[dependency.get('id')]
        for item in dependencyOrder:
          index = item['index']
          if 'lock' in dependency:
            process['dependencies'][index]['lock'] = dependency['lock']
          if 'inject' in dependency:
            process['dependencies'][index]['inject'] = dependency['inject']
    else:
      # Add our actual dependency
      fullDependency = copy.deepcopy(dependency)
      process['dependencies'].append(fullDependency)
      dependencyCache[token] = fullDependency

      # Order dependencies within list correctly (by revision, as found,
      # and then by version, ascending.)

      # This list is stored via the id and it points to the list of versions
      # or revisions and the indices they are currently located.

      # When a new dependency is added, it will be added where it should go and
      # the rest will be propagated down. The new dependency starts at the end
      # of the list and will bubble up.

      # This will sort the dependency list in-place.
      dependencyCache[dependency.get('id')] = dependencyCache.get(dependency.get('id'), [])
      dependencyOrder = dependencyCache[dependency.get('id')]
      listItem = {}
      if 'version' in dependency:
        listItem['version'] = dependency.get('version')
      else:
        listItem['revision'] = dependency.get('revision')

      listItem['index'] = len(process['dependencies']) - 1
      dependencyOrder.append(listItem)

      # Bubble up this listItem
      index = len(dependencyOrder) - 2
      for currentItem in reversed(dependencyOrder[:-1]):
        # Done if we hit the revision items
        if 'revision' in listItem and 'revision' in currentItem:
          break
        elif 'version' in listItem and 'version' in currentItem:
          # Done if the current item is a lesser version than
          # the new item
          if Semver.versionMatches(currentItem['version'], '<', listItem['version']):
            break

        # Swap
        dependencyOrder[index] = listItem
        dependencyOrder[index+1] = currentItem

        tmp = process['dependencies'][listItem['index']]
        process['dependencies'][listItem['index']] = process['dependencies'][currentItem['index']]
        process['dependencies'][currentItem['index']] = tmp

        tmp = listItem['index']
        listItem['index'] = currentItem['index']
        currentItem['index'] = tmp

        # Look at the next entry
        index = index - 1

    # Reduce ourselves
    dependency = {
      'id': dependency.get('id'),
      'revision': dependency.get('revision'),
      'version': dependency.get('version'),
      'requested': dependency.get('requested'),
      'lock': dependency.get('lock'),
      'init': dependency.get('init', {}),
    }

    if dependency['requested'] is None:
      del dependency['requested']

    if dependency['lock'] is None:
      del dependency['lock']

    if dependency['version'] is None:
      del dependency['version']

    # Add back the dependency list
    if dependencies:
      dependency['dependencies'] = dependencies

    # Reduce each sub-dependency
    for i in range(0, len(dependencies)):
      self.reduceDependency(process, dependencyCache, dependencies, i)

    # Combine ENV section
    dependency['init'] = dependency.get('init', {})
    dependency['init']['env'] = dependency['init'].get('env', {})
    for i in range(0, len(dependencies)):
      self.combineEnvSection(dependency['init']['env'], dependencies[i].get('init', {}).get('env'))
      if 'env' in dependencies[i].get('init', {}):
        del dependencies[i]['init']['env']
    if len(dependency['init']['env']) == 0:
      del dependency['init']['env']

    # Update ourselves
    dependencyList[dependencyIndex] = dependency

  def reduceProcess(self, process):
    """ Shrinks down the process metadata to only what is necessary.
    """

    # We only need full information in the initial set of dependencies.
    dependencyCache = {}

    # All unique dependencies in subsections go into the main dependency list
    # for the process.
    dependencies = process.get('dependencies', [])
    process['dependencies'] = []

    # Append all run-time dependencies
    buildTimeDependencies = process.get('build', {}).get('dependencies', [])
    if buildTimeDependencies:
      dependencies.extend(buildTimeDependencies)

    # Go through and reduce each dependency
    for i, dependency in enumerate(dependencies):
      self.reduceDependency(process, dependencyCache, dependencies, i)

    # Retrieve the new list
    dependencies = process.get('dependencies', [])

    # The prior loop will aggregate all dependencies into our list
    # Reduce, now, each fully formed dependency
    for dependency in dependencies:
      # Get rid of the run-time dependencies of the sub-dependencies
      # And any other build information fluff
      if 'build' in dependency:
        build = {}
        for key in ['id', 'revision']:
          if key in dependency['build']:
            build[key] = dependency['build'][key]
        dependency['build'] = build

      # Reduce 'init' section
      if 'init' in dependency:
        dependency['init'] = self.reduceDependencyInitSection(dependency)

      # Get rid of any unnecessary keys
      keys = ["init", "build", "run", "id", "uid", "revision", "version",
              "name", "type", "dependencies", "network", "file", "metadata",
              "summary", "inject", "lock", "requested", "install", "stage"]
      for key in list(dependency.keys()):
        if key not in keys:
          del dependency[key]

    # Append it back
    if dependencies:
      process['dependencies'] = dependencies

  def addProcessToTask(self, currentEnvironment, object, objectInfo, isBuilding=False, buildTask=None, interactive=False, context={}, locks={}, person=None):
    """ Adds the process given by object to the current 'running' array.

    Side effects include the updating of any manifest context.

    Args:
      object (Object): The object being added.
      objectInfo (dict): The object metadata being added.
      interactive (Boolean): Whether or not we are running the object interactively.
      context (dict): Contains context metadata for VM generation.
      person (Person): The current Person that is authenticated.

    Returns:
      dict: The added process manifest.
    """

    currentEnvironment['running'] = currentEnvironment.get('running', [])
    currentRunning = {}
    currentRunning['objects'] = []
    currentEnvironment['running'].append(currentRunning)

    # inputInfo will be the copy of the object description that gets placed in the task
    # It will have only the minimal information the deployment backend needs to deploy the task
    inputInfo = {
      "id":           objectInfo.get('id'),
      "uid":          objectInfo.get('uid'),
      "name":         objectInfo.get('name'),
      "type":         objectInfo.get('type'),
      "revision":     objectInfo.get('revision'),
      "environment":  objectInfo.get('environment'),
      "architecture": objectInfo.get('architecture'),
      "dependencies": []
    }

    # Capture the network effects
    if not isBuilding and 'network' in objectInfo.get('run', {}):
      inputInfo['network'] = objectInfo['run']['network']

    for key in ['run', 'build', 'init', 'provides', 'install', 'basepath',
                'owner', 'file', 'outputs', 'inputs', 'network', 'lock',
                'requested', 'metadata', 'summary', 'stage']:
      if key in objectInfo:
        inputInfo[key] = objectInfo[key]

    if interactive:
      inputInfo['interactive'] = True

    # Mark the index in this object
    objectIndex = context['objectIndex']
    while objectIndex in context['reservedIndicies']:
      objectIndex += 1
    inputInfo['index'] = objectIndex
    objectIndex += 1
    context['objectIndex'] = objectIndex

    # For every dependency of this environment, add it to the previous
    # environment

    # Resolving the dependencies
    resolved = self.resolveDependenciesFor(object, objectInfo, context, locks, person)

    # We bail if we cannot resolve the dependencies
    if resolved is not True:
      raise DependencyUnresolvedError(resolved, "Cannot find dependency")

    # Shrink the process information down now that we have resolved everything
    self.reduceProcess(objectInfo)

    # Form the running object's init section from dependencies
    initSection = {}

    dependencies = objectInfo.get('dependencies')

    for dependency in dependencies:
      dependencyInfo = {
        "id":       dependency.get('id'),
        "uid":      dependency.get('uid'),
        "name":     dependency.get('name'),
        "type":     dependency.get('type'),
        "revision": dependency.get('revision'),
      }

      # For direct dependencies, we copy certain metadata to the task
      for key in ['init', 'inject', 'version', 'build', 'owner',
                  'file', 'network', 'paths', 'lock', 'summary', 'install',
                  'provides', 'environment', 'architecture', 'requested']:
        if key in dependency:
          dependencyInfo[key] = dependency[key]

      # Add this object to the context of the running process
      objectIndex = context['objectIndex']
      while objectIndex in context['reservedIndicies']:
        objectIndex += 1
      dependencyInfo['index'] = objectIndex
      objectIndex += 1
      context['objectIndex'] = objectIndex

      # Keep track of the dependencies in the current object
      inputInfo['dependencies'].append(dependencyInfo.copy())

      # Combine init section with main section, using the currentEnvironment
      self.parseInitSection(dependency, initSection, currentEnvironment)

      # Adds this object to the VM
      currentRunning['objects'].append(dependencyInfo)

    # Add prior processes' init sections
    processes = currentEnvironment["running"]
    for process in processes[:-1]:
      processInfo = process['objects'][-1]
      self.parseInitSection(processInfo, initSection, currentEnvironment)

    # TODO: add mount paths HERE instead of at the end of the task generation

    # Parse the {{ tags }} within the object
    self.parseTagsInTask(inputInfo)

    if 'dependencies' in inputInfo and len(inputInfo['dependencies']) == 0:
      del inputInfo['dependencies']

    # Add our own init section
    self.parseInitSection(inputInfo, initSection, currentEnvironment)
    self.completeInitSection(initSection, currentEnvironment)
    if 'run' in inputInfo:
      inputInfo['run'].update(initSection)

    currentRunning['objects'].append(inputInfo)

    return inputInfo

  def taskFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run", arguments=None, buildTask=None, using=None, penalties=None, service=None, services=[], preferred=None, backend=None, locks=None, person=None):
    """ Generates a task manifest for the given object that will run the object
    on this system.

    Args:
    """

    reservedIndicies = []

    # Gather the reserved indicies
    for wireIndex, wire in enumerate(inputs or []):
      for input in wire:
        # Gather information about the input
        if isinstance(input, dict):
          if 'index' in input:
            reservedIndicies.append(input['index'])

    if penalties is None:
      penalties = {}

    # Look for any cached successful tasks and use that (even if the revision
    # is blank)

    if not isinstance(object, dict):
      objInfo = self.objects.infoFor(object)
      ownerInfo = self.objects.ownerInfoFor(object)
      revision = object.revision
    else:
      objInfo = object
      object = self.objects.retrieve(id=objInfo.get('id'), revision=objInfo.get('revision', revision), person=person)
      ownerInfo = {}

    objInfo = copy.deepcopy(objInfo)
    objInfo['id']  = object.id
    objInfo['uid'] = object.uid

    # Put build dependencies in with normal dependencies
    if section == "build":
      objInfo['init'] = {"dependencies": []}
      objInfo['init']['dependencies'].extend(objInfo.get('build', {}).get('dependencies', []))

      # Penalize resolving this object as its own dependency
      # (Unless it is locked in, which overrides this behavior)
      resolvedRevision = None
      if locks and objInfo['id'] in locks:
        resolvedRevision, resolvedVersion = self.versions.resolve(object, locks[objInfo['id']], penalties = penalties)

      if revision != resolvedRevision:
        penalties[objInfo['id']] = penalties.get(objInfo['id'], []) + [revision]

    # Gather object information
    objName = objInfo.get('name', 'unknown')
    objType = objInfo.get('type', 'object')

    runningObject = None

    usingInfo = objInfo
    if using:
      usingInfo = self.objects.infoFor(using)
      usingInfo['revision'] = using.revision
      usingInfo['id']       = using.id
      usingInfo['uid']      = using.uid

    sectionInfo = usingInfo.get(section, {})
    environment  = sectionInfo.get('environment',  usingInfo.get('environment'))
    architecture = sectionInfo.get('architecture', usingInfo.get('architecture'))

    # Gather providers and the native backend
    if environment == environmentGoal and architecture == architectureGoal:
      backendPath = []
    else:
      backendPath = self.backends.providerPath(environment, architecture, environmentGoal, architectureGoal, backend=backend, person = person)

    if backendPath is None:
      ManifestManager.Log.error("No suitable way to %s this object has been found" % (section))
      return None

    nativeBackend = None
    if len(backendPath) > 0:
      nativeBackend = backendPath[-1]

    if architectureGoal is None and environmentGoal is None and (nativeBackend is None or isinstance(nativeBackend, Object)):
      # No native backend. We failed.
      ManifestManager.Log.error("No backend found for %s on %s" % (environment, architecture))
      return None

    if isinstance(nativeBackend, Object) or nativeBackend is None:
      # The backend is still unknown, we are making a partial VM
      backendName = None
    else:
      # The backend is known. We are making a VM with the intent to run on
      # this backend.
      backendName = nativeBackend.__class__.name()

    # Once we have a native backend, build up the task manifest to run
    # on top of that backend
    from uuid import uuid1

    # Generate the base task object
    task = {
      "type": "task",
      "environment":  environmentGoal  or 'native',
      "architecture": architectureGoal or 'native',
      "backend": backendName,
      "id": str(uuid1())
    }

    if backendName:
      task["name"] = "Virtual Machine to %s %s with %s" % (section, objName, backendName)
    else:
      task["name"] = "Virtual Machine to %s %s on %s/%s" % (section, objName, environmentGoal, architectureGoal)

    # Keep track of the source of the task (workflow, etc)
    if generator:
      task['generator'] = self.objects.infoFor(generator)
      task['generator']['revision'] = generator.revision
      task['generator']['id']  = generator.id
      task['generator']['uid'] = generator.uid

    # We need to run each backend/provider to map the native backend to the environment
    # of the requested object.

    lastInput = None
    currentEnvironment = task
    currentInput = task

    # We need to keep track of any capabilities requested by objects that
    # haven't been fulfilled
    capabilities = set([])

    # The starting index for tagging unique objects in the VM
    objectIndex = indexStart

    objectInfo = copy.deepcopy(self.objects.infoFor(object))
    objectInfo['revision'] = object.revision
    objectInfo['id']       = object.id
    objectInfo['uid']      = object.uid
    if not object.link is None:
      # Retain information about the local stage being used
      objectInfo['stage'] = { 'id': object.link }

    if service:
      objectServices = objectInfo.get('services', {})
      if service not in objectServices:
        raise "Service not found"

      serviceInfo = objectServices[service]
      ManifestManager.deepMerge(objectInfo, serviceInfo)

    ownerObj = self.objects.ownerFor(object, person=person)
    if ownerObj.id != object.id:
      objectInfo['owner'] = objectInfo.get('owner', {
        "id": ownerObj.id,
        "uid": ownerObj.uid
      })

    if section == "build":
      objectInfo['init'] = {"dependencies": []}

    usingSections = []

    # Ensure the object we are running/building is using the section
    # asked for
    if section in objectInfo:
      objectInfo['dependencies'] = objectInfo.get('dependencies', []) + objectInfo.get('init', {}).get('dependencies', []) + objectInfo[section].get('dependencies', [])

      # Capture any other metadata.
      if 'metadata' in objectInfo[section]:
        objectInfo['metadata'] = objectInfo[section]['metadata']

      # Capture the network effects
      if 'network' in objectInfo[section]:
        objectInfo['network'] = objectInfo[section]['network']

      # Capture what file is selected to run
      if 'file' in objectInfo[section]:
        objectInfo['file'] = objectInfo[section]['file']

      # Add 'using' to dependencies
      if 'using' in objectInfo[section]:
        objectInfo['dependencies'] = objectInfo.get('dependencies', []) + [objectInfo[section]['using']]

      if buildTask:
        objectInfo['build'] = objectInfo.get('build', {})
        objectInfo['build']['id'] = buildTask.get('id')
        objectInfo['build']['revision'] = buildTask.get('revision')
        objectInfo['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

      if section != "run":
        objectInfo['run'] = objectInfo[section]
        del objectInfo[section]

    interactive = objectInfo.get('run', {}).get('interactive', False)

    # Ignore the backend class when we go through the objects
    lastIndex = -1
    if backendName is None:
      lastIndex = len(backendPath)

    # Get the provider objects as a list
    [self.objects.infoFor(obj).update({"id": obj.id, "uid": obj.uid}) for obj in backendPath[0:lastIndex]]

    # Enumerate through each object and append the 'revision' key
    for i, obj in enumerate(backendPath[0:lastIndex]):
      info = self.objects.infoFor(obj)
      info['revision'] = backendPath[i].revision

      ownerObj = self.objects.ownerFor(obj, person=person)
      if ownerObj.id != obj.id:
        info['owner'] = info.get('owner', {
          "id": ownerObj.id,
          "uid": ownerObj.uid
        })

    # For each provider, determine how to run each object
    providerList = backendPath[0:lastIndex]
    if using:
      providerList = [using] + providerList
    providerList = [object] + providerList
    for backendObject in reversed(providerList):
      if backendObject is object:
        backend = objectInfo
      elif backendObject is using:
        backend = usingInfo
      else:
        backend = self.objects.infoFor(backendObject)

      ManifestManager.Log.noisy("setting up %s @ %s" % (backend.get('name'), backend.get('revision')))

      # Add forced inputs
      if backend is objectInfo and inputs is not None and isinstance(inputs, list):
        ManifestManager.Log.write("Adding input to task")

        # Ready the list of inputs
        objectInfo['inputs'] = objectInfo.get('inputs', [])

        # Make sure the inputs are a list
        if not isinstance(objectInfo['inputs'], list):
          objectInfo['inputs'] = [objectInfo['inputs']]

        # For each input requested
        for wireIndex, wire in enumerate(inputs):
          for input in wire:
            # Gather information about the input
            inputInfo = input
            if not isinstance(input, dict):
              inputInfo = self.objects.infoFor(input)
              inputInfo['id']       = input.id
              inputInfo['uid']      = input.uid
              inputInfo['revision'] = input.revision
              if not input.link is None:
                # Retain information about the local stage being used
                inputInfo['stage'] = { 'id': input.link }

            if 'build' in inputInfo:
              if 'buildId' not in inputInfo:
                buildTask = self.retrieveBuildTaskFor(input, preferred, person = person)
                buildId = buildTask.get('id')

                inputInfo['buildId'] = buildId

            # Give the input a relative identifier
            while objectIndex in reservedIndicies:
              objectIndex += 1
            inputInfo['index'] = objectIndex
            objectIndex += 1

            # TODO: input dependencies??

            objectInfo['inputs'] = objectInfo.get('inputs', [])
            objectInfo['inputs'].extend([{}] * (wireIndex + 1 - len(objectInfo['inputs'])))
            objectInfo['inputs'][wireIndex]['connections'] = objectInfo['inputs'][wireIndex].get('connections', [])
            objectInfo['inputs'][wireIndex]['connections'].append({
              "id":       inputInfo.get('id'),
              "uid":      inputInfo.get('uid'),
              "type":     inputInfo.get('type'),
              "name":     inputInfo.get('name'),
              "revision": inputInfo.get('revision'),
              "index":    inputInfo.get('index'),
            })

            for key in ['stage', 'version', 'buildId', 'owner', 'file', 'network', 'dependencies', 'run', 'init', 'build', 'metadata']:
              if key in inputInfo:
                objectInfo['inputs'][wireIndex]['connections'][-1][key] = inputInfo[key]

      # Keep track of all dependencies within this environment
      # We will throw away duplicates
      dependenciesIndex = {}

      context = {'objectIndex': objectIndex, 'penalties': penalties, 'dependencies': {}, 'dependenciesIndex': dependenciesIndex, 'reservedIndicies': reservedIndicies}

      # Resolve each service requirement
      self.resolveServicesFor(backend, services = services,
                                       context  = context)

      # Adds a process to the current 'running' listing
      inputInfo = self.addProcessToTask(currentEnvironment, backendObject, backend, isBuilding = (backend is objectInfo and section == 'build'), buildTask = (buildTask if backend is objectInfo else None), interactive = interactive, context = context, locks = locks, person=person)
      objectIndex = context['objectIndex']

      # Follow the environment and then we will loop around and add objects to that
      # context as needed.

      # Environments are objects that will run dependencies. They are emulators or
      # OS environments.
      currentEnvironment['provides'] = {
        'environment':  inputInfo.get('environment'),
        'architecture': inputInfo.get('architecture')
      }

      # Add this to our task
      #currentInput['inputs'] = currentInput.get('inputs', [])

      ManifestManager.Log.noisy("Adding %s @ %s" % (backend.get('name'), backend.get('revision')))

      if 'provides' in inputInfo or lastInput is None:
        lastInput = inputInfo
        currentEnvironment['running'] = currentEnvironment.get('running', [])
        currentRunning = currentEnvironment['running'][len(currentEnvironment['running'])-1]
        currentEnvironment = currentRunning['objects'][len(currentRunning['objects'])-1]
      else:
        #currentInput['inputs'].append(inputInfo)
        # Go to the next element of our task
        #lastInput = currentInput
        #currentInput = currentInput['inputs'][len(currentInput['inputs'])-1]
        pass
      currentInput = inputInfo

      if 'provides' in inputInfo:
        currentEnvironment = inputInfo
        dependencies = {}

      capabilities |= set(backend.get('capabilities', []))

    # The last time this is assigned, it will indicate the index of the intended object
    runningObject = currentInput

    def printTaskListing(task, padding=''):
      for processes in task.get('running', []):
        processes = processes.get('objects', [])
        if processes:
          process = processes[len(processes)-1]
          print(padding + process.get('type') + " " + process.get('name'), process.get('run'))
          printTaskListing(process, padding + '  ')

    # Ensure 'command' is always an array
    if 'run' in currentInput and 'command' in currentInput['run']:
      if not isinstance(currentInput['run']['command'], list):
        currentInput['run']['command'] = [currentInput['run']['command']]

    # This will be a collection of all capabilities claimed by the objects needed to run
    capabilities |= set(objInfo.get('capabilities', []))

    # Now we need to satisfy the capabilities for the task

    capabilities |= set(needCapabilities)
    capabilities = capabilities - set(haveCapabilities)

    task['capabilities'] = list(capabilities)
    task['client'] = {}
    task['client']['capabilities'] = haveCapabilities
    task['client']['requires'] = needCapabilities
    providers = []
    ManifestManager.Log.noisy("Raking capabilities")
    for capability in task['capabilities']:
      if capability in haveCapabilities:
        continue
      ManifestManager.Log.write("Fulfilling capability '%s'" % (capability))
      objs = self.backends.providersFor(capability=capability)
      if len(objs) > 0:
        provider = self.objects.retrieve(objs[0].uid)
        providerInfo = self.objects.infoFor(provider)
        if not providerInfo.get('id') in providers:
          ManifestManager.Log.write("Found: %s" % (objs[0].name))
          capabilities |= set(providerInfo.get('capabilities', []))
          providers.append(providerInfo.get('id'))

          # Add provider object
          if not 'running' in task['running'][0]['objects'][0]:
            task['running'][0]['objects'][0]['running'] = [{}]
          if not 'objects' in task['running'][0]['objects'][0]['running'][0]:
            task['running'][0]['objects'][0]['running'][0]['objects'] = []
          task['running'][0]['objects'][0]['running'][0]['objects'].append(providerInfo)

          while objectIndex in reservedIndicies:
            objectIndex += 1

          task['running'][0]['objects'][0]['running'][0]['objects'][len(task['running'][0]['objects'][0]['running'][0]['objects'])-1]['index'] = objectIndex
          task['running'][0]['objects'][0]['running'][0]['objects'][len(task['running'][0]['objects'][0]['running'][0]['objects'])-1]['revision'] = provider.revision
          objectIndex += 1

          # TODO: the providers need to be ordered by dependency and capability relations
          #       for instance, fluxbox provides windowing but requires x11 first!
      else:
        raise Exception("Cannot Resolve Capability %s for %s" % (capability, task.get('name')))

    task['capabilities'] = list(capabilities)

    # Add Running object
    #if using is not None:
    if False:
      ManifestManager.Log.write("Adding running object to task")
      currentInput['running'] = objInfo.get('running', [])

      inputInfo = self.objects.infoFor(toRunObject)

      # Assign revision and ids
      inputInfo['id']       = toRunObject.id
      inputInfo['uid']      = toRunObject.uid
      inputInfo['revision'] = toRunObject.revision

      # Give the input a relative identifier
      while objectIndex in reservedIndicies:
        objectIndex += 1
      inputInfo['index'] = objectIndex
      objectIndex += 1

      # TODO: input dependencies??

      currentInput['running'].append({
        'objects': [inputInfo]})

    # TODO: move the above capability satisfier somewhere else (object manager??)
    # TODO: ensure that provider object capabilities are also satisfied

    while objectIndex in reservedIndicies:
      objectIndex += 1
    currentInput['index'] = objectIndex
    objectIndex += 1

    # Establish the 'volume' and 'mounted' paths for every object so they
    # know where they, and their inputs, exist in their environments
    basepath = None
    if 'backend' in task and task['backend'] is not None:
      if nativeBackend:
        basepath = {
          "id":           "backend",
          "environment":  task['provides'].get('environment'),
          "architecture": task['provides'].get('architecture'),
          "basepath":     nativeBackend.rootBasepath()
        }

    # Fill in the mount paths for each object in the task
    self.setVolumesInTask(task, basepath = basepath)

    # Parse the {{ tags }} within the object
    # TODO: get rid of this and do this on the object first chance (addProcessToTask)
    #printTaskListing(task)

    self.parseTagsInTask(task, removeUnknown = True)

    if section == "build":
      task['builds'] = runningObject
    else:
      task['runs'] = runningObject

    if interactive:
      task['interactive'] = interactive

    if 'provides' in task:
      del task['provides']

    # Add machine information
    task['metadata'] = task.get('metadata', {})
    task['metadata'].update(self.system.machineInfo())

    return task

  def printTask(self, task):
    """ Prints out the task information to the Log.
    """

    ManifestManager.Log.write("Task: %s" % task.get('name', 'unnamed'))

    def printTaskObject(obj):
      for wire in obj.get('inputs', []):
        for input in wire.get('connections', []):
          ManifestManager.Log.write("Input: %s" % input.get('name', 'unnamed'))
          if 'run' in input:
            ManifestManager.Log.write('Run: %s' % input['run'].get('command', input['run'].get('script')))

          printTaskObject(input)

    printTaskObject(task)

  def backendsFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run"):
    """ Returns a list of backends that can run the given object.
    """

    info = object
    if isinstance(object, Object):
      info = self.objects.infoFor(object)

    targetEnvironment  = info.get('environment')  or info.get(section, {}).get('environment')
    targetArchitecture = info.get('architecture') or info.get(section, {}).get('architecture')

    return self.backends.backendsFor(environment=targetEnvironment, architecture=targetArchitecture)

  def setVolumesInTask(self, objInfo, rootBasepath = None, basepath=None, rootMountPathIndex=None, mountPathIndex=None):
    """ This method takes a task's metadata and adds 'volume' and 'local' fields
    based on any 'basepath' fields it finds.
    """

    # Start at the root of the virtual machine and go through recursively
    # to any leaves.
    mountId = objInfo.get('id')
    if 'owner' in objInfo:
      mountId = objInfo['owner'].get('id', objInfo.get('id'))

    if rootBasepath is None:
      # TODO: get the rootbase path from the backend?
      rootBasepath = []

    if mountPathIndex is None:
      mountPathIndex = [0]

    if rootMountPathIndex is None:
      rootMountPathIndex = [0]

    # Set the volume and mounted for this entry
    if rootBasepath is not None and basepath is not None:
      rootMountPaths  = {}
      localMountPaths = {}
      for subBasepath in rootBasepath + [basepath]:
        rootMountPath = subBasepath.get('basepath', {}).get('mount', '/')
        if not isinstance(rootMountPath, str) and isinstance(rootMountPath, list):
          index = rootMountPathIndex[0]
          if index < len(rootMountPath):
            rootMountPath = rootMountPath[index]
            rootMountPathIndex[0] += 1
          else:
            # TODO: cannot create this task... place errors somewhere
            rootMountPath = "/"

        baseArchitecture = subBasepath.get('architecture', "unknown")
        baseEnvironment  = subBasepath.get('environment',  "unknown")

        rootMountPaths[baseArchitecture] = rootMountPaths.get(baseArchitecture, {})
        rootMountPaths[baseArchitecture][baseEnvironment] = rootMountPath

        localMountPath = subBasepath.get('basepath', {}).get('local', '/home/occam/local')

        localMountPaths[baseArchitecture] = localMountPaths.get(baseArchitecture, {})
        localMountPaths[baseArchitecture][baseEnvironment] = localMountPath

      mountPath = basepath.get('basepath', {}).get('mount', '/')
      if not isinstance(mountPath, str) and isinstance(mountPath, list):
        index = mountPathIndex[0]
        if index < len(mountPath):
          mountPath = mountPath[index]
          mountPathIndex[0] += 1
        else:
          # TODO: cannot create this task... place errors somewhere
          mountPath = "/"

      objInfo['paths'] = {
        "volume": rootMountPaths,
        "mount":  mountPath,
        "separator": basepath.get('basepath', {}).get('separator', '/'),

        "local":  localMountPaths,
        "localMount": basepath.get('basepath', {}).get('local', '/home/occam/local'),

        "taskLocal":  basepath.get('basepath', {}).get('taskLocal', '/home/occam/task'),

        "cwd": basepath.get('basepath', {}).get('cwd', basepath.get('basepath', {}).get('local', '/home/occam/task'))
      }

      if len(rootBasepath) > 0:
        objInfo["paths"]["mountLocal"] = rootBasepath[-1].get('basepath', {}).get('objectLocal', '/home/occam/local'),

    if 'provides' in objInfo:
      if rootBasepath is None:
        rootBasepath = []
      if basepath is not None:
        rootBasepath = rootBasepath[:]
        rootBasepath.append(basepath)
      basepath = {
        "id": objInfo.get('id'),
        "basepath": objInfo.get('basepath', (basepath or {}).get('basepath', {})),
        "architecture": objInfo.get('provides', {}).get('architecture'),
        "environment":  objInfo.get('provides', {}).get('environment'),
      }

    # Recursively go through the rest of the entries:
    inputs = objInfo.get('inputs', [])
    if isinstance(inputs, list):
      for wire in inputs:
        for input in wire.get('connections', []):
          self.setVolumesInTask(input, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    for subTask in objInfo.get('running', []):
      for inputObject in subTask.get('objects', []):
        self.setVolumesInTask(inputObject, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Do the outputs, too
    outputs = objInfo.get('outputs', [])
    if isinstance(outputs, list):
      for wire in outputs:
        for output in wire.get('connections', []):
          self.setVolumesInTask(output, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Dependencies
    for dep in objInfo.get('dependencies', []):
      self.setVolumesInTask(dep, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Update cwd
    if 'run' in objInfo and 'cwd' in objInfo['run']:
      objInfo['paths']['cwd'] = objInfo['run']['cwd']

    if 'run' in objInfo and 'script' in objInfo['run'] and 'command' not in objInfo['run']:
      objInfo['run']['command'] = objInfo['run']['script']

    if 'run' in objInfo and 'command' in objInfo['run']:
      if not isinstance(objInfo['run']['command'], list):
        objInfo['run']['command'] = [objInfo['run']['command']]

      if not objInfo['run']['command'][0].strip().startswith("/"):
        objInfo['run']['command'][0] = objInfo['paths']['mount'] + objInfo['paths']['separator'] + objInfo['run']['command'][0].strip()

  def updateTag(self, value, originalKey, root):
    # Determine the root of the key
    # Determine the relative key
    # Update the tag with relative keys

    # For each tag, reflect the relative key
    try:
      index = 0
      start = value.index("{{", index)

      while start >= 0:
        end   = value.index("}}", start)
        key   = value[start+2:end].strip()

        if key == "{{":
          # Escaped curly brace section
          replacement = "{{"
        else:
          try:
            path = self.parser.path(originalKey)

            # Determine the root
            newRoot = root
            current = root
            keyPrefix = ""
            currentPrefix = ""
            for subKey in path:
              current = current[subKey]
              if isinstance(subKey, int):
                currentPrefix = currentPrefix + "[" + str(subKey) + "]"
              else:
                currentPrefix = currentPrefix + "." + subKey

              if isinstance(current, dict) and "id" in current:
                newRoot = current
                keyPrefix = currentPrefix

            if keyPrefix.startswith("."):
              keyPrefix = keyPrefix[1:]

            okey = key
            if keyPrefix:
              key = keyPrefix + "." + key

            replacement = "{{ " + key + " }}"
          except:
            replacement = "{{ %s }}" % (key)

        if replacement is None:
          replacement = ""

        replacement = str(replacement)

        try:
          value = value[0:start] + replacement + value[end+2:]
        except:
          value = value

        # Adjust where we look next
        index = start + len(replacement)

        # Pull the index of the next {{ section
        # If there isn't one, it will fire an exception
        start = value.index("{{", index)

    except ValueError as e:
      pass

    if value is None:
      value = ""
    return value

  def parseTagsInTask(self, value, root=None, parent=None, environment=None, removeUnknown=False):
    """ This method goes through every entry in the task info and replaces curly
    brace tags with what they request.

    These replacements are completed to create the task manifest for creating
    a virtual machine. Each tag is based on the root of any object metadata.
    The root of an object is any section with an 'id' tag. The following shows
    how the changes made above are actually the same regardless of where the
    object is inserted into the task manifest as a whole.

    Examples:

      A section such as this::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample {{ paths.mount }}",
          "bar": "{{ paths.mount }}/usr/lib"
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
          "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
        }

      And, in a more complex example where some items are self referencing from their parent::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample {{ paths.mount }}",
              "bar": "{{ paths.mount }}/usr/lib"
            }
          ]
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
              "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
            }
          ]
        }

      As a special case, ``{{ {{ }}`` will escape double curly brackets,
      although double right curly brackets don't require it::

        {
          "foo": "./sample 'foo bar {{{{}}baz}}'"
        }

      Creates the task manifest with the curly brace intact::

        {
          "foo": "./sample 'foo bar {{baz}}'"
        }
    """

    if root is None:
      root = value

    if isinstance(value, dict):
      # Go through a chunk of metadata

      newRoot = root
      if 'id' in value:
        newRoot = value

      # Handle complex objects first
      for k, v in value.items():
        if isinstance(v, dict):
          self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

      # Then lists
      for k, v in value.items():
        if isinstance(v, list):
          self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

      # Then strings
      for k, v in value.items():
        if isinstance(v, str):
          value[k] = self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

    elif isinstance(value, list):
      # Go through a list of items

      i = 0
      for v in value:
        value[i] = self.parseTagsInTask(v, root=root, removeUnknown=removeUnknown)
        if value[i] == "" and v != "":
          # Delete empty strings in lists
          del value[i]
        else:
          i += 1

    elif isinstance(value, str):
      # This is a string, so we parse it for curly brace sections
      try:
        index = 0
        start = value.index("{{", index)

        while start >= 0:
          end   = value.index("}}", start)
          key   = value[start+2:end].strip()

          commands = []
          replaced = True

          if key == "{{":
            # Escaped curly brace section
            replacement = "{{"
          else:
            key, *commands = [x.strip() for x in key.split('|')]
            try:
              replacement = self.parser.get(root, key)
            except:
              replaced = False
              if removeUnknown:
                replacement = ""
              else:
                replacement = "{{ %s }}" % (key)

          if replacement is None:
            replacement = ""

          replacement = str(replacement)

          # Update keys *within* the replacement text
          replacement = self.updateTag(replacement, key, root)

          # If there are no more keys, then we're good to execute the commands
          if "{{" not in replacement:
            for command in commands:
              parts = [x.strip() for x in command.split(" ", 1)]
              func = parts[0]
              args = ""
              if len(parts) > 1:
                args = parts[1]

              import shlex

              # Parse args (splits on ' except when escaped)
              args = shlex.split(args)

              if func == "substring":
                if len(args) == 0:
                  args = [0]
                if len(args) == 1:
                  args.append(len(replacement))
                if len(args) > 2:
                  args = args[0:2]

                args = [int(x) for x in args]

                args[0] = min(args[0], len(replacement))
                args[1] = min(args[1], len(replacement))

                replacement = replacement[args[0]:args[1]]
              elif func == "basename":
                rmext = None
                if len(args) == 1:
                  import re
                  if args[0] == ".*":
                    rmext = re.compile(re.escape(".") + ".*$")
                  else:
                    rmext = re.compile(re.escape(args[0]) + "$")

                sep = root.get('basepath', {}).get('separator', "/")
                old = replacement

                if os.sep != sep:
                  repsep = "\\\\"
                  if os.sep == "/":
                    repsep = "\\/"
                  replacement = replacement.replace(os.sep, repsep).replace(sep, os.sep)

                replacement = os.path.basename(replacement)

                if rmext:
                  replacement = rmext.sub("", replacement)
              elif func == "extname":
                replacement = os.path.splitext(replacement)[1]
              elif func == "replace":
                replacement = replacement.replace(args[0], args[1])
              elif func == "dospath":
                # We can assume sep is a "\\" (whew!)
                # We can then split the paths up
                if os.sep != "\\":
                  tmppath = replacement.replace("/", "%/%").replace("\\", os.sep).replace("%/%", "\\/")

                # Shorten all directories and filenames when appropriate
                tmppath = tmppath.split(os.sep)
                newpath = []
                for subpath in tmppath:
                  subpath = subpath.replace(" ", "")
                  name, ext = os.path.splitext(subpath)
                  if len(name) > 8 or len(ext) > 4:
                    subpath = name[0:6] + "~1" + ext[0:4]
                  newpath.append(subpath)

                replacement = "/".join(newpath)
                replacement = replacement.replace("\\/", "%/%").replace(os.sep, "\\").replace("%/%", "/")

          try:
            value = value[0:start] + replacement + value[end+2:]
          except:
            value = value

          # Adjust where we look next
          index = start
          if not replaced:
            index = start + len(replacement)

          # Pull the index of the next {{ section
          # If there isn't one, it will fire an exception
          start = value.index("{{", index)

      except ValueError as e:
        pass

    if value is None:
      value = ""
    return value

  def rakeTaskObjects(self, objInfo):
    """ Returns an array of every object metadata for the given task metadata.
    """

    ret = []

    for wire in objInfo.get('inputs', []):
      for input in wire.get('connections', []):
        ret.append(input)
        ret += self.rakeTaskObjects(input)

    return ret

class ManifestError(Exception):
  """ Base class for all manifest errors.
  """
  pass

class DependencyUnresolvedError(ManifestError):
  """ Error is generated when a dependency cannot be resolved.
  """

  def __init__(self, objectInfo, message):
    self.objectInfo = objectInfo
    self.message = message

class BuildRequiredError(ManifestError):
  """ Error is generated when a build is required.
  """

  def __init__(self, objectInfo, requiredObject, message):
    self.objectInfo = objectInfo
    self.objectInfo['id'] = requiredObject.id
    self.objectInfo['uid'] = requiredObject.uid
    self.objectInfo['revision'] = requiredObject.revision
    self.objectInfo['version'] = requiredObject.version
    self.requiredObject = requiredObject
    self.message = message
