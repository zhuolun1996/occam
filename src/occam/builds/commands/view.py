# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import datetime, time

from occam.log      import Log
from occam.object   import Object
from occam.datetime import Datetime

from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.manager  import BuildManager

from occam.manager import uses

@command('builds', 'view',
  category      = 'Build Management',
  documentation = "View build log or files within a build.")
@argument("object", type="object", nargs="?", help="A buildable object")
@argument("task",   type="object", help="The build task")
@option("-a", "--all",    dest    = "list_all",
                          action  = "store_true",
                          help    = "includes all files within resources as well and will redirect to linked resources")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option("-s", "--start",  action  = "store",
                          dest    = "start",
                          type    = int,
                          default = 0,
                          help    = "the byte position to start reading from")
@option("-l", "--length", action  = "store",
                          dest    = "length",
                          type    = int,
                          help    = "the number of bytes to read")
@option("-c", "--compress", action = "store",
                            dest   = "compress",
                            help   = "compress the directory or file with the given type ('tgz', 'zip', 'txz')")
@uses(ObjectManager)
@uses(BuildManager)
class BuildsViewCommand:
  def viewBuild(self, obj, task):
    Log.header("Viewing build log")

    if self.options.compress:
      raise Exception("cannot compress build logs")

    data = self.builds.logFor(obj, task)

    if data is not None:
      Log.pipe(data)

    return 0

  def viewBuildData(self, obj, task, path):
    if self.options.compress == "txz":
      # Compress the build
      if path != "/":
        raise Exception("cannot compress individual files or directories with txz")

      tarpath = self.builds.compress(obj.uid, obj.revision, task.id)
      with open(tarpath, "rb") as f:
        Log.pipe(f)
    elif self.options.compress == "zip":
      import occam.vendor.zipstream as zipstream
      z = zipstream.ZipFile()

      # This iterator can chunk the file contents
      # But we lose the file stat because the zipstream cannot
      # handle adding that information
      class ChunkIterator:
        def __init__(self, f):
          self.__f = f

        def __iter__(self):
          return self

        def __next__(self):
          if self.__f is None:
            raise StopIteration

          if isinstance(self.__f, bytes):
            ret = self.__f
            self.__f = None
          else:
            ret = self.__f.read(1024)

          if len(ret) == 0:
            raise StopIteration

          return ret

      class OccamFileIterator:
        def __init__(self, builds, obj, curPath, includeResources, person):
          self.__obj = obj
          self.__curPath = curPath
          self.__includeResources = includeResources
          self.__person = person

          self.builds = builds

        def __iter__(self):
          return ChunkIterator(self.builds.retrieveFileFrom(self.__obj.uid, self.__obj.revision, task.id, self.__curPath))

      stat = self.builds.retrieveFileStatFrom(obj.uid, obj.revision, task.id, path)
      if stat and stat["type"] == "tree":
        # Go through the directory and pull each file
        def packPath(z, path):
          # Get directory listing
          listing = self.builds.retrieveDirectoryFrom(obj.uid, obj.revision, task.id, path)
          for item in listing.get('items'):
            curPath = os.path.join(path, item.get('name'))
            if item.get('type') == 'tree':
              packPath(z, curPath)
            else:
              utime = time.gmtime(Datetime.from8601(item.get('date')).timestamp())
              if not item.get('link'):
                z.write_iter(curPath, OccamFileIterator(self.builds, obj, curPath, includeResources = self.options.list_all, person = self.person), stat = item.get('mode'), utime=utime)
              else:
                z.write_iter(curPath, OccamFileIterator(self.builds, obj, curPath, includeResources = self.options.list_all, person = self.person), stat = item.get('mode'), utime=utime, link=True)

        packPath(z, path)
      else:
        z.write_iter(path, ChunkIterator(data))

      for zipdata in z:
        Log.pipe(zipdata)
    else:
      data = self.builds.retrieveFileFrom(obj.uid, obj.revision, task.id, path, start = self.options.start, length = self.options.length)
      Log.pipe(data, length=self.options.length)

    return 0

  def do(self):
    if self.options.object is None:
      obj = self.objects.resolve(SimpleNamespace(id       = "+",
                                                 revision = None,
                                                 version  = None,
                                                 uid      = None,
                                                 path     = None,
                                                 index    = None,
                                                 link     = None,
                                                 identity = None),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Could not find the object.")
      return -1

    task = self.objects.resolve(self.options.task,
                                person = self.person)

    if task is None:
      Log.error("Could not find the task.")
      return -1

    if self.options.task.path:
      return self.viewBuildData(obj, task, self.options.task.path)

    return self.viewBuild(obj, task)
