# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log    import loggable
from occam.object import Object
from occam.config import Config

from occam.manager import uses, manager

from occam.objects.manager import ObjectManager

@loggable
@uses(ObjectManager)
@manager("services")
class ServiceManager:
  """ This Occam manager organizes objects that can provide particular services.
  """

  def __init__(self):
    """ Initialize the service manager.
    """

  def retrieveAll(self, environment=None, architecture=None, service=None):
    """ Retrieves a list of service records based on the given filters.

    Args:
      environment (string): The environment to filter by.
      architecture (string): The architecture to filter by.
      service (string): The service name to filter by.

    Returns:
      list: A set of objects matching the request. Will be empty if none are found.
    """

    return self.datastore.retrieve(environment, architecture, service)

  def retrieve(self, environment, architecture, service):
    """ Retrieves a list of possible objects that offer the requested service.

    Args:
      environment (string): The environment to target.
      architecture (string): The architecture to target.
      service (string): The service requested.

    Returns:
      list: A set of objects matching the request. Will be empty if none are found.
    """

    records = self.datastore.retrieve(environment, architecture, service)

    return records
