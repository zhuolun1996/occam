# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.log    import Log

from occam.manager import uses

from occam.objects.manager import ObjectManager
from occam.people.manager import PersonManager

@command('people', 'view',
  category      = 'Person Information',
  documentation = "Lists the object metadata for the given identity.")
@uses(ObjectManager)
@uses(PersonManager)
@argument("identity", type = str, help = "The identity to query.")
@argument("object", type = "object", nargs="?", help = "The specific person object to use.")
class PeopleStatusCommand:
  def do(self):
    person = None
    if self.options.object:
      person = self.objects.resolve(self.options.object, person = self.person)
    else:
      person = self.people.retrieve(self.options.identity, person = self.person)

    if person is None:
      raise Exception("Person cannot be found.")
      return -1

    personInfo = self.objects.infoFor(person)

    ret = {
      "id": person.id,
      "uid": person.uid,
      "identity": person.identity
    }

    ret.update(personInfo)

    Log.output(json.dumps(ret))

    return 0
