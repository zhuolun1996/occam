# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.people.database import PersonDatabase

@datastore("people.write", reader=PersonDatabase)
class PersonWriteDatabase:
  """ Manages the database interactions for the private aspects of the Key component.
  """

  def update(self, identity, uuid):
    """ Creates or updates the record relating identity to the given Object id.

    Args:
      identity (str) The identity URI.
      uuid (str) The object id for the related "person"

    Returns:
      PersonRecord The updated record.
    """

    from occam.people.records.person import PersonRecord

    session = self.database.session()

    record = PersonRecord()
    record.identity_uri = identity
    record.id = uuid

    self.database.update(session, record)
    self.database.commit(session)

    return record
