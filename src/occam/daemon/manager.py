# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager

import select
import os

class socket_wrap():
  def __init__(self, socket):
    self.socket = socket
    self.buffer = self

  def write(self, data):
    self.socket.send(data)

  def flush(self):
    pass

  def read(self, amount):
    return self.socket.recv(amount)

  def fileno(self):
    return self.socket.fileno()

class WaitableEvent:
  """Provides an abstract object that can be used to resume select loops with
  indefinite waits from another thread or process. This mimics the standard
  threading.Event interface."""

  def __init__(self):
    self._read_fd, self._write_fd = os.pipe()

  def wait(self, timeout=None):
    rfds, wfds, efds = select.select([self._read_fd], [], [], timeout)
    return self._read_fd in rfds

  def isSet(self):
    return self.wait(0)

  def clear(self):
    if self.isSet():
      os.read(self._read_fd, 1)

  def set(self):
    if not self.isSet():
      os.write(self._write_fd, b'1')

  def fileno(self):
    """Return the FD number of the read side of the pipe, allows this object to
    be used with select.select()."""
    return self._read_fd

  def __del__(self):
    os.close(self._read_fd)
    os.close(self._write_fd)

@loggable
@manager("daemon")
class DaemonManager:
  """ Handles an OCCAM daemon.

  The OCCAM daemon runs as a process and can accept commands from a socket.

  Generally, one daemon would run per server to accept and issue commands for
  that server. For instance, a web server can spawn an occam daemon on a known
  local port and issue occam commands to handle requests.

  The port has a configurable default, but can be overwritten by a command line
  argument to the "occam daemon" command.
  """

  def __init__(self):
    """ Initializes the daemon.
    """

    self.initializeInfoDirectory()

  def defaultPort(self):
    """ Retrieves the default configured port.
    """

    # Get the port from configuration option daemon.port
    return self.configuration.get('port', 32000)

  def defaultHost(self):
    """ Retrieves the default configured hostname.
    """

    # Get the port from configuration option daemon.port
    return self.configuration.get('host', '127.0.0.1')

  def path(self):
    """ Returns the path to the daemon metadata.
    """

    from occam.config import Config
    import os

    basePath = Config.root()

    return os.path.join(basePath, "daemons")

  def initializeInfoDirectory(self):
    """ Initializes the directory for the metadata for running daemons.
    """

    import os

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)

  def stopClientThreads(self, max_wait_seconds):
    """ Triggers the stopEvent for each of the client threads started by the
    daemon, and waits up to max_wait_seconds for them to exit.

    Returns the thread objects of threads that did not exit within the allotted
    time.
    """
    import threading

    # Tell all client threads to gracefully stop.
    for thread in threading.enumerate():
      if hasattr(thread, "stopEvent"):
        thread.stopEvent.set()
        DaemonManager.Log.write(
          "Closing connection to %s:%s."
          % (thread.connectionAddr[0], thread.connectionAddr[1])
        )

    from datetime import datetime as dt
    import time

    # Check to see if all threads have gracefully exited at 1 second
    # intervals, up until the maximum wait time.
    start = dt.now()
    while (dt.now() - start).total_seconds() < max_wait_seconds:
      if not any(hasattr(t, "stopEvent") for t in threading.enumerate()):
        break

      time.sleep(1)

    # Don't get stuck waiting for blocked threads. Note the stall and move
    # on.
    stalledThreads = []
    for thread in threading.enumerate():
      if hasattr(thread, "stopEvent"):
        DaemonManager.Log.error(
          "Couldn't gracefully stop the thread created for %s:%s."
          % (
            thread.connectionAddr[0],
            thread.connectionAddr[1]
          )
        )
        stalledThreads.append(thread)

    return stalledThreads

  def run(self, host=None, port=None):
    """ Starts the server loop.
    """

    import io
    import os
    import json
    import sys
    import traceback

    from occam.network.manager import NetworkManager
    network = NetworkManager()

    if host is None:
      host = self.defaultHost()

    if port is None:
      port = self.defaultPort()

    try:
      port = int(port)
    except:
      DaemonManager.Log.error("Provided port is not a valid integer.")
      return -1

    # TODO: check port range to fit within 16-bits

    # Get pid for this process
    pid = os.getpid()

    # Write the daemon metadata file
    metadata = {}
    metadata['port'] = port
    metadata['host'] = host
    metadata['pid']  = pid

    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    metadata['cmd'] = cmd_check

    # Open socket
    import socket
    import sys
    import threading
    import codecs
    import select

    writer = codecs.getwriter('utf8')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    daemonRunning = True

    def clientThread(conn):
      stopEvent = threading.currentThread().stopEvent

      buffer = b''

      while True:
        readers, _, _ = select.select([stopEvent, conn], [], [])

        if len(readers) > 0 and stopEvent in readers:
          break

        if len(readers) > 0:
          if len(buffer) < 1024:
            buffer += conn.recv(1024)
          buffering = True
          line = ""
          while buffering:
            if b'\n' in buffer:
              (line, buffer) = buffer.split(b'\n', 1)
              try:
                line = codecs.decode(line, 'utf8').strip()
                break
              except:
                pass
            else:
              more = conn.recv(1024)
              if not more:
                buffering = False
              else:
                buffer += more

          if not buffer and buffering == False:
            DaemonManager.Log.write("Connection closed on the client's side.")
            break

          try:
            line = json.loads(line)
          except:
            continue

          if not isinstance(line, dict):
            continue

          # Build out the option (keyword) argument list
          #
          # Here, options is a dict where the key is the argument (i.e. "--set")
          #   and the value is a list of items. If the item itself is a list, it
          #   is the multiple values.
          #
          # options = {
          #   "--set": [['x','y','z'], ['a','b']]
          # }
          # These will be rendered as ("--set x y z --set a b")
          #
          # When the value is explicitly true and not a string, then the option
          # merely exists.
          # options = {
          #   "-j": true
          # }
          # Renders as ("-j")
          options = line.get("options", {})
          optionsArguments = []
          for key, arguments in options.items():
            if arguments is True:
              optionsArguments.append(key)
              continue

            if not isinstance(arguments, list):
              arguments = [arguments]

            for argumentList in arguments:
              optionsArguments.append(key)

              if not isinstance(argumentList, list):
                argumentList = [argumentList]

              optionsArguments.extend(argumentList)

          promoted = ""
          if line.get('promote'):
            promoted = "promoted "

          DaemonManager.Log.write("Received %scommand '%s %s \"%s\" -- %s'" % (promoted, line["component"], line["command"], "\" \"".join(optionsArguments), " ".join(line["arguments"])))

          # Get incoming data for use as stdin
          stdin = None
          if "stdin" in line:
            # TODO: wrap the network stream to zero-copy the data
            DaemonManager.Log.write("Looking at stdin for %s bytes" % (line["stdin"]))
            stdin = None
            if len(buffer) >= line["stdin"]:
              stdin = io.BytesIO(buffer[0:line["stdin"]])
              buffer = buffer[line["stdin"]:]
            else:
              stdin = io.BytesIO(buffer)

            buffer = b''

            stdin.seek(0, os.SEEK_END)
            stdinLength = stdin.tell()

            amountNeeded = line["stdin"] - stdinLength

            while stdinLength < line["stdin"]:
              DaemonManager.Log.write("Looking for remaining %s bytes" % (amountNeeded))
              data = conn.recv(amountNeeded)
              stdin.write(data)
              stdinLength = stdin.tell()
              amountNeeded = line["stdin"] - stdinLength

            stdin.seek(0, os.SEEK_SET)

          from occam.commands.manager import CommandManager
          commands = CommandManager()

          import shlex
          error = False
          errorMessage = ""
          code = 0

          if len(line["arguments"]) > 0:
            optionsArguments.append("--")

          invocation = [line["component"]]

          if line["command"]:
            invocation.append(line["command"])

          invocation.extend([*optionsArguments, *line["arguments"]])

          promote = line.get("promote", False)

          stdout = None
          if promote:
            stdout = socket_wrap(conn)
            stdin  = socket_wrap(conn)

            header = {
                'status': 'ok'
            }

            header = json.dumps(header).encode('utf8')
            conn.send((str(len(header)) + "\n").encode('utf8'))
            conn.send(header)
            output = b""
            conn.send((str(len(output)) + "\n").encode('utf8'))
            conn.send(output)

          try:
            code = commands.execute(invocation, storeOutput=not promote, stdin = stdin, stdout = stdout, allowLocalPerson = False)
            if code < 0:
              error = True
          except Exception as e:
            DaemonManager.Log.write("Error handling command (%s)" % (line))
            DaemonManager.Log.write("Error type: %s" % (sys.exc_info()[0]))
            traceback.print_exc()
            # TODO: fix these error codes?
            code = -1
            error = True
          except:
            DaemonManager.Log.write("Error handling command (%s)" % (line))
            DaemonManager.Log.write("Error type: %s" % (sys.exc_info()[0]))
            code = -1
            error = True

          # When we promote, the thread and socket are dedicated to a task
          # So, we will destroy the thread when the command finishes
          if promote:
            break
          else:
            # Send stdout
            output = DaemonManager.Log.storedOutput()
            output = output.read()

            errorMessage = DaemonManager.Log.errorMessage()
            if error:
              header = {
                  'status': 'error',
                  'message': errorMessage,
                  'code':    code
              }
            else:
              header = {
                  'status': 'ok'
              }

            header = json.dumps(header).encode('utf8')
            conn.send((str(len(header)) + "\n").encode('utf8'))
            conn.send(header)
            conn.send((str(len(output)) + "\n").encode('utf8'))
            conn.send(output)

      conn.close()
      DaemonManager.Log.write("Disconnected from %s: %s" % (threading.currentThread().connectionAddr[0], str(threading.currentThread().connectionAddr[1])))

    try:
      s.bind((host, port))
    except socket.error as msg:
      DaemonManager.Log.error("Could not bind the socket: (host: %s, port: %s) Error %s: %s" % (host, port, str(msg.errno), msg.strerror))
      return -1

    path = self.path()
    metadataFilename = os.path.join(path, "%s.json" % str(port))
    with open(metadataFilename, 'w+') as f:
      json.dump(metadata, f)

    s.listen(10)
    while True:
      try:
        conn, addr = s.accept()
        DaemonManager.Log.write("Connected with %s: %s" % (addr[0], str(addr[1])))

        # Note that these threads are spawned as daemon threads. This means
        # that if they end up being the only remaining threads, they will be
        # immediately stopped - potentially without their resources being
        # cleaned up. This prevents the case where the main thread goes down,
        # but the port remains allocated because one of these threads is
        # blocked and unable to join.
        thread = threading.Thread(
          target = clientThread, args = (conn,), daemon=True
        )
        thread.connection = conn
        thread.connectionAddr = addr
        thread.stopEvent = WaitableEvent()
        thread.start()
      except(KeyboardInterrupt):
        max_wait_seconds = 5
        DaemonManager.Log.write(
          "Receiving Keyboard Interrupt. Waiting up to %d seconds for client"
          "threads to stop. Send another interrupt to exit immediately."
          % (max_wait_seconds)
        )

        try:
          self.stopClientThreads(max_wait_seconds)
        except(KeyboardInterrupt):
          pass

        break

    DaemonManager.Log.write("Closing socket.")
    s.shutdown(socket.SHUT_RDWR)
    s.close()
    DaemonManager.Log.write("Socket closed.")

    # Clean up the daemon metadata file
    os.remove(metadataFilename)

  def start(self, host=None, port=None):
    """ Starts the daemon.

    This will spawn a separate process to run as a daemon, if it isn't running
    on the given port already. If a daemon is already running on that port, or
    the port is not open for any other reason, the call will fail.

    Returns True upon success and False upon failure.
    """

    if port is None:
      port = self.defaultPort()

    DaemonManager.Log.write("Starting daemon on port %s" % (port))

    path = self.path()

    from occam.daemon.daemon import Daemon

    # Move ourselves to a background process
    server = Daemon(workingDir=path, logFile="daemon")
    server.create()

    # Run the server
    self.run(host = host, port = port)

  def status(self, port=None):
    """ Gives the status of all running daemons on the system.

    If you pass along a port, you can filter the list to just the daemon running
    on that port.

    For each daemon, it will report a dictionary object with keys for the port
    and the uptime if applicable.
    """

    import json
    import os
    import time

    # Walk the daemon path and read and then report the metadata

    path = self.path()
    statuses = []
    try:
      filenames = os.listdir(path)
    except OSError:
      DaemonManager.Log.error("Failed to list contents of %s" % (path))
      return statuses

    for filename in filenames:
      if filename.endswith(".json"):
        # Read json data and fill in fields only we know such as uptime and if
        # the process is still running.
        try:
          with open(os.path.join(path, filename)) as f:
            data = json.load(f)
            if 'pid' in data.keys() and 'port' in data.keys():
              # Filter by port if specified.
              if not port is None and data['port'] != port:
                continue

              # Determine if the process is still live, and if live it's runtime.
              try:
                # Figure out our best guess of the runtime. Ideally we would be
                # using creation time, but that is filesystem dependent.
                # Note that if we used os.stat()'s st_ctime here the behavior
                # would be divergent on Windows and non-Windows platforms.
                uptime = time.time() - os.path.getmtime(
                  os.path.join("/", "proc", str(data["pid"]))
                )
                data['status'] = {'state': 'up', 'uptime_seconds': uptime}
              except OSError:
                data['status'] = {'state': 'down'}

              statuses.append(data)
        except OSError:
          pass

    return statuses

  def delete_daemon_instance_file(self, filename):
    try:
      os.remove(filename)
    except FileNotFoundError:
      pass
    except OSError as e:
      DaemonManager.Log.write(
        "Failed to remove %s: %s." % (filename, e)
      )
      return False

    return True

  def stop(self, port=None, graceful_exit_timeout_s=30):
    """ Stops the daemon.

    If the daemon fails to terminate gracefully via SIGINT within
    graceful_exit_timeout_s seconds a SIGKILL will be sent.

    Returns True if the daemon was running and was successfully stopped.
    """

    import json
    import os
    import time

    if port is None:
      port = self.defaultPort()

    # Open metadata
    path = self.path()
    data = {}
    try:
      filename = os.path.join(path, "%s.json" % (str(port)))
      with open(filename) as f:
        data = json.load(f)
    except FileNotFoundError:
      pass
    except Exception as e:
      DaemonManager.Log.write(
        "Failed to read json file %s: %s" % (filename, e)
      )
      return False

    pid = data.get('pid', None)
    cmd = data.get('cmd', None)

    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except FileNotFoundError:
      # The process is not running.
      self.delete_daemon_instance_file(filename)

    if pid is None or cmd is None or cmd_check is None:
      DaemonManager.Log.error(
        "Cannot find a running daemon on port %s." % (str(port))
      )
      return False

    if cmd != cmd_check:
      DaemonManager.Log.error(
        "Cannot safely kill the daemon process. The process was launched with "
        "an unexpected command and may not be a daemon."
      )
      return False

    DaemonManager.Log.write("Stopping server running on port %s" % (str(port)))

    import _signal
    try:
      print ("Attempting to gracefully stop the daemon...")
      # The daemon will stop accepting new connections and tell client threads
      # to start cleaning up when it receives a SIGINT. We do not send a
      # SIGTERM prior to SIGKILL because that will prevent the daemon from
      # performing graceful cleanup.
      os.kill(pid, _signal.SIGINT)

      # We want to give the daemon a chance to quit gracefully, but only
      # for a controlled amount of time.
      try:
        for attempt in range(0, graceful_exit_timeout_s):
          time.sleep(1)
          # Will throw ProcessLookupError if the process is no longer running.
          os.kill(pid, 0)

        DaemonManager.Log.write("Forcefully stopping the daemon...")

        # Ideally we would never have to do this. However, this fallback to
        # SIGKILL  means that after "occam daemon stop" the user won't have to
        # wait an unknown amount of time before "occam daemon start" will
        # successfully bind on the default port.
        #
        # I believe we can avoid the need for SIGKILL in the future by fixing
        # the daemon behavior when interacting with the frontend. For example,
        # as of 2020-04-14 when the front end is connected to the backend the
        # backend seemingly ignores SIGTERMs.
        #
        # See the discussion in the following threads:
        # https://unix.stackexchange.com/questions/8916/when-should-i-not-kill-9-a-process
        # https://stackoverflow.com/questions/690415/in-what-order-should-i-send-signals-to-gracefully-shutdown-processes
        os.kill(pid, _signal.SIGKILL)

        # Init does not have a hard deadline by which to kill a process. But in
        # practice this is a reasonable amount of time to wait before informing the user
        # the process is possibly a zombie.
        time.sleep(3)
        os.kill(pid, 0)
        DaemonManager.Log.write(
          "Server process %s running on port %s was potentially leaked while "
          "stopping the daemon." % (str(pid), str(port))
        )
      except ProcessLookupError:
        pass

      DaemonManager.Log.write("Server stopped.")
      self.delete_daemon_instance_file(filename)

      return True
    except ProcessLookupError:
      DaemonManager.Log.write("Server not actually running. Cleaning up.")
      self.delete_daemon_instance_file(filename)

    return False
