from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.daemon.manager  import DaemonManager

@command('daemon', 'stop',
  category      = 'Services',
  documentation = "Stops a running daemon.")
@option("-p", "--port", action = "store",
                        dest   = "port",
                        help   = "determines the port to start the daemon")
@uses(DaemonManager)
class DaemonCommand:
  """ This command stops the runningdaemon on the given port.
  """

  def do(self):
    """ Perform the command.
    """

    self.daemon.stop(port = self.options.port)
    return 0
