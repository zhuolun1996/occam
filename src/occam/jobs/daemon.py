# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess

from occam.log     import loggable

from occam.manager import uses

from occam.jobs.manager      import JobManager
from occam.databases.manager import DatabaseManager
from occam.caches.manager    import CacheManager

@loggable
@uses(JobManager)
@uses(DatabaseManager)
@uses(CacheManager)
class JobDaemon:
  """ The JobDaemon keeps track of running jobs as long as there are jobs.

  When all jobs are done, the JobDaemon can finally sleep. When a job is queued,
  the JobDaemon will run once more. This daemon simply dispatches work when it
  can (or queue it in an auxiliary scheduler that already exists on the machine)
  and keeps track of those running tasks.

  When a job completes, the daemon will respond by cleaning up the task and
  recording output, etc. If the job fails, the daemon will record this as well.

  Internally, job dependencies are kept and jobs are dispatched and scheduled
  such that this ordering is preserved.
  """

  def __init__(self):
    """ Initializes the job daemon.
    """
    self.initializeInfoDirectory()

  def path(self):
    """ Returns the path to the daemon metadata.
    """

    from occam.config import Config

    basePath = Config.root()

    return os.path.join(basePath, "job-daemon")

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None, start_new_session=False):
    import subprocess
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout,
                                     stdin=stdin,
                                     stderr=stderr,
                                     cwd=cwd,
                                     env=env,
                                     start_new_session=start_new_session)

  def idle(self, stdout=None):
    """ Retrieve a job record and schedule it.
    """

    import datetime
    import time
    import subprocess
    import json

    print("[%s] started daemon" % (datetime.datetime.today()))

    while(self.run(stdout)):
      print("[%s] ran" % (datetime.datetime.today()))

    print("[%s] finished queue" % (datetime.datetime.today()))
    return

  def initializeInfoDirectory(self):
    """ Initializes the directory for the metadata for running daemons.
    """

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)

  def run(self, stdout=None):
    import datetime
    import time
    import subprocess
    import json

    # Look for jobs, quit if there aren't any
    job = self.jobs.pull()
    if job is None:
      # Exit the loop when we can't find a job to run
      return False
    print("[%s] deploying job %s" % (datetime.datetime.today(), job.id))

    # Spawn a process to run this task through current scheduler extension
    # TODO: do through extension

    output = os.path.join(self.path(), "%s.log" % (job.id))
    closeFile = False
    if stdout is None:
      closeFile = True
      stdout = open(output, "wb+")

    metadataPath = os.path.join(self.path(), "%s.json" % (job.id))
    metadataFile = open(metadataPath, "w+")

    stdinPath = os.path.join(self.path(), "%s-stdin" % (job.id))

    # Create the stdin pipe
    if os.path.exists(stdinPath):
      os.remove(stdinPath)
    os.mkfifo(stdinPath)

    stdinFile = os.open(stdinPath, os.O_NONBLOCK | os.O_TRUNC | os.O_RDWR)

    if job.initialize:
      # Callback the component
      tagArgs = []
      if job.initialize_tag:
        tagArgs = [job.initialize_tag]
      command = ['occam', job.initialize, 'job-init', str(job.id)] + tagArgs
      print("[%s] job-initialize %s" % (datetime.datetime.today(), command))
      p = JobDaemon.popen(command, stdout=stdout, cwd=self.path())
      p.communicate()

      # If initialization fails, fail the run.
      if p.returncode != 0:
        self.failJob(job)
        print(
          "[%s] failed to initialize %s" % (
            datetime.datetime.today(), job.initialize
          )
        )
        print("[%s] failed to run command due to initialization failure")
        if closeFile:
          stdout.close()
        return False

    command = None
    command = ['occam', 'jobs', 'run', str(job.id)]
    if job.interactive:
      command.append('-i')
    print("[%s] running command %s" % (datetime.datetime.today(), command) )
    p = JobDaemon.popen(command, stdout=stdout, stdin=stdinFile, cwd=self.path(), start_new_session=True)

    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(p.pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    metadata = {
      "pid": p.pid,
      "command": ' '.join(p.args),
      "check": cmd_check
    }

    print("[%s] process %s" % (datetime.datetime.today(), metadata["pid"]))

    json.dump(metadata, metadataFile)
    metadataFile.close()

    p.communicate()
    if p.returncode != 0:
      self.failJob(job)
    else:
      self.completeJob(job, stdout)

    if closeFile:
      stdout.close()

    return True

  def failJob(self, job):
    self.Log.write("Job failed: %d" % job.id)
    self.jobs.failure(job.id)

  def completeJob(self, job, stdout):
    import datetime
    # If it is not "started" the job probably failed
    job = self.jobs.retrieve(status="started", job_id=job.id)
    if len(job) == 0:
      return
    else:
      job=job[0]
    self.jobs.finish(job)
    # After this is done, look for a finalize
    if job.finalize:
      # Callback the component
      tagArgs = []
      if job.finalize_tag:
        tagArgs = [job.finalize_tag]
      command = ['occam', job.finalize, 'job-done', str(job.id)] + tagArgs
      print("[%s] job-finalize %s" % (datetime.datetime.today(), command))
      p = JobDaemon.popen(command, stdout=stdout, cwd=self.path())
      p.communicate()
      if p.returncode == 0:
        print("[%s] job-finalized" % (datetime.datetime.today()))
      else:
        print("[%s] failed to finalize job" % (datetime.datetime.today()))

  def spawnWorker(self):
    """ Spawns a background job worker as a separate process.
    """

    import datetime

    command = ["occam", "jobs", "daemon", "-b"]

    # We must use stdin otherwise the occam command's stdin will be None.
    # The command manager expects to be able to set stdin = sys.stdin.buffer.
    p = JobDaemon.popen(command, cwd=self.path(), stdin=subprocess.PIPE)
    p.communicate()

    if p.returncode != 0:
      self.Log.error("[%s] Failed to spawn worker" % (datetime.datetime.today()))
      return p.returncode

    return 0

  def start(self):
    """ Starts the job daemon process.
    """

    from occam.daemon.daemon import Daemon

    path = self.path()

    try:
      pid = os.fork()
    except OSError as e:
      raise(Exception, "%s [%d]" % (e.strerror, e.errno))

    if (pid == 0):
      # The new child process
      jobDaemon = Daemon(workingDir=path, logFile="job-daemon", pty=True)
      jobDaemon.create()

      # Force reinitializing a database connection
      self.database.disconnect()

      # Force reinitializing a cache connection
      self.cache.disconnect()

      # Run the daemon
      self.idle()

      # Exit the process
      exit(0)

    return pid

  def stop(self):
    pass

  def restart(self):
    self.stop()
    self.start()
