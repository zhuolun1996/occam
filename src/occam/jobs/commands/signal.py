# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'signal',
    category = "Job Management",
    documentation = "Sends a running task a system signal.")
@argument("job_id", type=str, help = "The identifier for the job you wish to send a signal.")
@argument("signal", type=int, help = "The signal to send.")
@uses(JobManager)
@uses(ObjectManager)
class SignalCommand:
  """ Signals the given job.
  """

  def do(self):
    Log.header("Signalling job %s" % (self.options.job_id))

    taskObject = self.jobs.taskForJob(self.options.job_id, self.person)

    task = None
    if taskObject:
      task = self.objects.infoFor(taskObject)

    if task is None:
      Log.error("Cannot find task for job %s" % (self.options.job_id))
      return -1

    pid = self.jobs.pidForJob(job_id = self.options.job_id)

    if not pid:
      Log.write("No process found")

    # Have the backend kill the task
    task = self.jobs.taskInfoForJob(self.options.job_id)
    self.jobs.signal(task, self.options.signal)

    return 0
