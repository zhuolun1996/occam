# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.citations.manager import CitationManager, citationFormat

from occam.manager import uses

@citationFormat("bibtex")
class BibTeX:
  """ This plugin generates BibTeX citations.
  """

  def generate(self, info):
    """ Returns a string representing the BibTeX for the given citation keys.
    """

    from bibtexparser.bibdatabase import BibDatabase
    from bibtexparser.bwriter     import BibTexWriter

    from datetime import datetime
    import re

    info.update({"ENTRYTYPE": info.get('type', 'misc')})
    if 'type' in info:
      del info['type']

    info['ID'] = re.sub('[\W_]+', '-', str(info.get('year', 'unknown')) + "-" + (info.get('author', '').split(' ')[-1] or (info.get('kind') + "-" + info.get('title'))))

    # The bibtexparser package haaaaates when things are lists
    if 'keywords' in info:
      if isinstance(info['keywords'], list):
        info['keywords'] = ",".join(info['keywords'])

    # bibtexparser haaaaates anything not a string
    # (even when some things aren't strings)
    for k,v in info.items():
      info[k] = str(v)

    months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]

    # parse month
    if 'month' in info:
      if info['month'] in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]:
        info['month'] = months[int(info['month']) - 1]

    db = BibDatabase()
    db.entries = [info]

    if 'version' in info:
      info['note'] = info['version']

    writer = BibTexWriter(write_common_strings=True)
    ret = "```\n" + writer.write(db) + "```"

    for month in months:
      ret = ret.replace("month = {%s}" % (month), "month = %s" % (month))

    return ret
