# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager   import ObjectManager
from occam.manifests.manager import ManifestManager
from occam.discover.manager  import DiscoverManager
from occam.backends.manager  import BackendManager

from occam.commands.manager   import command, option, argument

import json

@command('objects', 'providers',
  category      = 'Running Objects',
  documentation = "Returns a list of objects that provide the requested environment")
@argument("object", type = "object", nargs = '?')
@option("-j", "--json",         dest    = "to_json",
                                action  = "store_true",
                                help    = "returns result as a json document")
@option("-e", "--environment",  dest    = "environment",
                                action  = "store",
                                help    = "the environment to query")
@option("-a", "--architecture", dest    = "architecture",
                                action  = "store",
                                help    = "the architecture to query")
@option("-d", "--discover", dest   = "discover",
                            action = "store_true",
                            help   = "query the federation when no result is found locally")
@uses(ObjectManager)
@uses(DiscoverManager)
@uses(BackendManager)
class ObjectViewersCommand:
  """ This class will handle gathering a list of provider objects for the requested environment/architecture pair.

  Retrieve the possible provider objects that can run this object:
  occam objects providers QmaDnmWbTyibktXANJ3LJZzd8TPir9vruxbtberyCrpbkq@95ada4dd719f4de146c66871ede425201821ca50

  Retrieve the possible provider objects that can run any object with the given environment/architecture
  occam objects providers --environment dos --architecture x86
  """

  def do(self, recursive=False):
    # Query for object by id (if necessary)
    info = None
    if not (self.options.environment and self.options.architecture):
      obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
      if obj is None:
        if self.options.object:
          info = self.discover.retrieveJSON(self.options.object, person = self.person)
        else:
          Log.error(key="occam.objects.errors.localObjectNotFound")
          return -1

        if info is None:
          Log.error(key="occam.objects.errors.specifiedObjectNotFound", id=self.options.object.id)
          return -1

    environment = None
    architecture = None

    if self.options.environment:
      environment = self.options.environment
    else:
      if info is None:
        info = self.objects.infoFor(obj)

      environment = info.get('environment')

    if self.options.architecture:
      architecture = self.options.architecture
    else:
      if info is None:
        info = self.objects.infoFor(obj)

      architecture = info.get('architecture')

    # We will return this array of objects
    providers = []

    # Get a list of providers
    providers = self.backends.providersFor(environment = environment, architecture = architecture)

    ret = [{
      "id":           obj.id,
      "uid":          obj.uid,
      "identity":     obj.identity_uri,
      "revision":     obj.revision,
      "name":         obj.name,
      "summary":  obj.description,
      "type":         obj.object_type,
      "subtype":      obj.subtype,
      "architecture": obj.architecture,
      "organization": obj.organization,
      "environment":  obj.environment,
    } for obj in providers]

    # Do some discovery of providers within the federation, perhaps
    if self.options.discover:
      for obj in self.discover.providersFor(environment = environment, architecture = architecture):
        ret.append({
          "id":           obj.id,
          "uid":          obj.uid,
          "identity":     obj.identity_uri,
          "revision":     obj.revision,
          "name":         obj.name,
          "summary":  obj.description,
          "type":         obj.object_type,
          "subtype":      obj.subtype,
          "architecture": obj.architecture,
          "organization": obj.organization,
          "environment":  obj.environment,
          "remote":       True
        })
    Log.output(json.dumps(ret))

    return 0
