# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object         import Object
from occam.object_info    import ObjectInfo

from occam.log import Log

from occam.manager import uses

from occam.network.manager        import NetworkManager
from occam.versions.write_manager import VersionWriteManager
from occam.nodes.manager          import NodeManager
from occam.links.manager          import LinkManager
from occam.backends.manager       import BackendManager
from occam.discover.manager       import DiscoverManager
from occam.objects.write_manager  import ObjectWriteManager
from occam.objects.manager        import ObjectJSONError
from occam.permissions.manager    import PermissionManager
from occam.keys.write_manager     import KeyWriteManager, KeySignatureExistsError

from occam.commands.manager import command, option, argument

@command('objects', 'pull',
  category      = "Object Discovery",
  documentation = "Pulls an object from a specific URL or path.")
@argument("object", 
          action  = "store",
          type    = "object",
          default = ".",
          nargs   = "?")
@option("-f", "--force",
        action = "store_true",
        dest   = "force",
        help   = "pull even if the object is already known")
@option("-a", "--all",
        action = "store_true",
        dest   = "pull_all",
        help   = "will pull each known tag")
@option("-t", "--task",
        action = "store_true",
        dest   = "pull_task",
        help   = "will pull any necessary objects needed to run this object")
# TODO: Add option to suppress the pulling of a build binary
@option("--no-build",
        action = "store_true",
        dest   = "no_build",
        help   = "will only pull object metadata and not pull down a built binary.")
@option("-b", "--with-build-dependencies",
        action = "store_true",
        dest   = "with_build_dependencies",
        help   = "will pull down build dependencies as well.")
@uses(NetworkManager)
@uses(VersionWriteManager)
@uses(NodeManager)
@uses(LinkManager)
@uses(DiscoverManager)
@uses(BackendManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
@uses(KeyWriteManager)
class PullCommand:
  def pull(self, obj, link=None, suppressOutput=False):
    # Pull any objects determined by other nodes to be necessary to build/run
    # this one
    revision = obj.revision

    if self.options.pull_task:
      # TODO: update for more than one backend
      docker_backend = self.backends.handlerFor('docker')
      backend = docker_backend.provides()[0]
      objInfo = obj.objectInfo();
      hypotheticalTask = self.nodes.taskFor(backend[0], backend[1],
                           objInfo.get('environment'),
                           objInfo.get('architecture'))
      objs = self.occam.taskObjectsFor(hypotheticalTask)
      for discoveredObj in objs:
        Log.write(key="occam.objects.commands.pull.discovered", type=discoveredObj.get('type'), name=discoveredObj.get('name'))

        # Pull this object
        self.nodes.pullAllObjects(discoveredObj.get('id'))

    # Store the object
    self.objects.write.store(obj, self.person.identity)

    # Restore the revision
    obj.revision = revision

    # Discover the resources
    resources = self.objects.write.pullResources(obj, self.person.identity)

    # Sign the stored object
    try:
      id, signature, published = self.keys.write.signObject(obj, self.person.identity)
      self.keys.write.store(obj, signature, self.person.identity, id, published)
      Log.write(key="occam.objects.commands.pull.signed", identity=self.person.identity)
    except KeySignatureExistsError as e:
      pass

    # For introducing objects, they are public-by-default
    if self.options.object.id == "." and self.options.object.link is None:
      self.markAsPublic(obj)
      self.announce(obj)

    objectInfo = self.objects.infoFor(obj)

    # If this object is from a local stage, update the record for that stage
    if link:
      # We need to update links
      self.links.updateLocalLink(link, obj.root.revision)

    # Output the new information
    ret = {}
    ret["updated"] = []

    for x in (obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": obj.id,
      "uid": obj.uid,
      "revision": obj.revision,
      "position": obj.position,
    })

    if not suppressOutput:
      import json
      Log.output(json.dumps(ret))

    if self.options.object.link:
      Log.done(key="occam.objects.commands.pull.changes", type=objectInfo.get('type'), name=objectInfo.get('name'), id=obj.id)
    else:
      Log.done(key="occam.objects.commands.pull.pulled", type=objectInfo.get('type'), name=objectInfo.get('name'), id=obj.id)
    return 0

  def markAsPublic(self, obj):
    self.permissions.update(id = obj.id, canRead=True, canWrite=False, canClone=True)

  def announce(self, obj):
    objectInfo = self.objects.infoFor(obj)

    self.discover.announce(obj.id, self.objects.idTokenFor(obj, obj.identity or self.person.identity))

    # Announce subObjects
    for subObject in objectInfo.get('includes', []):
      subObjectId = self.objects.idFor(obj, obj.identity or self.person.identity, subObjectType = subObject.get('type'), subObjectName = subObject.get('name'))
      subObjectToken = self.objects.idTokenFor(obj, obj.identity or self.person.identity, subObjectType = subObject.get('type'), subObjectName = subObject.get('name'))
      self.discover.announce(subObjectId, subObjectToken)

    # Announce each viewer
    objectInfo['views'] = objectInfo.get('views', [])

    if not isinstance(objectInfo['views'], list):
      objectInfo['views'] = [objectInfo['views']]

    for viewer in objectInfo.get('views', []):
      self.discover.announceViewer(viewer.get('type'), viewer.get('subtype'))

    for editor in objectInfo.get('edits', []):
      self.discover.announceEditor(editor.get('type'), editor.get('subtype'))

    environment = objectInfo.get('provides', {}).get('environment')
    architecture = objectInfo.get('provides', {}).get('architecture')

    if environment and architecture:
      # announce a provider
      self.discover.announceProvider(environment, architecture)

  def do(self):
    if (self.person is None or not hasattr(self.person, 'id')) and (self.options.object is None or self.options.object.id == "."):
      Log.error("Must be authenticated to store new objects")
      return -1

    Log.header(key="occam.objects.commands.pull.header")

    git_path = None

    # Grab the object from the command line argument
    try:
      obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    except ObjectJSONError as e:
      Log.error("Invalid object.json")
      Log.write(e.report)
      Log.output(e.context, padding="")
      return -1

    if (obj is None or (self.options.force and self.options.object.id != ".")) and self.options.object:
      # Attempt to discover this object
      Log.write("Cannot find the object locally; trying to discover")
      obj = self.discover.discover(uuid = self.options.object.id, revision = self.options.object.revision, version = self.options.object.version, withBuild = not self.options.no_build, withBuildDependencies = self.options.with_build_dependencies)
      if obj:
        Log.write("Object discovered")

        self.markAsPublic(obj)
        self.announce(obj)
        return 0

    if obj is None:
      Log.error("Object not found")
      return -1

    if obj and self.options.object.id != "." and self.options.object.link is None:
      Log.write("Object already exists")
      self.announce(obj)

      ret = {}
      ret["updated"] = []

      import json
      Log.output(json.dumps(ret))

      return 0

    link = None
    if obj.link:
      # We need a reference to the local link
      links = self.links.retrieveLocalLinks(obj.root, link_id = int(obj.link))

      if links:
        link = links[0]

        # We need to commit the staged changes,
        # but we also need to recursively consider all contained objects which
        # also may have changed. The "commitLocal" function does this.
        objects = self.objects.write.commitLocal(obj, self.person.identity)

        for object in objects[1:]:
          self.pull(object, suppressOutput=True)

    if self.options.pull_all:
      # Pull each revision along with the normal one
      tagsPath = os.path.join(".occam", "tags")
      tags = {}
      import json
      try:
        with open(tagsPath, "r") as f:
          tags = json.load(f)
      except:
        pass

      for tag,revision in tags.items():
        Log.write("Pulling tag %s" % (tag))
        self.options.object.revision = revision
        try:
          taggedObj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
        except ObjectJSONError as e:
          Log.error("Invalid object.json")
          Log.write(e.report)
          Log.output(e.context, padding="")
          return -1

        self.pull(taggedObj)

        # Look at whether or not the tag is already stored
        versions = self.versions.retrieve(taggedObj)
        versions = list(filter(lambda x: x.tag == tag, versions))
        versions = [version.revision for version in versions]

        if versions:
          if taggedObj.revision not in versions:
            Log.warning("Tag %s already exists." % (tag))
          else:
            Log.warning("The tag %s is already set to this revision." % (tag))
        else:
          # Tag the version in the note store
          signature = None
          published = None
          try:
            signature, published = self.keys.write.signTag(taggedObj, self.person.identity, tag)
            Log.write("Signed as %s" % (self.person.identity))
          except KeySignatureExistsError as e:
            pass

          if signature and published:
            self.versions.write.update(taggedObj, tag, self.person.identity, published, signature)

    return self.pull(obj, link)
