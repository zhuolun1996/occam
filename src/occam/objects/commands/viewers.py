# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager   import ObjectManager
from occam.manifests.manager import ManifestManager
from occam.discover.manager  import DiscoverManager

from occam.commands.manager   import command, option, argument

import json

@command('objects', 'viewers',
  category      = 'Running Objects',
  documentation = "Returns a list of objects that may be used for viewing the given object.")
@argument("object", type = "object", nargs = '?')
@option("-j", "--json",    dest    = "to_json",
                           action  = "store_true",
                           help    = "returns result as a json document")
@option("-s", "--subtype", dest    = "subtype",
                           action  = "append",
                           help    = "the subtype to query")
@option("-t", "--type",    dest    = "type",
                           action  = "append",
                           help    = "the type to query")
@option("-d", "--discover", dest   = "discover",
                            action = "store_true",
                            help   = "query the federation when no result is found locally")
@uses(ObjectManager)
@uses(DiscoverManager)
class ObjectViewersCommand:
  """ This class will handle gathering a list of viewer objects for the given object.

  Retrieve the possible viewer objects that can view this object:
  occam objects viewers QmaDnmWbTyibktXANJ3LJZzd8TPir9vruxbtberyCrpbkq@95ada4dd719f4de146c66871ede425201821ca50
  """

  def do(self, recursive=False):
    # Query for object by id (if necessary)
    info = None
    if not (self.options.subtype and self.options.type):
      obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
      if obj is None:
        if self.options.object:
          info = self.discover.retrieveJSON(self.options.object, person = self.person)
        else:
          Log.error(key="occam.objects.errors.localObjectNotFound")
          return -1

        if info is None:
          Log.error(key="occam.objects.errors.specifiedObjectNotFound", id=self.options.object.id)
          return -1

    subtypes = None

    if self.options.subtype:
      subtypes = self.options.subtype
    else:
      if info is None:
        info = self.objects.infoFor(obj)
      subtypes = info.get('subtype')

    if subtypes is None:
      subtypes = []

    if not isinstance(subtypes, list):
      subtypes = [subtypes]

    type = None
    if self.options.type:
      type = self.options.type[0]
    else:
      if info is None:
        info = self.objects.infoFor(obj)
      type = info.get('type')

    # We will return this array of objects
    viewers = []

    # First get the viewers that are very specific
    for subtype in subtypes:
      viewers.extend(self.objects.viewersFor(viewsType    = type,
                                             viewsSubtype = subtype))

    # Get the generic viewers
    viewers.extend(self.objects.viewersFor(viewsType = type))

    for subtype in subtypes:
      viewers.extend(self.objects.viewersFor(viewsSubtype = subtype))

    def normalize(obj, remote=False):
      normalized = {
        "id":           obj.id,
        "uid":          obj.uid,
        "identity":     obj.identity_uri,
        "revision":     obj.revision,
        "name":         obj.name,
        "summary":      obj.description,
        "type":         obj.object_type,
        "subtype":      obj.subtype,
        "architecture": obj.architecture,
        "organization": obj.organization,
        "environment":  obj.environment,
      }

      if remote:
        normalized["remote"] = True

      return normalized

    used = []
    viewers = filter(lambda x: used.append(x.id + "@" + x.revision) is None if x.id + "@" + x.revision not in used else False, viewers)
    ret = [normalize(obj) for obj in viewers]

    # Do some discovery of viewers within the federation, perhaps
    if self.options.discover:
      viewers = []
      for subtype in subtypes:
        viewers.extend(self.discover.viewersFor(type = type, subtype = subtype))
      viewers.extend(self.discover.viewersFor(type = type))
      viewers = filter(lambda x: used.append(x.id + "@" + x.revision) is None if x.id + "@" + x.revision not in used else False, viewers)

      ret.extend([normalize(obj, remote=True) for obj in viewers])

    Log.output(json.dumps(ret))

    return 0
