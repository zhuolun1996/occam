cp -r $1/$2 .
cp $1/ingest.py .
cp $1/parse-wheel.py .
cp $1/parse-toml.py .
cp $1/parse-setup-cfg.py .

mkdir -p path2
rm -f path2/python2
ln -s /usr/bin/python2 path2/python 2>/dev/null
mkdir -p path3
rm -f path3/python2
ln -s /usr/bin/python3 path3/python 2>/dev/null

echo "{\"requirements\": "
if [ "$3" == "2" ]; then
  # Always try to use the old method for disgusting Python 2
  /bin/env PATH=$PWD/path2:$PATH python2 ingest.py $2 "${@:4}" > /dev/null 2> /dev/null
else
  # Well, we also need to support PIP518
  # This is the pyproject.toml file... Yuck.
  if [ -f $2/pyproject.toml ]; then
    /bin/env PATH=$PWD/path3:$PATH python3 parse-toml.py $2/pyproject.toml "${@:4}"
  fi

  if [ -f $2/setup.cfg ]; then
    # Gather potential setup requirements
    /bin/env PATH=$PWD/path3:$PATH python3 parse-setup-cfg.py $2/setup.cfg "${@:4}"
  fi

  if [ ! -f _output ]; then
    # The New Method (build a wheel and parse result) (requires wheel)
    /bin/env PATH=$PWD/path3:$PATH python3 -m pip wheel -vvv --no-index --no-deps --no-cache-dir --disable-pip-version-check --no-build-isolation --no-clean $2 $PIPOPTS > /dev/null 2> /dev/null
    
    # If pip failed... just accept facts
    if [ ! -f *.whl ]; then
      echo "[]" > _output
    else
      cp *.whl wheel.zip
      mkdir -p wheel
      cd wheel
      unzip ../wheel.zip > /dev/null 2> /dev/null
      /bin/env PATH=$PWD/path3:$PATH python3 ../parse-wheel.py *.dist-info/METADATA > ../_output
      cd ..
      rm -rf wheel.zip wheel
    fi

    # The old (non-wheel) method uses pip internals
    #/bin/env PATH=$PWD/path3:$PATH python3 ingest.py $2 "${@:4}" > /dev/null 2> /dev/null
  fi
fi
cat _output
echo "}"
