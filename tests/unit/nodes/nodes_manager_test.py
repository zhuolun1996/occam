import unittest

from occam.nodes.manager import NodeManager

from unittest.mock import patch, Mock, MagicMock, mock_open,create_autospec
from urllib.parse import urlparse

class TestNodesManager:

  class Obj():
    def __init__(self,http_ssl=1,pull=True):
      self.http_ssl = http_ssl
      self.host = 'host'
      self.http_port = 'http_port'
      self.name = 'name'
      self.fakePull = pull
    
    def pull(self,objectPath,uuid,revision,cert):
      return self.fakePull
  
  class TestPartialFromURL(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
    
    def test_should_None_when_given_url_is_None(self):
      self.nodeManager.urlFromPartial = Mock(return_value = "mockUrlFromPartial")
      partialFromURL = self.nodeManager.partialFromURL(None)
      self.assertEqual(partialFromURL,None)
    
    def test_should_assert_urlFromPartial_is_called_with_empty_url_when_parameter_url_is_None(self):
      self.nodeManager.urlFromPartial = Mock(return_value = None)
      self.nodeManager.partialFromURL(None)
      self.nodeManager.urlFromPartial.assert_called_with("")

    def test_should_string_containing_localhost_and_port_when_port_not_equal_80_and_not_equal_to_443(self):
      self.nodeManager.urlFromPartial = Mock(return_value = "htpp://localhost:8090/to/somewhere?v=123")
      partialFromURL = self.nodeManager.partialFromURL("htpp://localhost:8080/mockPath/to/somewhere?v=123")
      self.assertEqual(partialFromURL,"localhost,8090")

    def test_should_assert_urlFromPartial_is_called_with_given_url_not_None(self):
      self.nodeManager.urlFromPartial = Mock(return_value = "htpp://localhost:8090/to/somewhere?v=123")
      self.nodeManager.partialFromURL("htpp://localhost:8080/mockPath/to/somewhere?v=123")
      self.nodeManager.urlFromPartial.assert_called_with("htpp://localhost:8080/mockPath/to/somewhere?v=123")

  class TestUrlFromPartial(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()

    def test_should_return_None_when_url_is_empty_string(self):
      urlFromPartial = self.nodeManager.urlFromPartial(None)
      self.assertEqual(urlFromPartial,None)

    def test_should_return_string_when_url_is_proper_link_and_scheme_not_empty(self):
      urlFromPartial = self.nodeManager.urlFromPartial("http://localhost:8080/mockPath/to/somewhere?v=123")
      self.assertEqual(urlFromPartial,"http://localhost:8080/mockPath/to/somewhere?v=123")

    def test_should_return_string_when_url_is_missing_scheme_(self):
      urlFromPartial = self.nodeManager.urlFromPartial("youtube.com/somevideo?v=123")
      self.assertEqual(urlFromPartial,"https://youtube.com/somevideo?v=123")

  class TestHostAndPortFromURL(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()

    def test_should_return_list_of_length_2(self):
      hostAndPortFromURL = self.nodeManager.hostAndPortFromURL("http://localhost:8080/mockPath/to/somewhere?v=123")
      self.assertIsInstance(hostAndPortFromURL,list)
      self.assertEqual(2,len(hostAndPortFromURL))

    def test_should_return_host_of_url_at_first_position_of_returned_list(self):
      hostAndPortFromURL = self.nodeManager.hostAndPortFromURL("http://localhost:8080/mockPath/to/somewhere?v=123")
      self.assertEqual(hostAndPortFromURL[0],"localhost")

    def test_should_return_port_of_url_at_second_position_of_list_and_port_equals_to_80_when_doubleDots_not_in_netloc_and_scheme_not_https(self):
      hostAndPortFromURL = self.nodeManager.hostAndPortFromURL("http://www.mockMe.com/mockPath/to/somewhere?v=123")
      self.assertEqual(hostAndPortFromURL[1],"80")

    def test_should_return_port_of_url_at_second_position_of_list_and_port_equals_to_port_given_in_url_when_doubleDots_in_netloc_and_scheme_not_https(self):
      hostAndPortFromURL = self.nodeManager.hostAndPortFromURL("http://localhost:8080/mockPath/to/somewhere?v=123")
      self.assertEqual(hostAndPortFromURL[1],"8080")

    def test_should_return_port_of_url_at_second_position_of_list_and_port_equals_443_when_doubleDots_not_in_netloc_and_scheme_is_https(self):
      hostAndPortFromURL = self.nodeManager.hostAndPortFromURL("https://www.mockMe.com/mockPath/to/somewhere?v=123")
      self.assertEqual(hostAndPortFromURL[1],"443")

  class TestSearch(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.urlFromPartial = Mock(return_value = 'fakeUrlFromPartial')
      self.nodeManager.hostAndPortFromURL = Mock(return_value = ('fakeHostAndPortFromURLHost','fakeHostAndPortURLPort'))
      self.nodeManager.datastore.retrieve = Mock()
    
    @patch('occam.nodes.records.node.NodeRecord')
    def test_should_return_object_of_type_NodeRecord(self,NoteRecord):
      noteRecord = NoteRecord()
      self.nodeManager.datastore.retrieve = Mock(return_value = noteRecord)
      search = self.nodeManager.search('fakeURL')
      self.assertIs(search,noteRecord)

    def test_should_assert_urlFromPartial_is_called_with_given_url(self):
      self.nodeManager.search('fakeURL')
      self.nodeManager.urlFromPartial.assert_called_with('fakeURL')
    
    def test_should_assert_hostAndPortFromURL_is_called_with_given_url(self):
      self.nodeManager.search('fakeURL')
      self.nodeManager.hostAndPortFromURL.assert_called_with('fakeUrlFromPartial')

    def test_should_assert_datastore_retrieve_is_called_with_url_host_and_port(self):
      self.nodeManager.search('fakeURL')
      self.nodeManager.datastore.retrieve.assert_called_with('fakeUrlFromPartial','fakeHostAndPortFromURLHost','fakeHostAndPortURLPort')

  class TestUrlForNode(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.fakeObject = TestNodesManager.Obj()

    def test_should_return_string_containing_url_when_node_http_ssl_not_0(self):
      urlForNode = self.nodeManager.urlForNode(self.fakeObject,'fakePath')
      self.assertIsInstance(urlForNode,str)

    def test_should_assert_returned_string_follows_the_url_pattern_when_node_http_ssl_not_0(self):
      urlForNode = self.nodeManager.urlForNode(self.fakeObject,'fakePath')
      self.assertEqual(urlForNode,"https://%s:%s/%s" %(self.fakeObject.host,self.fakeObject.http_port,'fakePath'))

    def test_should_return_string_containing_url_when_node_http_ssl_equals_to_0(self):
      fakeObject = TestNodesManager.Obj(http_ssl=0)
      urlForNode = self.nodeManager.urlForNode(fakeObject,'fakePath')
      self.assertIsInstance(urlForNode,str)

    def test_should_assert_returned_string_follows_the_url_pattern_when_node_http_ssl_equals_to_0(self):
      fakeObject = TestNodesManager.Obj(http_ssl=0)
      urlForNode = self.nodeManager.urlForNode(fakeObject,'fakePath')
      self.assertEqual(urlForNode,"http://%s:%s/%s" %(fakeObject.host,self.fakeObject.http_port,'fakePath'))

  class TestUrlForObject(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.urlForNode = Mock(return_value = "https://fake:8080/fakeMe")

    def test_should_return_string_containing_url_when_revision_is_None_and_subPath_is_objects(self):
      urlForObject = self.nodeManager.urlForObject("node", "uuid")
      self.assertIsInstance(urlForObject, str)
    
    def test_should_return_string_containing_url_when_revision_is_Not_None_and_subPath_is_objects(self):
      urlForObject = self.nodeManager.urlForObject("node", "uuid", revision="revision")
      self.assertIsInstance(urlForObject, str)

    def test_should_return_string_containing_url_when_revision_is_None_and_subPath_is_not_objects(self):
      urlForObject = self.nodeManager.urlForObject("node", "uuid", path='subPath')
      self.assertIsInstance(urlForObject, str)
    
    def test_should_return_string_containing_url_when_revision_is_Not_None_and_subPath_is_not_objects(self):
      urlForObject = self.nodeManager.urlForObject("node", "uuid", revision="revision", path='subPath')
      self.assertIsInstance(urlForObject, str)

    def test_assert_urlForNode_is_called_with_path_as_subPath_and_uuid(self):
      self.nodeManager.urlForObject("node", "uuid", path='subPath')
      self.nodeManager.urlForNode.assert_called_with("node", "uuid/subPath")
    
    def test_assert_urlForNode_is_called_with_revision_and_default_path(self):
      self.nodeManager.urlForObject("node", "uuid", revision='revision')
      self.nodeManager.urlForNode.assert_called_with("node", "uuid/revision")
    
    def test_assert_urlForNode_is_called_with_path_and_revision(self):
      self.nodeManager.urlForObject("node", "uuid", revision='revision', path='subPath')
      self.nodeManager.urlForNode.assert_called_with("node", "uuid/revision/subPath")

  class TestTaskFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.datastore.retrieveList = Mock(return_value = ['listObject1','listObject2'])
      self.fakeObject = TestNodesManager.Obj()

    def test_return_None_when_datastore_retrieveList_returns_empty_list_and_all_parameters_are_None(self):
      self.nodeManager.datastore.retrieveList = Mock(return_value = [])
      taskFor = self.nodeManager.taskFor()
      self.assertEqual(taskFor,None)

    def test_should_not_call_taskForFrom_when_datastore_retrieveList_returns_empty_list(self):
      self.nodeManager.datastore.retrieveList = Mock(return_value = [])
      self.nodeManager.taskForFrom = Mock()
      self.nodeManager.taskFor()
      self.nodeManager.taskForFrom.assert_not_called()

    def test_return_None_when_datastore_retrieveList_returns_empty_list_and_all_parameters_are_not_None(self):
      self.nodeManager.datastore.retrieveList = Mock(return_value = [])
      taskFor = self.nodeManager.taskFor(fromEnvironment='fromEnvironment', fromArchitecture='fromArchitecture', toEnvironment='toEnvironment', toArchitecture='toArchitecture', toBackend='toBackend', obj='obj')
      self.assertEqual(taskFor,None)

    def test_return_JSON_containing_metadata_of_node_object(self):
      self.nodeManager.taskForFrom = Mock(return_value = 'MockJSONCertificate')
      self.nodeManager.datastore.retrieveList = Mock(return_value = [self.fakeObject,'fakeObject2'])
      taskFor = self.nodeManager.taskFor(fromEnvironment='fromEnvironment', fromArchitecture='fromArchitecture', toEnvironment='toEnvironment', toArchitecture='toArchitecture', toBackend='toBackend', obj='obj')
      self.assertEqual(taskFor,'MockJSONCertificate')

    def test_assert_taskForFrom_is_called_with_proper_arguments(self):
      self.nodeManager.taskForFrom = Mock(return_value = 'MockJSONCertificate')
      self.nodeManager.datastore.retrieveList = Mock(return_value = [self.fakeObject,'fakeObject2'])
      self.nodeManager.taskFor(fromEnvironment='fromEnvironment', fromArchitecture='fromArchitecture', toEnvironment='toEnvironment', toArchitecture='toArchitecture', toBackend='toBackend', obj='obj')
      self.nodeManager.taskForFrom.assert_called_with(self.fakeObject,'fromEnvironment','fromArchitecture','toEnvironment','toArchitecture',toBackend='toBackend',obj='obj')
    
    def test_assert_datastore_retrieveList_is_called(self):
      self.nodeManager.taskForFrom = Mock(return_value = 'MockJSONCertificate')
      self.nodeManager.datastore.retrieveList = Mock(return_value = [self.fakeObject,'fakeObject2'])
      self.nodeManager.taskFor(fromEnvironment='fromEnvironment', fromArchitecture='fromArchitecture', toEnvironment='toEnvironment', toArchitecture='toArchitecture', toBackend='toBackend', obj='obj')
      self.nodeManager.datastore.retrieveList.assert_called()

  class TestPathFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.configuration = Mock()
      self.nodeManager.configuration.get.return_value = 'mockConfigurationGet'
      self.nodeManager.partialFromURL = Mock(return_value = ['localhost','8080'])

    @patch('os.path.join',return_value = 'mock/path/to/somewhere')
    def test_should_return_path_for_the_given_node(self,join):
      pathFor = self.nodeManager.pathFor('fakeUrl')
      self.assertEqual(pathFor,'mock/path/to/somewhere')

    @patch('os.path.join',return_value = 'mock/path/to/somewhere')
    def test_assert_configuration_get_is_called_with_string_path_and_path_join_Config_root(self,join):
      self.nodeManager.pathFor('fakeUrl')
      self.nodeManager.configuration.get.assert_called_with('path',join.return_value)
    
    @patch('os.path.join',return_value = 'mock/path/to/somewhere')
    def test_assert_path_join_is_called_with_nodePath_and_return_of_partialFromURL(self,join):
      self.nodeManager.pathFor('fakeUrl')
      join.assert_called_with(self.nodeManager.configuration.get.return_value, self.nodeManager.partialFromURL.return_value)

    @patch('os.path.join',return_value = 'mock/path/to/somewhere')
    def test_assert_partialFromURL_is_called_with_url(self,join):
      self.nodeManager.pathFor('fakeUrl')
      self.nodeManager.partialFromURL.assert_called_with('fakeUrl')

  class TestViewersFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.urlForNode = Mock(return_value = "https://fake:8080/fakeUrl")
      self.nodeManager.certificatePathFor = Mock(return_value = "mockCertificatePathFor")
      self.success = {"foo": "bar"}
      self.nodeManager.network.getJSON = Mock(return_value = self.success)

    def test_should_return_dict_containing_metadata_of_node(self):
      viewersFor = self.nodeManager.viewersFor('node', 'type', 'subtype')
      self.assertEqual(viewersFor, self.success)

    def test_asserts_urlForNode_is_called_with_node_type_and_subtype(self):
      self.nodeManager.viewersFor('node', 'type', 'subtype')
      self.nodeManager.urlForNode.assert_called_with('node', "viewers?type=%s&subtype=%s" % ('type','subtype'))

    def test_asserts_certificatePathFor_is_called_with_taskPath(self):
      self.nodeManager.viewersFor('node', 'type', 'subtype')
      self.nodeManager.certificatePathFor.assert_called_with('https://fake:8080/fakeUrl')

    def test_asserts_network_getJSON_is_called_with_taskPath_cert_and_suppressError_equals_to_True(self):
      self.nodeManager.viewersFor('node','type','subtype')
      self.nodeManager.network.getJSON("https://fake:8080/fakeUrl",'application/json',cert='mockCertificatePathFor',suppressError=True)

  class TestIdentityFrom(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.urlForNode = Mock(return_value = "https://fake:8080/fakeUrl")
      self.nodeManager.certificatePathFor = Mock(return_value = "mockCertificatePathFor")
      self.success = {"foo": "bar"}
      self.nodeManager.network.getJSON = Mock(return_value = self.success)

    def test_should_return_dict_containing_metadata_of_node(self):
      identityFrom = self.nodeManager.identityFrom('node', 'id')
      self.assertEqual(identityFrom, self.success)

    def test_asserts_urlForNode_is_called_with_node_and_id(self):
      self.nodeManager.identityFrom('node', 'id')
      self.nodeManager.urlForNode.assert_called_with('node', "identities/%s" %('id'))

    def test_asserts_certificatePathFor_is_called_with_taskPath(self):
      self.nodeManager.identityFrom('node', 'id')
      self.nodeManager.certificatePathFor.assert_called_with('https://fake:8080/fakeUrl')

    def test_asserts_network_getJSON_is_called_with_taskPath_cert_and_suppressError_equals_to_True(self):
      self.nodeManager.identityFrom('node','id')
      self.nodeManager.network.getJSON("https://fake:8080/fakeUrl",'application/json',cert='mockCertificatePathFor',suppressError=True)

  class TestCreatePathFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.configuration = Mock()
      self.nodeManager.configuration.get.return_value = 'mockConfigurationGet/path'
      self.nodeManager.partialFromURL = Mock(return_value = 'localhost,8080')

    @patch('os.path.exists')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_should_return_path_to_node_when_path_exists_with_nodePath_and_after_join_with_partialFromURL(self,join,exists):
      exists.side_effect = [True,True]
      createPathFor = self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      self.assertEqual(createPathFor,'mockPathJoin')

    @patch('os.path.exists')
    @patch('os.mkdir')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_assert_os_mkdir_is_not_called_with_nodePath_when_path_exists_with_configurationGet_path_and_with_partialFromURL_join(self,join,mkdir,exists):
      exists.side_effect = [True,True]
      self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      mkdir.assert_not_called()
    
    @patch('os.path.exists')
    @patch('os.mkdir')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_assert_os_mkdir_is_called_with_nodePath_when_path_does_not_exist_with_nodePath_configurationGet(self,join,mkdir,exists):
      exists.side_effect = [False,True]
      self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      mkdir.assert_called_with('mockConfigurationGet/path')

    @patch('os.path.exists')
    @patch('os.mkdir')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_assert_os_mkdir_is_called_with_nodePath_when_path_does_not_exist_with_nodePath_configurationGet_after_path_join_with_partialFromURL(self,join,mkdir,exists):
      exists.side_effect = [True,False]
      self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      mkdir.assert_called_with('mockPathJoin')

    @patch('os.path.exists')
    @patch('os.mkdir')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_assert_configuration_get_is_called_with_path_and_path_join_of_config_root_and_nodes(self,join,mkdir,exists):
      exists.side_effect = [True,True]
      self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      self.nodeManager.configuration.get.assert_called_with('path','mockPathJoin')
    
    @patch('os.path.exists')
    @patch('os.mkdir')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_assert_path_join_is_called_with_ConfigRoot_and_nodes(self,join,mkdir,exists):
      join.reset_mock()
      exists.side_effect = [True,True]
      self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      args, _ = join.call_args_list[3]
      self.assertEqual(args[0],'mockPathJoin')
      self.assertEqual(args[1],'nodes')

    @patch('os.path.exists')
    @patch('os.mkdir')
    @patch('os.path.join',return_value = 'mockPathJoin')
    def test_assert_path_join_is_called_with_nodePath_and_return_of_partialFromURL(self,join,mkdir,exists):
      join.reset_mock()
      exists.side_effect = [True,True]
      self.nodeManager.createPathFor('https://localhost:8080/fakeURL')
      args, _ = join.call_args_list[4]
      self.assertEqual(args[0],'mockConfigurationGet/path')
      self.assertEqual(args[1],'localhost,8080')

  class TestPullBackendFrom(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.urlForObject = Mock(return_value = "https://localhost:8080/fake/url")
      self.nodeManager.certificatePathFor = Mock(return_value = 'mock/to/path/where/certificate/is')
      
    #This method should return a boolean, however, the implementation
    #has a return before all other statements
    def test_should_return_boolean_True_if_object_was_pulled(self):
      fakeObject = TestNodesManager.Obj(pull = True)
      self.nodeManager.backends.handlerFor = Mock(return_value = fakeObject)
      pullBackendFrom = self.nodeManager.pullBackendFrom('node','uuid')
      self.assertEqual(pullBackendFrom,True)

    def test_should_return_boolean_False_if_object_was_pulled(self):
      fakeObject = TestNodesManager.Obj(pull = False)
      self.nodeManager.backends.handlerFor = Mock(return_value = fakeObject)
      pullBackendFrom = self.nodeManager.pullBackendFrom('node','uuid')
      self.assertEqual(pullBackendFrom,False)

    def test_assert_backends_handlerFor_is_called_with_docker(self):
      self.nodeManager.backends.handlerFor = Mock()
      self.nodeManager.pullBackendFrom('node','uuid')
      self.nodeManager.backends.handlerFor.assert_called_with('docker')
    
    def test_assert_urlForObject_is_called_with_node_uuid_and_revision_None(self):
      self.nodeManager.backends.handlerFor = Mock()
      self.nodeManager.pullBackendFrom('node','uuid')
      self.nodeManager.urlForObject.assert_called_with('node','uuid',None)

    def test_assert_urlForObject_is_called_with_node_uuid_and_revision_not_None(self):
      self.nodeManager.backends.handlerFor = Mock()
      self.nodeManager.pullBackendFrom('node','uuid',revision='revision')
      self.nodeManager.urlForObject.assert_called_with('node','uuid','revision')

    def test_assert_certificatePathFor_is_called_with_objectPath(self):
      self.nodeManager.backends.handlerFor = Mock()
      self.nodeManager.pullBackendFrom('node','uuid',revision='revision')
      self.nodeManager.certificatePathFor("https://localhost:8080/fake/url")

    def test_assert_dockerHandler_pull_is_called_with_objectPath_uuid_revision_and_cert_when_revision_is_None(self):
      self.nodeManager.backends.handlerFor = Mock()
      self.nodeManager.pullBackendFrom('node','uuid')
      self.nodeManager.backends.handlerFor.return_value.pull.assert_called_with("https://localhost:8080/fake/url",'uuid',revision=None,cert='mock/to/path/where/certificate/is')

    def test_assert_dockerHandler_pull_is_called_with_objectPath_uuid_revision_and_cert_when_revision_is_not_None(self):
      self.nodeManager.backends.handlerFor = Mock()
      self.nodeManager.pullBackendFrom('node','uuid',revision='revision')
      self.nodeManager.backends.handlerFor.return_value.pull.assert_called_with("https://localhost:8080/fake/url",'uuid',revision='revision',cert='mock/to/path/where/certificate/is')

  class TestRetrieveInfo(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.nodeManager = NodeManager()
      self.nodeManager.urlFromPartial = Mock(return_value = "https://localhost:8080/mockUrlFromPartial")
      self.nodeManager.certificatePathFor = Mock(return_value = 'this/is/a/fake/path/to/certificate')
      self.nodeManager.network.getJSON = Mock(return_value = b"mockNetworkGetJSON")
      self.nodeManager.retrieveCertificate = Mock(return_value = True)

    # def test_should_return_None_when_certificate_could_not_be_retrieved(self):
    #   self.nodeManager.retrieveCertificate = Mock(return_value = False)
    #   retrieveInfo = self.nodeManager.retrieveInfo("https://localhost:8080/fakeUrl")
    #   self.assertEqual(retrieveInfo,None)

    # def test_assert_certificatePathFor_is_not_called_when_certificate_could_not_be_retrieved(self):
    #   self.nodeManager.retrieveCertificate = Mock(return_value = False)
    #   self.nodeManager.retrieveInfo("https://localhost:8080/fakeUrl")
    #   self.nodeManager.certificatePathFor.assert_not_called()

    # def test_assert_network_getJSON_is_not_called_when_certificate_could_not_be_retrieved(self):
    #   self.nodeManager.retrieveCertificate = Mock(return_value = False)
    #   self.nodeManager.retrieveInfo("https://localhost:8080/fakeUrl")
    #   self.nodeManager.network.getJSON.assert_not_called()
      
    def test_should_return_None_when_network_getJSON_returns_None_with_scheme_http(self):
      self.nodeManager.network.getJSON = Mock(return_value = None)
      retrieveInfo = self.nodeManager.retrieveInfo('https://localhost:8080/fakeUrl')
      self.assertEqual(retrieveInfo,None)
