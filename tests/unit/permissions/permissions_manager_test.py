import unittest

from occam.permissions.manager import PermissionManager

from unittest.mock import patch, Mock, MagicMock, mock_open,create_autospec

class TestPermissionsManager:  

  class Obj:
    def __init__(self):
      self.can_read = True
      self.can_write = True
      self.can_clone = True
      self.can_run = True

  class TestCan(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()

    def test_should_return_bool_when_specific_key_is_given(self):
      self.permissionManager.retrieve = Mock(return_value = {"read":True, "write":False, "clone":True})
      permissionManager = self.permissionManager.can("read", "id")
      self.assertIsInstance(permissionManager, bool)

    def test_should_assert_retrieve_is_called_without_person_when_none_is_given(self):
      self.permissionManager.retrieve = Mock(return_value = {"read": True, "write": False, "clone": True})
      self.permissionManager.can("read", "id")
      self.permissionManager.retrieve.assert_called_with("id", person=None, revision=None)

    @patch('occam.person.Person', spec=True)
    def test_should_assert_retrieve_is_called_with_given_person(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.retrieve = Mock(return_value = {"read": True, "write": False, "clone": True})
      self.permissionManager.can("read", "id", person=person)
      self.permissionManager.retrieve.assert_called_with("id", person=person, revision=None)

  class TestRetrieve(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    def test_should_return_dictionary_when_revision_and_personID_is_None_and_retrieveAccessControl_returns_empty(self):
      self.permissionManager.retrieveAccessControl = Mock(return_value = [])
      retrieve = self.permissionManager.retrieve("id")
      self.assertIsInstance(retrieve, dict)
    
    def test_should_return_dictionary_with_read_when_personID_is_None_and_retrieveAccessControl_returns_empty(self):
      self.permissionManager.retrieveAccessControl = Mock(return_value = [])
      retrieve = self.permissionManager.retrieve("id")
      self.assertEqual(retrieve.get("read"), True)

    def test_should_return_dictionary_with_write_when_personID_is_None_and_retrieveAccessControl_returns_empty(self):
      self.permissionManager.retrieveAccessControl = Mock(return_value = [])
      retrieve = self.permissionManager.retrieve("id")
      self.assertEqual(retrieve.get("write"), False)

    def test_should_return_dictionary_with_clone_when_personID_is_None_and_retrieveAccessControl_returns_empty(self):
      self.permissionManager.retrieveAccessControl = Mock(return_value = [])
      retrieve = self.permissionManager.retrieve("id")
      self.assertEqual(retrieve.get("clone"), True)

    def test_should_return_dictionary_with_run_when_personID_is_None_and_retrieveAccessControl_returns_empty(self):
      self.permissionManager.retrieveAccessControl = Mock(return_value = [])
      retrieve = self.permissionManager.retrieve("id")
      self.assertEqual(retrieve.get("run"), True)

    @patch('occam.person.Person', spec=True)
    def test_should_assert_retrieveAccessControl_is_called_with_id_and_personID(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.retrieveAccessControl = Mock(return_value = [])
      self.permissionManager.retrieve("id", person=person)
      self.permissionManager.retrieveAccessControl.assert_called_with(id="id", person_id=person.id)

    def test_should_return_dictionary_when_revision_and_personID_is_None_and_retrieveAccessControl_returns_list_of_object(self):
      self.permissionManager.retrieveAccessControl = Mock(return_value = [self.fakeObject])
      retrieve = self.permissionManager.retrieve("id")
      self.assertIsInstance(retrieve,dict)

    @patch('occam.person.Person', spec=True)
    def test_should_return_dictionary_with_read_when_personID_is_None_and_retrieveAccessControl_returns_list_of_object(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.retrieveAccessControl = Mock(return_value = [self.fakeObject])
      retrieve = self.permissionManager.retrieve("id", person = person)
      self.assertEqual(retrieve.get("read"),self.fakeObject.can_read == 1)

    @patch('occam.person.Person', spec=True)
    def test_should_return_dictionary_with_write_when_personID_is_None_and_retrieveAccessControl_returns_list_of_object(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.retrieveAccessControl = Mock(return_value = [self.fakeObject])
      retrieve = self.permissionManager.retrieve("id", person=person)
      self.assertEqual(retrieve.get("write"), self.fakeObject.can_write == 1)

    @patch('occam.person.Person', spec=True)
    def test_should_return_dictionary_with_clone_when_retrieveAccessControl_returns_list_of_object(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.retrieveAccessControl = Mock(return_value = [self.fakeObject])
      retrieve = self.permissionManager.retrieve("id", person=person)
      self.assertEqual(retrieve.get("clone"), self.fakeObject.can_clone == 1)

    @patch('occam.person.Person', spec=True)
    def test_should_return_dictionary_with_run_when_retrieveAccessControl_returns_list_of_object(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.retrieveAccessControl = Mock(return_value = [self.fakeObject])
      retrieve = self.permissionManager.retrieve("id", person=person)
      self.assertEqual(retrieve.get("run"), self.fakeObject.can_run == 1)

  class TestUpdate(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()

    def test_should_update_access_with_given_id_and_default_parameters(self):
      self.permissionManager.updateAccessControl = Mock()
      self.permissionManager.update("id")
      self.permissionManager.updateAccessControl.assert_called_with(canClone=0, canRead=0, canRun=0, canWrite=0, children=False, id='id', person_id=None)

    def test_should_update_access_with_given_id_and_given_parameters(self):
      self.permissionManager.updateAccessControl = Mock()
      self.permissionManager.update("id",person_id = "person_id", canWrite = 1, canRun=1,canRead=1,canClone=1)
      self.permissionManager.updateAccessControl.assert_called_with(canClone=1, canRead=1, canRun=1, canWrite=1, children=False, id='id', person_id="person_id")

  class TestUpdateChildren(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()

    def test_should_update_access_with_given_id_and_default_parameters(self):
      self.permissionManager.updateAccessControl = Mock()
      self.permissionManager.updateChildren("id")
      self.permissionManager.updateAccessControl.assert_called_with(canClone=0, canRead=0, canRun=0, canWrite=0, children=True, id='id', person_id=None)

    def test_should_update_access_with_given_id_and_given_parameters(self):
      self.permissionManager.updateAccessControl = Mock()
      self.permissionManager.updateChildren("id",person_id = "person_id", canWrite = 1, canRun=1,canRead=1,canClone=1)
      self.permissionManager.updateAccessControl.assert_called_with(canClone=1, canRead=1, canRun=1, canWrite=1, children=True, id='id', person_id="person_id")

  class TestRetrieveAllRecords(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()

    @patch('occam.permissions.records.access_control.AccessControlRecord')
    @patch('occam.objects.records.object.ObjectRecord')
    def test_should_return_list_of_tuples_of_AccessControlRecord_ObjectRecord_when_all_parameters_are_default(self,ObjectRecord,AccessControlRecord):
      self.permissionManager.datastore.retrieveAllRecords = Mock(return_value = [(AccessControlRecord,ObjectRecord)])
      retrieveAllRecords = self.permissionManager.retrieveAllRecords()
      self.assertIsInstance(retrieveAllRecords, list)

    @patch('occam.permissions.records.access_control.AccessControlRecord')
    @patch('occam.objects.records.object.ObjectRecord')
    def test_should_return_tuples_of_AccessControlRecord_ObjectRecord_when_all_parameters_are_default(self,ObjectRecord,AccessControlRecord):
      self.permissionManager.datastore.retrieveAllRecords = Mock(return_value = [(AccessControlRecord,ObjectRecord)])
      retrieveAllRecords = self.permissionManager.retrieveAllRecords()
      self.assertIsInstance(retrieveAllRecords[0], tuple)

    @patch('occam.permissions.records.access_control.AccessControlRecord')
    @patch('occam.objects.records.object.ObjectRecord')
    def test_should_return_tuple_of_AccessControlRecord_ObjectRecord_when_all_parameters_are_default(self,ObjectRecord,AccessControlRecord):
      objectRecord = ObjectRecord()
      accessControlRecord = AccessControlRecord()
      self.permissionManager.datastore.retrieveAllRecords = Mock(return_value = [(accessControlRecord,objectRecord)])
      retrieveAllRecords = self.permissionManager.retrieveAllRecords()
      self.assertIs(retrieveAllRecords[0][0], accessControlRecord)
      self.assertIs(retrieveAllRecords[0][1], objectRecord)

    def test_should_assert_datastore_retrieveAllRecords_is_called_with_proper_arguments(self):
      self.permissionManager.datastore.retrieveAllRecords = Mock()
      self.permissionManager.retrieveAllRecords()
      self.permissionManager.datastore.retrieveAllRecords.assert_called_with(None, None, None, None, False, False)
      
    @patch('occam.permissions.records.access_control.AccessControlRecord')
    @patch('occam.objects.records.object.ObjectRecord')
    def test_should_return_list_of_tuples_of_AccessControlRecord_ObjectRecord_when_all_parameters_are_not_default(self,ObjectRecord,AccessControlRecord):
      self.permissionManager.datastore.retrieveAllRecords = Mock(return_value = [(AccessControlRecord,ObjectRecord)])
      retrieveAllRecords = self.permissionManager.retrieveAllRecords(id="id", db_obj="db_obj", person="person_id", person_obj="person_obj", allPeople=True, addChildren=True)
      self.assertIsInstance(retrieveAllRecords, list)

    @patch('occam.permissions.records.access_control.AccessControlRecord')
    @patch('occam.objects.records.object.ObjectRecord')
    def test_should_return_tuples_of_AccessControlRecord_ObjectRecord_when_all_parameters_are_not_default(self,ObjectRecord,AccessControlRecord):
      self.permissionManager.datastore.retrieveAllRecords = Mock(return_value = [(AccessControlRecord,ObjectRecord)])
      retrieveAllRecords = self.permissionManager.retrieveAllRecords(id="id", db_obj="db_obj", person="person_id", person_obj="person_obj", allPeople=True, addChildren=True)
      self.assertIsInstance(retrieveAllRecords[0], tuple)
    
    @patch('occam.person.Person', spec=True)
    @patch('occam.permissions.records.access_control.AccessControlRecord')
    @patch('occam.objects.records.object.ObjectRecord')
    def test_should_return_tuple_of_AccessControlRecord_ObjectRecord_when_all_parameters_are_not_default(self,ObjectRecord,AccessControlRecord, Person):
      person = Person()
      person.id = "person_id"
      objectRecord = ObjectRecord()
      accessControlRecord = AccessControlRecord()
      self.permissionManager.datastore.retrieveAllRecords = Mock(return_value = [(accessControlRecord,objectRecord)])
      retrieveAllRecords = self.permissionManager.retrieveAllRecords(id="id", db_obj="db_obj", person=person, person_obj="person_obj", allPeople=True, addChildren=True)
      self.assertIs(retrieveAllRecords[0][0], accessControlRecord)
      self.assertIs(retrieveAllRecords[0][1], objectRecord)

    @patch('occam.person.Person', spec=True)
    def test_should_assert_datastore_retrieveAllRecords_is_called_with_proper_given_arguments(self, Person):
      person = Person()
      person.id = "person_id"
      self.permissionManager.datastore.retrieveAllRecords = Mock()
      self.permissionManager.retrieveAllRecords(id="id", db_obj="db_obj", person=person, person_obj="person_obj", allPeople=True, addChildren=True)
      self.permissionManager.datastore.retrieveAllRecords.assert_called_with("id","db_obj", "person_id","person_obj", True,True)

  class TestRetrieveAccessControl(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
    
    def test_should_return_list_when_all_parameters_are_default(self):
      self.permissionManager.datastore.retrieveAccessControl = Mock(return_value = [])
      retrieveAccessControl = self.permissionManager.retrieveAccessControl()
      self.assertIsInstance(retrieveAccessControl,list)

    def test_should_assert_datastore_retrieveAccessControl_is_called_with_proper_default_arguments(self):
      self.permissionManager.datastore.retrieveAccessControl = Mock(return_value = [])
      self.permissionManager.retrieveAccessControl()
      self.permissionManager.datastore.retrieveAccessControl.assert_called_with(None,None,None,None)

    def test_should_return_list_when_all_parameters_are_not_default(self):
      self.permissionManager.datastore.retrieveAccessControl = Mock(return_value = [])
      retrieveAccessControl = self.permissionManager.retrieveAccessControl(id='id', db_obj='db_obj', person_id='person_id', person_obj='person_obj')
      self.assertIsInstance(retrieveAccessControl,list)

    def test_should_assert_datastore_retrieveAccessControl_is_called_with_proper_given_arguments(self):
      self.permissionManager.datastore.retrieveAccessControl = Mock(return_value = [])
      self.permissionManager.retrieveAccessControl(id='id', db_obj='db_obj', person_id='person_id', person_obj='person_obj')
      self.permissionManager.datastore.retrieveAccessControl.assert_called_with('id','db_obj','person_id','person_obj')

  class TestUpdateAccessControl(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()

    @patch('occam.permissions.records.access_control.AccessControlRecord')
    def test_should_return_AccessControlRecord_object_when_all_parameters_are_default(self,AccessControlRecord):
      accessControlRecord = AccessControlRecord()
      self.permissionManager.datastore.updateAccessControl = Mock(return_value = accessControlRecord)
      updateAccessControl = self.permissionManager.updateAccessControl()
      self.assertIs(updateAccessControl,accessControlRecord)

    def test_should_assert_datastore_AccessControlRecord_is_called_with_default_parameters(self):
      self.permissionManager.datastore.updateAccessControl = Mock()
      self.permissionManager.updateAccessControl()
      self.permissionManager.datastore.updateAccessControl.assert_called_with(None, None,
                                None, None,
                                False,
                                0, 0, 0, 0)

    @patch('occam.permissions.records.access_control.AccessControlRecord')
    def test_should_return_AccessControlRecord_object_with_given_parameters(self,AccessControlRecord):
      accessControlRecord = AccessControlRecord()
      self.permissionManager.datastore.updateAccessControl = Mock(return_value = accessControlRecord)
      updateAccessControl = self.permissionManager.updateAccessControl(id='id', db_obj='db_obj',
                                person_id='person_id', person_obj='person_obj',
                                children=True,
                                canRead=1, canWrite=1, canClone=1, canRun=1)
      self.assertIs(updateAccessControl,accessControlRecord)

    def test_should_assert_datastore_AccessControlRecord_is_called_with_given_parameters(self):
      self.permissionManager.datastore.updateAccessControl = Mock()
      self.permissionManager.updateAccessControl(id='id', db_obj='db_obj',
                                person_id='person_id', person_obj='person_obj',
                                children=True,
                                canRead=1, canWrite=1, canClone=1, canRun=1)
      self.permissionManager.datastore.updateAccessControl.assert_called_with('id', 'db_obj',
                                'person_id', 'person_obj',
                                True,
                                1, 1, 1, 1)

  class TestRetrieveReviewLinks(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_list_of_ReviewCapabilityRecord_when_revision_is_None(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.retrieveReviewLinks = Mock(return_value = [reviewCapabilityRecord])
      retrieveReviewLinks = self.permissionManager.retrieveReviewLinks(self.fakeObject)
      self.assertIsInstance(retrieveReviewLinks,list)

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_ReviewCapabilityRecord_objects_in_list_when_revision_is_None(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.retrieveReviewLinks = Mock(return_value = [reviewCapabilityRecord])
      retrieveReviewLinks = self.permissionManager.retrieveReviewLinks(self.fakeObject)
      for i in retrieveReviewLinks:
        self.assertIs(i,reviewCapabilityRecord)

    def test_should_assert_datastore_retrieveReviewLinks_is_called_with_obj_and_revision_None(self):
      self.permissionManager.datastore.retrieveReviewLinks = Mock()
      self.permissionManager.retrieveReviewLinks(self.fakeObject)
      self.permissionManager.datastore.retrieveReviewLinks.assert_called_with(self.fakeObject,None)

    def test_should_assert_datastore_retrieveReviewLinks_is_called_with_obj_and_revision_not_None(self):
      self.permissionManager.datastore.retrieveReviewLinks = Mock()
      self.permissionManager.retrieveReviewLinks(self.fakeObject,revision='revision')
      self.permissionManager.datastore.retrieveReviewLinks.assert_called_with(self.fakeObject,'revision')

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_list_of_ReviewCapabilityRecord_when_revision_is_not_None(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.retrieveReviewLinks = Mock(return_value = [reviewCapabilityRecord])
      retrieveReviewLinks = self.permissionManager.retrieveReviewLinks(self.fakeObject,revision='revision')
      self.assertIsInstance(retrieveReviewLinks,list)

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_ReviewCapabilityRecord_objects_in_list_when_revision_is_not_None(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.retrieveReviewLinks = Mock(return_value = [reviewCapabilityRecord])
      retrieveReviewLinks = self.permissionManager.retrieveReviewLinks(self.fakeObject,revision='revision')
      for i in retrieveReviewLinks:
        self.assertIs(i,reviewCapabilityRecord)

  class TestRetrieveReviewLink(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_RecordCapabilityRecord_object(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.retrieveReviewLink = Mock(return_value = reviewCapabilityRecord)
      retrieveReviewLink = self.permissionManager.retrieveReviewLink(self.fakeObject)
      self.assertIs(retrieveReviewLink, reviewCapabilityRecord)
    
    def test_should_assert_datastore_retrieveReviewLink_is_called_with_object(self):
      self.permissionManager.datastore.retrieveReviewLink = Mock()
      self.permissionManager.retrieveReviewLink(self.fakeObject)
      self.permissionManager.datastore.retrieveReviewLink.assert_called_with(self.fakeObject)

  class TestCreateReviewLink(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_ReviewCapabilityRecord(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.createReviewLink = Mock(return_value = reviewCapabilityRecord)
      createReviewLink = self.permissionManager.createReviewLink(self.fakeObject)
      self.assertIs(createReviewLink,reviewCapabilityRecord)

    def test_should_assert_datastore_createReviewLink_is_called_with_obj(self):
      self.permissionManager.datastore.createReviewLink = Mock()
      self.permissionManager.createReviewLink(self.fakeObject)
      self.permissionManager.datastore.createReviewLink.assert_called_with(self.fakeObject)

  class TestRemoveReviewLink(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    @patch('occam.permissions.records.review_capability.ReviewCapabilityRecord')
    def test_should_return_ReviewCapabilityRecord_object_after_delete(self,ReviewCapabilityRecord):
      reviewCapabilityRecord = ReviewCapabilityRecord()
      self.permissionManager.datastore.removeReviewLink = Mock(return_value = reviewCapabilityRecord)
      removeReviewLink = self.permissionManager.removeReviewLink(self.fakeObject)
      self.assertIs(removeReviewLink,reviewCapabilityRecord)

    def test_should_assert_datastore_removeReviewLink(self):
      self.permissionManager.datastore.removeReviewLink = Mock()
      self.permissionManager.removeReviewLink(self.fakeObject)
      self.permissionManager.datastore.removeReviewLink.assert_called_with(self.fakeObject)

  class TestTrust(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()
    
    @patch('occam.permissions.records.trust_association.TrustAssociationRecord')
    @patch('occam.person.Person', spec=True)
    def test_should_return_TrustAssociationRecord_object(self,Person,TrustAssociationRecord):
      person = Person()
      trustAssociationRecord = TrustAssociationRecord()
      self.permissionManager.datastore.createTrustAssociation = Mock(return_value = trustAssociationRecord)
      trust = self.permissionManager.trust(self.fakeObject,person)
      self.assertIs(trust,trustAssociationRecord)

    @patch('occam.person.Person', spec=True)
    def test_should_assert_datastore_createTrustAssociation_is_called_with_obj_and_person(self,Person):
      person = Person()
      self.permissionManager.datastore.createTrustAssociation = Mock()
      self.permissionManager.trust(self.fakeObject,person)
      self.permissionManager.datastore.createTrustAssociation.assert_called_with(self.fakeObject,person)
  
  class TestUntrust(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    def test_should_return_None_when_datastore_removeTrustAssociation_returns_None(self):
      self.permissionManager.datastore.removeTrustAssociation = Mock(return_value = None)
      untrust = self.permissionManager.untrust(self.fakeObject,self.fakeObject)
      self.assertEqual(untrust,None)
    
    @patch('occam.permissions.records.trust_association.TrustAssociationRecord')
    def test_should_return_None_when_datastore_removeTrustAssociation_returns_TrustAssociationRecord(self,TrustAssociationRecord):
      trustAssociationRecord = TrustAssociationRecord()
      self.permissionManager.datastore.removeTrustAssociation = Mock(return_value = trustAssociationRecord)
      untrust = self.permissionManager.untrust(self.fakeObject,self.fakeObject)
      self.assertIs(untrust,trustAssociationRecord)

    @patch('occam.person.Person', spec=True)
    def test_should_assert_datastore_removeTrustAssociation_is_called_with_obj_and_person(self,Person):
      person = Person()
      self.permissionManager.datastore.removeTrustAssociation = Mock()
      self.permissionManager.untrust(self.fakeObject,person)
      self.permissionManager.datastore.removeTrustAssociation.assert_called_with(self.fakeObject,person)

  class TestIsTrusted(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.permissionManager = PermissionManager()
      self.fakeObject = TestPermissionsManager.Obj()

    @patch('occam.person.Person', spec=True)
    def test_should_return_True_when_datastore_isTrusted_returns_True(self,Person):
      person = Person()
      self.permissionManager.datastore.isTrusted = Mock(return_value = True)
      isTrusted = self.permissionManager.isTrusted(self.fakeObject,person)
      self.assertEqual(isTrusted,True)
    
    @patch('occam.person.Person', spec=True)
    def test_should_return_False_when_datastore_isTrusted_returns_False(self,Person):
      person = Person()
      self.permissionManager.datastore.isTrusted = Mock(return_value = True)
      isTrusted = self.permissionManager.isTrusted(self.fakeObject,person)
      self.assertEqual(isTrusted,True)

    @patch('occam.person.Person', spec=True)
    def test_should_assert_datastore_isTrusted_is_called_with_object_and_person(self,Person):
      person = Person()
      self.permissionManager.datastore.isTrusted = Mock(return_value = True)
      self.permissionManager.isTrusted(self.fakeObject,person)    
      self.permissionManager.datastore.isTrusted.assert_called_with(self.fakeObject,person)
