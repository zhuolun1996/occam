import unittest

from tests.helper import multihash

import os, random, re, io
random.seed(os.environ["PYTHONHASHSEED"])

from occam.objects.manager import ObjectManager

from unittest.mock import patch, Mock, MagicMock, mock_open
from unittest.mock import create_autospec
from occam.git_repository import GitRepository
from occam.object_info import ObjectInfo
from occam.objects.manager import ObjectJSONError

class GenerateRandomObjectIdentifiers:

  @staticmethod
  def generate_random_uuid():
    return multihash()

  @staticmethod
  def generate_random_version():
    version = "" + str(random.randint(0,10))
    for i in range(1,random.randint(2,4)):
      version += "." + str(random.randint(0,100))
    return version

  @staticmethod
  def generate_random_revision():
    return multihash("sha1")

  @staticmethod
  def generate_random_link():
    link = str(random.randint(0,10000000))
    return link

  @staticmethod
  def generate_random_index():
    index_max_depth = random.randint(1,10)
    index = ""
    for i in range(0,index_max_depth):
      index += "[" + str(random.randint(0,100)) + "]"
    return index

  @staticmethod
  def generate_random_path():
    import os
    import string
    path = "/"+''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits+os.path.sep+".") for _ in range(random.randint(10,250)) )
    return path

  @staticmethod
  def generate_random_object_identifier(config=None):
    if config is None:
      config = {}
      for item in ["version", "revision", "link", "index", "path"]:
        config[item] = config.get(item, False)

    # The user can specify the uuid
    uuid = config.get("uuid", None)
    if( uuid is None ):
      uuid = GenerateRandomObjectIdentifiers.generate_random_uuid()
    obj_id = uuid

    if( config.get("version") ):
      version = GenerateRandomObjectIdentifiers.generate_random_version()
      obj_id += ":" + version
    else:
      version = None

    if( config.get("revision") ):
      revision = GenerateRandomObjectIdentifiers.generate_random_revision()
      obj_id += "@" + revision
    else:
      revision = None

    if( config.get("link") ):
      link = GenerateRandomObjectIdentifiers.generate_random_link()
      obj_id += "#" + link
    else:
      link = None

    if( config.get("index") ):
      index = GenerateRandomObjectIdentifiers.generate_random_index()
      obj_id += index
    else:
      index = None

    if( config.get("path") ):
      path = GenerateRandomObjectIdentifiers.generate_random_path()
      obj_id += path
    else:
      path = None

    return obj_id, {
      "uuid"     : uuid,
      "version"  : version,
      "revision" : revision,
      "link"     : link,
      "index"    : index,
      "path"     : path
    }

  @staticmethod
  def parse_identifier_into_elements(regex, identifier):
    parser = re.compile(regex)
    result = parser.match(identifier)

    if result is None:
      return None

    return {
      "uuid"     : result.group("uuid"),
      "version"  : result.group("version"),
      "revision" : result.group("revision"),
      "link"     : result.group("link"),
      "index"    : result.group("index"),
      "path"     : result.group("path")
    }

class TestObjectManager:
  class t(Exception):
    def __init__(self,msg="Expecting ',' delimiter",lineno=1,colno=3,pos=19,doc="jsonDecodeError.doc"):
      self.msg      = msg
      self.lineno  = lineno
      self.colno = colno
      self.pos     = pos
      self.doc     = doc

  class Obj:
    def __init__(self, info = {"name": "Name"}, file="/root/dir/files",
        path=None,infoRevision = None, ownerInfo={"name": "Name"},
        ownerInfoRevision="revision",owner_id = "owner_id",id="id",
        link=None,root=None,roots="roots",position="position",
        object_type=None,resource="resource",tag="tag",revision="revision"):
      self.origin = self
      self.fileCache = {}
      self.uid = "uid"
      self.revision = revision
      self.path = path
      self.temporary = False
      self.id = id
      self.identity = "identity"
      self.roots = roots
      self.position = position
      self.link = link
      self.owner_id = owner_id
      self.identity_uri = "identity_uri"
      self.owner_uid = "owner_uid"
      self.version = "version"
      self.index = 0
      self.person = "person"
      self.root = root or self
      self.object_type = object_type
      self.resource = resource
      self.ownerInfoRevision = ownerInfoRevision
      self.ownerInfo = ownerInfo
      self.infoRevision = infoRevision
      self.info = info
      self.file = file
      self.tag = tag
      self.anonymous = False

  class TestBuildPathFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_path(self):
      self.objManager.storage.pathFor = Mock(return_value="this_is_a_valid_path")
      o = TestObjectManager.Obj()
      t = self.objManager.buildPathFor(o,1)
      self.objManager.storage.pathFor.assert_called_once()
      self.assertIsInstance(t,str)

  class TestInstantiate(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('shutil.rmtree')
    @patch('os.path.join')
    @patch('occam.objects.manager.GitRepository')
    def test_should_return_string_containing_cache_path(self,mock_gitRepo,mock_pathJoin,rmtree):
      self.objManager.install = Mock(return_value='install')
      rmtree.return_value = '/path/foo.exe'
      mock_pathJoin.return_value = '/path/join/mock'
      self.objManager.storage.pathFor = Mock()
      self.objManager.storage.pathFor.side_effect = [None,'this_is_a_valid_path']
      self.objManager.storage.repositoryPathFor = Mock(return_value='repository_path')
      self.assertIsInstance(self.objManager.instantiate(TestObjectManager.Obj()),str)
      mock_gitRepo.assert_called_once()
      mock_pathJoin.assert_called_once()
      rmtree.assert_called_once()

  class TestReset(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('occam.objects.manager.GitRepository')
    def test_should_path_containing_new_revision(self,mock_gitRepo):
      mock_gitRepo.return_value.reset.return_value = 'mockingGitRepoCall'
      self.objManager.reset(TestObjectManager.Obj())

  class TestClone(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('os.path.realpath')
    @patch('tempfile.mkdtemp')
    @patch('os.path.exists')
    def test_should_return_False_when_path_exists_with_path_join_git(self,path_exists,mkdtemp,real_path):
      path_exists.side_effect = [False,True]
      mkdtemp.return_value = "mkdtemp_mock"
      real_path.return_value = "real_path_mock"
      self.assertEqual(self.objManager.clone(TestObjectManager.Obj()),False)

    @patch('os.path.exists')
    @patch('occam.objects.manager.GitRepository')
    @patch('occam.objects.manager.Object')
    def test_should_return_object_when_path_is_not_None(self,mock_objectManager,mock_gitRepo,path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      path_exists.side_effect = [True,False,False]
      self.assertIsInstance(self.objManager.clone(TestObjectManager.Obj(path="path"),path="test_path"),object)
      mock_objectManager.assert_called_once()

    @patch('os.path.exists')
    @patch('os.path.realpath')
    @patch('occam.objects.manager.Object')
    def test_should_return_object_when_obj_has_no_path(self,mock_objectManager,real_path,path_exists):
      self.objManager.storage.clone = Mock(return_value="storage_clone_mock")
      mock_objectManager.return_value = TestObjectManager.Obj()
      real_path.return_value = "real_path_mock"
      path_exists.side_effect = [True,True,False,False]
      self.assertIsInstance(self.objManager.clone(TestObjectManager.Obj(path=None)),object)
      mock_objectManager.assert_called_once()
      real_path.assert_called_once()
      mock_objectManager.assert_called_once()

  class TestSearchEnvironments(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list_with_string_for_each_environment_that_matches(self):
      self.objManager.datastore.retrieveEnvironments = Mock(return_value=["envo1","envo2"])
      self.assertIsInstance(self.objManager.searchEnvironments("environ"),list)

  class TestSearchArchitectures(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list__with_string_for_each_architecture_that_matches(self):
      self.objManager.datastore.retrieveArchitectures = Mock(return_value=["arch1","arch2"])
      self.assertIsInstance(self.objManager.searchArchitectures("arch"),list)

  class TestSearchTypes(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list_with_string_for_each_type_that_matches(self):
      self.objManager.datastore.retrieveObjectTypes = Mock(return_value=["obj1","obj2"])
      self.assertIsInstance(self.objManager.searchTypes("arch"),list)

  class TestFullSearch(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_dictionary_containing_search_and_aggregate_information(self):
      self.objManager.datastore.fullSearch = Mock(return_value={"search":{"something":"anything"}})
      self.assertIsInstance(self.objManager.fullSearch("search"),dict)

  class TestSearch(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list_with_ObjectRecord_for_each_object_found_given_the_terms(self):
      self.objManager.datastore.retrieveObjects = Mock(return_value=["obj1"])
      self.assertIsInstance(self.objManager.search("obj1"),list)

  class TestViewersFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list_with_objects_with_type_and_subtype(self):
      self.objManager.datastore.viewersFor = Mock(return_value=[{"obj1":{"type":"string","subtype":None}}])
      self.assertIsInstance(self.objManager.viewersFor("obj1"),list)

  class TestEditorsFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list_with_objects_with_type_and_subtype_editors(self):
      self.objManager.datastore.editorsFor = Mock(return_value=[{"obj1":{"type":"string","subtype":None}}])
      self.assertIsInstance(self.objManager.editorsFor("obj1"),list)

  class TestConfiguratorsFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_a_list_with_objects_that_can_configure_the_given_object(self):
      self.objManager.datastore.configuratorsFor = Mock(return_value=["configurator1","configurator2"])
      self.assertIsInstance(self.objManager.configuratorsFor("obj1"),list)

  class TestOriginFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('occam.objects.manager.Object')
    @patch('occam.objects.manager.GitRepository')
    def test_should_return_a_object_containing_the_reference_to_the_given_object_at_its_initial_commit(self,mock_gitRepo,mock_object):
      mock_object.return_value = TestObjectManager.Obj()
      self.assertIsInstance(self.objManager.originFor(TestObjectManager.Obj()),object)

  class TestInstall(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_None_when_object_info_is_None(self):
      self.objManager.infoFor = Mock(return_value=None)
      self.assertEqual(self.objManager.install(TestObjectManager.Obj(),basePath=None),None)

    def test_should_return_list_of_objects_for_pulled_objects(self):
      self.objManager.resources = Mock()
      self.objManager.resources.installAll.return_value = ["g++",'cmake']
      self.objManager.infoFor = Mock(return_value={"install":["g++","cmake"],"run":{"install":["gcc","vim"]}})
      self.assertIsInstance(self.objManager.install(TestObjectManager.Obj(),basePath=None),list)
      self.objManager.infoFor.assert_called_once()

    def test_should_return_list_of_objects_for_pulled_objects_when_build_in_objectInfo_and_build_is_True(self):
      self.objManager.resources = Mock()
      self.objManager.resources.installAll.return_value = ["g++",'cmake']
      self.objManager.infoFor = Mock(return_value={"install":["g++","cmake"],"build":{"install":["python"]},"run":{"install":["gcc","vim"]}})
      self.assertIsInstance(self.objManager.install(TestObjectManager.Obj(),basePath=None,build=True),list)
      self.objManager.infoFor.assert_called_once()

  class TestUidTokenFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_bytes_containing_token_with_type_name_source(self):
      self.objManager.ownerInfoFor = Mock(return_value={"source":"test","type":"string","name":"mock_test"})
      t = self.objManager.uidTokenFor("t")
      self.assertIsInstance(t,bytes)
      self.objManager.ownerInfoFor.assert_called_once()

    def test_should_return_bytes_containing_token_and_information_when_objectinfo_does_not_have_source(self):
      self.objManager.ownerInfoFor = Mock(return_value={"type":"string","name":"mock_test"})
      self.objManager.originFor = Mock(return_value=TestObjectManager.Obj())
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      t = self.objManager.uidTokenFor("t")
      self.assertIsInstance(t,bytes)
      self.objManager.ownerInfoFor.assert_called_once()

    def test_should_return_bytes_containing_token_and_information_when_objectinfo_does_not_have_source_and_try_raises_exception(self):
      self.objManager.ownerInfoFor = Mock(return_value={"type":"string","name":"mock_test"})
      self.objManager.originFor = Mock(return_value=TestObjectManager.Obj())
      self.objManager.retrieveFileFrom = Mock()
      self.objManager.retrieveFileFrom.side_effect = Exception("mockException")
      t = self.objManager.uidTokenFor("t")
      self.assertIsInstance(t,bytes)
      self.objManager.ownerInfoFor.assert_called_once()

    def test_should_return_bytes_containing_token_and_information_when_objectinfo_does_not_have_source_and_try_raises_exception_and_subObjectType_and_subObjectName_not_None(self):
      self.objManager.ownerInfoFor = Mock(return_value={"type":"string","name":"mock_test"})
      self.objManager.originFor = Mock(return_value=TestObjectManager.Obj())
      self.objManager.retrieveFileFrom = Mock()
      self.objManager.retrieveFileFrom.side_effect = Exception("mockException")
      t = self.objManager.uidTokenFor("t",subObjectType="subObjectType",subObjectName="subObjectName")
      self.assertIsInstance(t,bytes)
      self.objManager.ownerInfoFor.assert_called_once()

    def test_should_raise_exception(self):
      with self.assertRaises(ValueError) as cm:
        self.objManager.ownerInfoFor = Mock(return_value={"type":"string","name":"mock_test"})
        self.objManager.originFor = Mock(return_value=TestObjectManager.Obj())
        self.objManager.retrieveFileFrom = Mock()
        self.objManager.retrieveFileFrom.side_effect = Exception("mockException")
        t = self.objManager.uidTokenFor("t",subObjectName="subObjectName")
        self.assertEqual(t.exception.error_code,"Need both subObjectType and subObjectName")
        self.objManager.ownerInfoFor.assert_called_once()

  class TestRetrieve(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('os.path.exists')
    @patch('occam.objects.manager.GitRepository')
    @patch('occam.objects.manager.Object')
    def test_should_return_object_when_path_exists_but_resource_always_None(self,mock_objectManager, GitRepository, path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = None
      self.objManager.resolveVersion = Mock(return_value=("revisionMock","resolvedVersionMock"))
      self.objManager.versions = Mock()
      self.objManager.versions.resolve = Mock(return_value=("revision", "1",))
      path_exists.return_value = True
      GitRepository.hasRevision = Mock(return_value = True)
      self.assertIsInstance(self.objManager.retrieve("123",version="1"),object)

    def test_should_return_none_when_path_does_not_exist_and_ret_is_None(self):
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("teste",None,None)
      self.assertEqual(self.objManager.retrieve("123"),None)

    @patch('os.path.exists')
    @patch('occam.objects.manager.Object')
    def test_should_return_object_when_path_exists_and_index_equal_empty_array(self,mock_objectManager,path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("teste",None,None)
      path_exists.return_value = True
      self.assertIsInstance(self.objManager.retrieve("123"),object)

    @patch('os.path.exists')
    @patch('occam.objects.manager.Object')
    def test_should_return_None_when_index_less_than_len_info_contains(self,mock_objectManager,path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.versions = Mock()
      self.objManager.versions.resolve = Mock(return_value=("revision", "1",))
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.infoFor = Mock(return_value={"contains":["infoFor"]})
      self.objManager.ownerFor = Mock(return_value="ownerForMock")
      self.objManager.resolveVersion = Mock(return_value=("revisionMock","resolvedVersionMock"))
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("teste",None,None)
      path_exists.return_value = True
      self.assertEqual(self.objManager.retrieve("123",index=[1],version="1"),None)

    @patch('occam.person.Person')
    @patch('os.path.exists')
    @patch('occam.objects.manager.Object')
    def test_should_return_object_when_info_contains_data(self,mock_objectManager,path_exists,person):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.versions = Mock()
      self.objManager.versions.resolve = Mock(return_value=("revision", "1",))
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.infoFor = Mock(return_value={"contains":[{"id":"123","revision":"1.2"}]})
      self.objManager.ownerFor = Mock(return_value="ownerForMock")
      self.objManager.resolveVersion = Mock(return_value=("revisionMock","resolvedVersionMock"))
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("teste",None,None)
      path_exists.return_value = True
      self.assertIsInstance(self.objManager.retrieve("123",index=[0],version="1",person=person),object)

    @patch('os.path.exists')
    @patch('occam.objects.manager.Object')
    def test_should_return_object_when_info_contains_data_and_object_options_is_given(self,mock_objectManager,path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.versions = Mock()
      self.objManager.versions.resolve = Mock(return_value=("revision", "1",))
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.infoFor = Mock(return_value={"contains":[{"id":"123","revision":"1.2"}]})
      self.objManager.ownerFor = Mock(return_value="ownerForMock")
      self.objManager.resolveVersion = Mock(return_value=("revisionMock","resolvedVersionMock"))
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("test", None, None)
      path_exists.return_value = True
      self.assertIsInstance(self.objManager.retrieve("123",index=[0],version="1",object_options=TestObjectManager.Obj()),object)

    def test_should_return_None_when_datastore_retrieveObjects_returns_None(self):
      self.objManager.datastore.retrieveObjects = Mock(return_value=[None])
      self.assertEqual(self.objManager.retrieve("123"),None)
      self.objManager.datastore.retrieveObjects.assert_called_once()

    def test_should_return_None_when_permissions_retrieve_retuns_None(self):
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":None})
      self.assertEqual(self.objManager.retrieve("123"),None)

    def test_should_return_None_when_repositoryPathFor_returns_None(self):
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = None)
      self.objManager.storage.resourcePathFor = Mock(return_value = None)
      self.assertEqual(self.objManager.retrieve("123"),None)

    @patch('os.path.exists')
    @patch('occam.objects.manager.Object')
    def test_should_return_None_when_cannot_find_the_version_requested(self,mock_objectManager,path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.versions = Mock()
      self.objManager.versions.resolve = Mock(return_value=(None,None,))
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.infoFor = Mock(return_value={"contains":[{"id":"123","revision":"1.2"}]})
      self.objManager.ownerFor = Mock(return_value="ownerForMock")
      self.objManager.resolveVersion = Mock(return_value=(None,"resolvedVersionMock"))
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("test",None,None)
      path_exists.return_value = True
      self.assertEqual(self.objManager.retrieve("123", index=[0], version="1"), None)

    @patch('os.path.exists')
    @patch('occam.objects.manager.Object')
    def test_should_return_None_when(self,mock_objectManager,path_exists):
      mock_objectManager.return_value = TestObjectManager.Obj()
      self.objManager.versions = Mock()
      self.objManager.versions.resolve = Mock(return_value=("revision", "1",))
      self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.permissions.retrieve = Mock(return_value = {"read":True})
      self.objManager.storage.repositoryPathFor = Mock(return_value = "mock_repositoryPathFor")
      self.objManager.storage.resourcePathFor = Mock(return_value = "mock_resourcePathFor")
      self.objManager.infoFor = Mock(return_value=None)
      self.objManager.ownerFor = Mock(return_value="ownerForMock")
      self.objManager.resolveVersion = Mock(return_value=("revisionMock","resolvedVersionMock"))
      self.objManager.resources = Mock()
      self.objManager.resources.retrievePath.return_value = ("teste",None,None)
      path_exists.side_effect = [True,True]
      self.assertEqual(self.objManager.retrieve("123",index=[0],version="1"),None)

  class TestRetrieveLocal(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('os.path.exists')
    def test_should_return_None(self,path_exists):
      path_exists.return_value = None
      self.assertEqual(self.objManager.retrieveLocal("path"),(None,None))

    @patch('os.path.exists')
    @patch('os.path.join')
    @patch('os.path.realpath')
    @patch('occam.objects.manager.Object')
    def test_return_object_and_workset_as_string(self,mock_objectManager,real_path,path_join,path_exists):
      real_path.return_value = "realPathMock"
      mock_objectManager.return_value = TestObjectManager.Obj()
      path_join.return_value = "identityPathMock"
      path_exists.side_effect = [True,False]
      self.objManager.uidFor = Mock(return_value="uidForMock")
      self.objManager.idFor = Mock(return_value="idForMock")
      self.objManager.infoFor = Mock(return_value={"type":"int"})
      self.objManager.localSearch = Mock(return_value = TestObjectManager.Obj())
      t = self.objManager.retrieveLocal("path")
      self.assertIsInstance(t[0], object)
      self.assertIsInstance(t[1], object)
      self.objManager.uidFor.assert_called_once()
      self.objManager.idFor.assert_called_once()
      self.objManager.infoFor.assert_called_once()
      self.objManager.localSearch.assert_called_once()

    @patch('os.path.exists')
    @patch('os.path.join')
    @patch('os.path.realpath')
    @patch('occam.objects.manager.Object')
    @patch('occam.person.Person.__init__')
    def test_return_object_and_workset_as_string_when_objectType_is_person(self,person,mock_objectManager,real_path,path_join,path_exists):
      person.return_value = None
      real_path.return_value = "realPathMock"
      mock_objectManager.return_value = TestObjectManager.Obj()
      path_join.return_value = "identityPathMock"
      path_exists.side_effect = [True,True]
      self.objManager.uidFor = Mock(return_value="uidForMock")
      self.objManager.idFor = Mock(return_value="idForMock")
      self.objManager.infoFor = Mock(return_value={"type":"person"})
      self.objManager.localSearch = Mock(return_value = TestObjectManager.Obj())
      t = self.objManager.retrieveLocal("path",person=TestObjectManager.Obj())
      self.assertIsInstance(t[0], object)
      self.assertIsInstance(t[1], object)
      self.objManager.uidFor.assert_called_once()
      self.objManager.idFor.assert_called_once()
      self.objManager.infoFor.assert_called_once()
      self.objManager.localSearch.assert_called_once()

    @patch('os.path.exists')
    @patch('os.path.join')
    @patch('os.path.realpath')
    @patch('occam.objects.manager.Object')
    @patch("builtins.open",create=True)
    def test_return_object_and_workset_as_string_exists_path_with_identityPath(self,p,mock_objectManager,real_path,path_join,path_exists):
      p.return_value = mock_open(read_data="Data1").return_value
      real_path.return_value = "realPathMock"
      mock_objectManager.return_value = TestObjectManager.Obj()
      path_join.return_value = "identityPathMock"
      path_exists.side_effect = [True,True]
      self.objManager.uidFor = Mock(return_value="uidForMock")
      self.objManager.idFor = Mock(return_value="idForMock")
      self.objManager.infoFor = Mock(return_value={"type":"int"})
      self.objManager.localSearch = Mock(return_value = TestObjectManager.Obj())
      t = self.objManager.retrieveLocal("path")
      self.assertIsInstance(t[0], object)
      self.assertIsInstance(t[1], object)
      self.objManager.uidFor.assert_called_once()
      self.objManager.idFor.assert_called_once()
      self.objManager.infoFor.assert_called_once()
      self.objManager.localSearch.assert_called_once()

  class TestLocalSearch(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_the_object_retrieved(self):
      self.objManager.retrieveLocal = Mock(return_value=(TestObjectManager.Obj(),"teste"))
      self.assertIsInstance(self.objManager.localSearch("startingPath"),object)
      self.objManager.retrieveLocal.assert_called_once()

    def test_should_return_the_object_retrieved_recursively(self):
      with patch('os.path.realpath') as real_path:
        real_path.return_value = "mockRealPath"
        self.objManager.retrieveLocal = Mock()
        self.objManager.retrieveLocal.side_effect = [(None,"test1"),(TestObjectManager.Obj(),"test2")]
        self.assertIsInstance(self.objManager.localSearch("startingPath"),object)

    def test_should_return_None_when_max_depth_less_than_1(self):
      with patch('os.path.realpath') as real_path:
        real_path.return_value = "mockRealPath"
        self.objManager.retrieveLocal = Mock()
        self.objManager.retrieveLocal.side_effect = [(None,"test1"),(None,"test2"),(None,"test3")]
        self.assertEqual(self.objManager.localSearch("startingPath"),None)

  class TestResolve(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_None_when_option_is_none_and_allowNone_is_False(self):
      self.assertEqual(self.objManager.resolve(None),None)

    def test_should_return_object_of_class_Object_when_option_is_None_and_allowNone_is_true(self):
      self.objManager.retrieveLocal = Mock(return_value=[TestObjectManager.Obj()])
      self.assertIsInstance(self.objManager.resolve(TestObjectManager.Obj(id=".")),object)
      self.objManager.retrieveLocal.assert_called_once()

    @patch('occam.object')
    def test_should_return_object_from_class_Object_when_option_id_contains_plus(self,Object):
      self.objManager.retrieveLocal = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.uidFor = Mock(return_value="uidForMock")
      self.objManager.retrieve = Mock(return_value = Object)
      self.assertIsInstance(self.objManager.resolve(TestObjectManager.Obj(id="+")),object)
      self.objManager.retrieveLocal.assert_called_once()
      self.objManager.uidFor.assert_called_once()
      self.objManager.retrieve.assert_called_once()

    @patch('occam.object')
    def test_should_return_object_from_class_Object_when_option_id_contains_plus_and_person_not_None(self,Object):
      self.objManager.retrieveLocal = Mock(return_value=[TestObjectManager.Obj()])
      self.objManager.retrieve = Mock(return_value = Object)
      self.objManager.idFor = Mock(return_value = "idForMock")
      self.assertIsInstance(self.objManager.resolve(TestObjectManager.Obj(id="+"),person=TestObjectManager.Obj()),object)
      self.objManager.retrieveLocal.assert_called_once()
      self.objManager.retrieve.assert_called_once()
      self.objManager.idFor.assert_called_once()

  class TestPathFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()
      self.uuid = ["pos1","pos2","pos3","pos4","pos5"]

    @patch('os.path.realpath')
    @patch('os.path.join')
    def test_should_return_string_containing_real_path(self,pathJoin,realpath):
      pathJoin.return_value = "pathJoinMock"
      realpath.return_value = "realPathMock"
      self.objManager.configuration = Mock(return_value="Mock")
      t = self.objManager.pathFor(self.uuid)
      self.assertIsInstance(t,str)
      self.objManager.configuration.get.assert_called_once()
      pathJoin.assert_called()
      realpath.assert_called()

    def test_should_return_None_when_object_path_is_None(self):
      with patch('os.path.join', return_value="mock"):
        self.objManager.configuration = Mock(return_value={"paths":None})
        self.objManager.configuration.get.return_value.get.return_value = None
        self.assertEqual(self.objManager.pathFor(self.uuid),None)
        self.objManager.configuration.get.assert_called_once()

  class TestTemporaryClone(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_None_when_object_is_None(self):
      t = self.objManager.temporaryClone(None)
      for i in t:
        self.assertEqual(i,None)

    def test_should_return_root_obj_and_path_to_root(self):
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      self.objManager.clone = Mock(return_value=TestObjectManager.Obj(path="path"))
      self.objManager.infoFor = Mock()
      self.objManager.infoFor.side_effect = [{"name":"root"},{"type":"int"}]
      t = self.objManager.temporaryClone(TestObjectManager.Obj(roots=[],root=TestObjectManager.Obj(roots=[])))
      self.assertIsInstance(t[0],object)
      self.assertIsInstance(t[1],object)
      self.assertIsInstance(t[2],str)
      self.objManager.clone.assert_called()
      self.objManager.infoFor.assert_called()

    def test_should_return_root_obj_and_None_when_obj_is_root(self):
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      self.objManager.clone = Mock(return_value=TestObjectManager.Obj(path="path"))
      self.objManager.links.retrieveLocalLinks = Mock(return_value=[TestObjectManager.Obj(link="link",path="path"),"MockingRetrieveLocalLinks"])
      self.objManager.infoFor = Mock()
      self.objManager.infoFor.side_effect = [{"name":"root"},{"type":"int"}]
      self.objManager.retrieveLocal = Mock(return_value = (TestObjectManager.Obj(),"retrieveLocalMock"))
      t = self.objManager.temporaryClone(TestObjectManager.Obj(roots=[],root=None,link=10))
      self.assertIsInstance(t[0],object)
      self.assertIsInstance(t[1],object)
      self.assertEqual(t[2],None)
      self.objManager.infoFor.assert_called()
      self.objManager.retrieveLocal.assert_called()

    @patch('os.path.exists')
    @patch('os.path.join')
    def test_should_return_root_obj_and_root_path_when_obj_is_root(self,pathJoin,path_exists):
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      path_exists.side_effect = [True]
      pathJoin.return_value = "mockPathJoin"
      self.objManager.clone = Mock(return_value=TestObjectManager.Obj(path="path",roots=["test"]))
      self.objManager.links.retrieveLocalLinks = Mock(return_value=[])
      self.objManager.infoFor = Mock()
      self.objManager.infoFor.side_effect = [{"name":"root"},{"type":"int"},{"contains":["test1","test2"]}]
      self.objManager.retrieveLocal = Mock(return_value = (TestObjectManager.Obj(),"retrieveLocalMock"))
      self.objManager.reset = Mock()
      t = self.objManager.temporaryClone(TestObjectManager.Obj(roots=["test"],root=TestObjectManager.Obj(),position=0,link=10))
      self.assertIsInstance(t[0],object)
      self.assertIsInstance(t[1],object)
      self.assertIsInstance(t[2],str)
      self.objManager.infoFor.assert_called()

    @patch('os.path.join')
    @patch('os.path.exists')
    def test_should_return_None_None_None_when_current_is_None(self,path_exists,pathJoin):
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      path_exists.side_effect = [False]
      pathJoin.return_value = "mockPathJoin"
      self.objManager.clone = Mock(return_value=TestObjectManager.Obj(path="path",roots=["test"]))
      self.objManager.links.retrieveLocalLinks = Mock(return_value=[])
      self.objManager.infoFor = Mock()
      self.objManager.infoFor.side_effect = [{"name":"root"},{"type":"int"},{"contains":[{"id":1,"revision":"1.5.2"},"test2"]}]
      self.objManager.retrieve = Mock(return_value=None)
      t = self.objManager.temporaryClone(TestObjectManager.Obj(roots=["test"],root=TestObjectManager.Obj(),position=0,link=10))
      self.assertEqual(t[0],None)
      self.assertEqual(t[1],None)
      self.assertEqual(t[2],None)
      self.objManager.infoFor.assert_called()
      self.objManager.clone.assert_called()
      self.objManager.links.retrieveLocalLinks.assert_called()
      self.objManager.retrieve.assert_called()

    @patch('os.path.join')
    @patch('os.path.exists')
    def test_should_return_None_None_None_when_current_could_not_be_cloned(self,path_exists,pathJoin):
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      path_exists.side_effect = [False]
      pathJoin.return_value = "mockPathJoin"
      self.objManager.clone = Mock()
      self.objManager.clone.side_effect = [TestObjectManager.Obj(path="path",roots=["test"]),False]
      self.objManager.links.retrieveLocalLinks = Mock(return_value=[])
      self.objManager.infoFor = Mock()
      self.objManager.infoFor.side_effect = [{"name":"root"},{"type":"int"},{"contains":[{"id":1,"revision":"1.5.2"},"test2"]}]
      self.objManager.retrieve = Mock(return_value="mockRetrieve")
      t = self.objManager.temporaryClone(TestObjectManager.Obj(roots=["test"],root=TestObjectManager.Obj(),position=0,link=10))
      self.assertEqual(t[0],None)
      self.assertEqual(t[1],None)
      self.assertEqual(t[2],None)
      self.objManager.infoFor.assert_called()
      self.objManager.clone.assert_called()
      self.objManager.links.retrieveLocalLinks.assert_called()
      self.objManager.retrieve.assert_called()

  class TestParentFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_None_when_roots_len_is_equal_0(self):
      self.assertEqual(self.objManager.parentFor(TestObjectManager.Obj(roots=[])),None)

    def test_should_return_last_position_of_roots(self):
      self.assertEqual(self.objManager.parentFor(TestObjectManager.Obj(roots="test")),"t")

    def test_should_return_object_when_objectInfo_does_not_have_belongsTo_and_path_exists_to_object(self):
      with patch('os.path.exists',return_value=True):
        self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
        self.objManager.infoFor = Mock(return_value="infoForMock")
        self.assertIsInstance(self.objManager.parentFor(TestObjectManager.Obj(roots=None,path="path")),object)
        self.objManager.infoFor.assert_called()

    def test_should_return_object_when_parent_object_belongsToUUID(self):
      with patch('os.path.exists',return_value=True):
        self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
        self.objManager.infoFor = Mock()
        self.objManager.infoFor.side_effect = [{"belongsTo":{"id": "foo", "uid": "bar"}},{"id":None}]
        self.assertIsInstance(self.objManager.parentFor(TestObjectManager.Obj(roots=None,path="path")),object)
        self.objManager.infoFor.assert_called()

    def test_should_return_None_when_datastore_dbrows_is_None(self):
      with patch('os.path.exists',return_value=True):
        self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
        self.objManager.infoFor = Mock()
        self.objManager.infoFor.side_effect = [{"belongsTo":{"id": "foo", "uid": "bar"}},{"id":None}]
        self.objManager.datastore.retrieveObjects = Mock(return_value=None)
        self.assertEqual(self.objManager.parentFor(TestObjectManager.Obj(roots=None,path="path")),None)
        self.objManager.infoFor.assert_called()

    def test_should_return_object_when_retrieve_returns_object(self):
      with patch('os.path.exists',return_value=True):
        with patch('occam.objects.manager.ObjectManager') as Object:
          self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
          self.objManager.infoFor = Mock()
          self.objManager.infoFor.side_effect = [{"belongsTo":{"id": "foo", "uid": "bar"}},{"id":None}]
          self.objManager.datastore.retrieveObjects = Mock(return_value=[TestObjectManager.Obj(),"object2"])
          self.objManager.retrieve = Mock(return_value=Object)
          self.assertIsInstance(self.objManager.parentFor(TestObjectManager.Obj(roots=None,path="path",object_type="object")),object)
          self.objManager.infoFor.assert_called()
          self.objManager.datastore.retrieveObjects.assert_called()
          self.objManager.retrieve.assert_called()

    def test_should_return_None_when_objectInfo_does_not_have_belongsTo_and_path_does_not_exists_to_object(self):
      with patch('os.path.exists',return_value=False):
        self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
        self.objManager.infoFor = Mock(return_value="infoForMock")
        self.assertEqual(self.objManager.parentFor(TestObjectManager.Obj(roots=None,path="path")),None)
        self.objManager.infoFor.assert_called()

  class TestRetrieveHistory(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_list_of_entries_for_each_revision_from_the_current_revision_when_path_is_None(self):
      with patch('occam.objects.manager.StorageManager.retrieveHistory') as storeManager:
        storeManager.return_value = []
        self.assertIsInstance(self.objManager.retrieveHistoryFor(TestObjectManager.Obj()),list)

    def test_should_return_list_of_entries_for_each_revision_from_the_current_revision_when_path_not_None(self):
      with patch('occam.objects.manager.GitRepository.history') as gitRepo:
        gitRepo.return_value = []
        t = self.objManager.retrieveHistoryFor(TestObjectManager.Obj(path="path"))
        self.assertIsInstance(t,list)

    @patch('occam.objects.manager.StorageManager.retrieveHistory')
    @patch('occam.objects.manager.GitRepository')
    def test_should_return_list_of_entries_for_each_revision_from_the_current_revision_when_pot_None(self,mock_gitRepo,storeManager):
      mock_gitRepo.return_value.history.side_effect = IOError
      storeManager.return_value = []
      self.assertIsInstance(self.objManager.retrieveHistoryFor(TestObjectManager.Obj(path="path")),list)

  class TestRetrieveDirectoryWithResourcesFrom(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_empty_array_when_id_and_uid_not_in_resource(self):
      with patch('occam.person.Person') as person:
        self.objManager.infoFor = Mock(return_value={"install":["g++","cmake"],"run":{"install":["gcc","vim"]}})
        self.objManager.resources = Mock()
        self.objManager.resources.infoFor.return_value = None
        self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[])
        self.assertEqual(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}],"path",person),[])

    @patch('occam.person.Person')
    def test_should_return_empty_array_when_id_in_resource_and_resourceInfo_is_None(self, Person):
      person = Person()
      person.id = "person_id"
      self.objManager.infoFor = Mock(return_value={"install":[{"id":123},"cmake"],"run":{"install":["gcc","vim"]}})
      self.objManager.resources = Mock()
      self.objManager.resources.infoFor.return_value = None
      self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[])
      self.assertEqual(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}],"path",person),[])

    @patch('occam.person.Person')
    def test_should_return_empty_array_when_id_in_resource_resourceInfo_contains_data_there_is_no_path(self, Person):
      person = Person()
      person.id = "person_id"
      self.objManager.infoFor = Mock(return_value={"install":[{"id":123,"revision":123},"cmake"],"run":{"install":["gcc","vim"]}})
      self.objManager.resources = Mock()
      self.objManager.resources.infoFor.return_value = {"revision":1, "subtype": ["foo"]}
      self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[])
      self.assertEqual(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}],"path",person),[])

    @patch('occam.person.Person')
    @patch('os.path.basename')
    def test_should_return_array_with_items(self, basename, Person):
      person = Person()
      person.id = "person_id"
      basename.side_effect = ["baseNameMock"]
      self.objManager.infoFor = Mock(return_value={"install":[{"id":123,"revision":123},"cmake"],"run":{"install":["gcc","vim"]}})
      self.objManager.resources = Mock()
      self.objManager.resources.infoFor.return_value = {"revision": 1, "subtype": ["subtype"]}
      self.objManager.resources.retrieveFileStat.return_value = {"name":"name"}
      self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[{"items": [{"name": "foo"}]}])
      self.assertIsInstance(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}], "/", person), list)

    @patch('occam.person.Person')
    @patch('os.path.basename')
    def test_should_return_array_with_unpackPath_and_directory(self, basename, Person):
      person = Person()
      person.id = "person_id"
      basename.side_effect = ["baseNameMock"]
      self.objManager.infoFor = Mock(return_value={"section":{"first":"section","install":["installThis"]},"build":{"install":["installThis"]},"install":[{"id":123,"revision":123,"actions":{"unpack":"/"}},"cmake"],"run":{"install":["gcc","vim"]}})
      self.objManager.resources = Mock()
      self.objManager.resources.infoFor.return_value = {"revision":1,"subtype":"subtype"}
      self.objManager.resources.retrieveFileStat.return_value = {"name":"name","type":"tree"}
      self.objManager.resources.retrieveDirectoryFrom.return_value = {"items":[{"something":{"this":"that"}}],"from":"somewhere"}
      self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[{"items": [{"name": "foo"}]}])
      self.assertIsInstance(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}],"/package/test",person),list)

    @patch('occam.person.Person')
    @patch('os.path.basename')
    def test_should_return_array_with_unpackPath_and_items_empty(self,basename,person):
      basename.side_effect = ["baseNameMock"]
      self.objManager.infoFor = Mock(return_value={"install":[{"id":123,"revision":123,"actions":{"unpack":"/"}},"cmake"],"run":{"install":["gcc","vim"]}})
      self.objManager.resources = Mock()
      self.objManager.resources.infoFor.return_value = {"revision":1,"subtype":"subtype"}
      self.objManager.resources.retrieveFileStat.return_value = {"name":"name","type":"tree"}
      self.objManager.resources.retrieveDirectoryFrom.side_effect = IOError
      self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[])
      self.assertEqual(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}],"path",person),[])
      self.assertIsInstance(self.objManager.retrieveDirectoryWithResourcesFrom([{"id": "foo", "uid": "bar", "subtype": ["foo"]}],"/p/test",person),list)

  class TestRetrieveDirectoryFrom(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_dict_containing_items_and_mime(self):
      self.objManager.resources = Mock()
      self.objManager.resources.retrieveRelativeDirectory = Mock(return_value=[])
      self.objManager.storage = Mock()
      self.objManager.storage.mimeTypeFor = Mock(return_value = "text/plain")
      self.objManager.storage.retrieveDirectory = Mock(return_value={"items":[{"name":{"test":"test"}}]})
      self.objManager.retrieveFileFrom = Mock(return_value="mockRetrieve".encode('utf-8'))
      self.objManager.retrieveDirectoryWithResourcesFrom = Mock(return_value=[["somethiong",{"items":[]}]])
      self.assertIsInstance(self.objManager.retrieveDirectoryFrom(TestObjectManager.Obj(),"path",includeResources=True),dict)

    @patch('occam.objects.manager.GitRepository')
    def test_should_return_dict_containing_item(self,git_repository):
      with self.assertRaises(IOError) as cm:
        git_repository.return_value.retrieveDirectory.side_effect = IOError
        self.objManager.storage.retrieveDirectory = Mock(return_value={"items":[{"name":{"test":"test"}}]})
        t = self.objManager.retrieveDirectoryFrom(TestObjectManager.Obj(path="path"),"path")
        self.assertEqual(t.exception.error_code,"File not found")

  class TestRetrieveFileStatFrom(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('occam.objects.manager.GitRepository')
    def test_should_return_dict_when_ret_is_None_and_objpath_not_None(self,git_repo):
      git_repo.return_value.retrieveFileStat.side_effect = IOError
      self.objManager.ownerFor = Mock(return_value=TestObjectManager.Obj())
      self.objManager.storage = Mock()
      self.objManager.storage.retrieveFileStat.return_value = {"name":"name","type":"tree"}
      self.assertIsInstance(self.objManager.retrieveFileStatFrom(TestObjectManager.Obj(path="path"),"path"),dict)

    def test_should_return_dict_when_resource_false_and_path_not_slash(self):
      self.objManager.ownerFor = Mock(return_value=TestObjectManager.Obj())
      self.objManager.storage = Mock()
      self.objManager.storage.retrieveFileStat.return_value = {"name":"name","type":"tree"}
      self.assertIsInstance(self.objManager.retrieveFileStatFrom(TestObjectManager.Obj(),"path"),dict)

    def test_should_return_dict_when_resources_true_and_path_not_slash(self):
      with patch('os.path.join', return_value="relativePathMock"):
        self.objManager.ownerFor = Mock(return_value=TestObjectManager.Obj())
        self.objManager.retrieveDirectoryWithResourcesFrom = Mock(return_value=[[{"name":"path","id":"id","uid":"uid","revision":"revision","subtype":"subtype"},{"items":[{"name":"path","id":"id","uid":"uid","revision":"revision","subtype":"subtype"}]},3]])
        self.objManager.retrieveFileFrom = Mock(return_value = b'MockRetrieveFile')
        self.objManager.storage = Mock()
        self.objManager.storage.mimeTypeFor = Mock(return_value = "text/plain")
        self.objManager.resources = Mock()
        self.objManager.resources.retrieveFileStatFrom.return_value = {"name": "name", "type": "tree"}
        self.objManager.mimeTypeFor = Mock(return_value={"mime": True})
        self.assertIsInstance(self.objManager.retrieveFileStatFrom(TestObjectManager.Obj(),"path",includeResources=True),dict)

    def test_should_return_dict_when_resources_true_and_internalpath_isNone(self):
      with patch('os.path.join', return_value="relativePathMock"):
        self.objManager.ownerFor = Mock(return_value=TestObjectManager.Obj())
        self.objManager.retrieveDirectoryWithResourcesFrom = Mock(return_value=[[{"name":"path","id":"id","uid":"uid","revision":"revision","subtype":"subtype"},{"items":[{"name":"path","id":"id","uid":"uid","revision":"revision","subtype":"subtype"}]},None]])
        self.objManager.retrieveFileFrom = Mock(return_value = b'MockRetrieveFile')
        self.objManager.storage = Mock()
        self.objManager.storage.mimeTypeFor = Mock(return_value = "text/plain")
        self.objManager.resources = Mock()
        self.objManager.resources.retrieveFileStat.return_value = {"name":"name","type":"tree"}
        self.objManager.mimeTypeFor = Mock(return_value={"mime":True})
        self.assertIsInstance(self.objManager.retrieveFileStatFrom(TestObjectManager.Obj(),"path",includeResources=True),dict)

    @patch('occam.objects.manager.GitRepository')
    def test_should_return_dict_when_resource_false_and_obj_path_not_None_and_GitRepo_fails(self,git_repo):
      self.objManager.ownerFor = Mock(return_value=TestObjectManager.Obj())
      git_repo.return_value.retrieveFileStat.return_value = None
      self.objManager.storage = Mock()
      self.objManager.storage.retrieveFileStat.return_value = {"name": "name", "type": "tree"}
      self.objManager.retrieveFileStatFrom(TestObjectManager.Obj(path="path"), "/")

  class TestRetrieveFileFrom(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_stream_when_resource_and_path_and_not_fromObject(self):
      self.objManager.resources = Mock()
      self.objManager.resources.retrieveFileFrom.return_value = b"stream"
      self.assertIsInstance(self.objManager.retrieveFileFrom(TestObjectManager.Obj(),"path"), bytes)

    def test_should_return_stream_when_includeResources_False_object_path_None(self):
      self.objManager.storage = Mock()
      self.objManager.storage.retrieveFile.return_value = b'MockRetrieveFile'
      self.assertIsInstance(self.objManager.retrieveFileFrom(TestObjectManager.Obj(),path=None), bytes)

    def test_should_return_stream_when_includeResources_True_resource_None(self):
      self.objManager.retrieveDirectoryWithResourcesFrom = Mock(return_value = [[{"id":123,"uid":123,"revision":"revision","subtype":"subtype"},{"items":[{"name":"path"}]},"internalPath"]])
      self.objManager.resources = Mock()
      self.objManager.resources.retrieveFileFrom.return_value = b'mockRetrieveFile'
      self.assertIsInstance(self.objManager.retrieveFileFrom(TestObjectManager.Obj(resource=None),path="path",includeResources=True), bytes)

    def test_should_return_stream_when_includeResources_True_resource_None_internalPath_None(self):
      self.objManager.retrieveDirectoryWithResourcesFrom = Mock(return_value = [[{"id":123,"uid":123,"revision":"revision","subtype":"subtype"},{"items":[{"name":"path"}]},None]])
      self.objManager.resources = Mock()
      self.objManager.resources.retrieveFile.return_value = b'MockRetrieveFile'
      self.assertIsInstance(self.objManager.retrieveFileFrom(TestObjectManager.Obj(resource=None),path="path",includeResources=True), bytes)

    def test_should_return_stream_when_fromObject_True(self):
      with patch('occam.objects.manager.GitRepository.retrieveFile', return_value = b"mock"):
        self.assertIsInstance(self.objManager.retrieveFileFrom(TestObjectManager.Obj(resource="resource",path="path"),path=None,fromObject=True), bytes)

    @patch('occam.objects.manager.GitRepository')
    def test_should_return_stream_when_includeResources_True_and_try_raises_exception(self,git_repo):
      git_repo.return_value.retrieveFile.side_effect = IOError
      self.objManager.storage = Mock()
      self.objManager.storage.retrieveFile.return_value = b'MockRetrieveFile'
      self.assertIsInstance(self.objManager.retrieveFileFrom(TestObjectManager.Obj(resource="resource",path="path"),path=None,fromObject=True), bytes)

  class TestRetrieveJSONFrom(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_list_containing_contents_from_json(self):
      self.objManager.cache.get = Mock(return_value = '["foo", {"bar":["baz", null, 1.0, 2]}]'.encode('utf-8').strip())
      self.assertIsInstance(self.objManager.retrieveJSONFrom(TestObjectManager.Obj(path="path",position=5),"path"),list)
      self.objManager.cache.get.assert_called()

    def test_should_return_list_containing_contents_from_json_when_cache_get_returns_None(self):
      stream = io.BytesIO('["foo", {"bar":["baz", null, 1.0, 2]}]'.encode('utf-8').strip())
      self.objManager.cache.get = Mock(return_value = None)
      self.objManager.retrieveFileFrom = Mock(return_value = stream)
      self.objManager.cache.set = Mock()
      self.assertIsInstance(self.objManager.retrieveJSONFrom(TestObjectManager.Obj(path="path",position=5),"path"),list)
      self.objManager.cache.get.assert_called()
      self.objManager.retrieveFileFrom.assert_called()
      self.objManager.cache.set.assert_called()

  class TestOwnerFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    @patch('occam.person.Person')
    def test_should_return_object_when_id_not_None(self,person):
      self.objManager.retrieve = Mock(return_value = TestObjectManager.Obj())
      self.assertIsInstance(self.objManager.ownerFor(TestObjectManager.Obj(),person),object)
      self.objManager.retrieve.assert_called()

    @patch('occam.person.Person')
    def test_should_return_object_when_id_None(self,person):
      self.assertIsInstance(self.objManager.ownerFor(TestObjectManager.Obj(owner_id=None),person),object)

  class TestOwnerInfoFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_dictionary_containing_info_from_the_owner_(self):
      with patch('occam.objects.manager.ObjectInfo', return_value= {"includes":[{"id":0,"uid":0}]}):
        self.objManager.retrieveJSONFrom = Mock(return_value = ["foo", "bar"])
        self.objManager.idFor = Mock(return_value = 1)
        self.objManager.uidFor = Mock(return_value = 2)
        self.assertIsInstance(self.objManager.ownerInfoFor(TestObjectManager.Obj(ownerInfo=None)),dict)
        self.objManager.retrieveJSONFrom.assert_called()
        self.objManager.idFor.assert_called()
        self.objManager.uidFor.assert_called()

    @patch('json.decoder.JSONDecodeError',new_callable=lambda:TestObjectManager.t)
    def test_should_raise_exception(self,jsonError):
      import json
      with self.assertRaises(ObjectJSONError) as cm:
        r = ObjectJSONError(TestObjectManager.t())
        self.objManager.retrieveJSONFrom = Mock()
        self.objManager.retrieveJSONFrom.side_effect = jsonError
        t = self.objManager.ownerInfoFor(TestObjectManager.Obj(ownerInfo=None))
        self.assertIsInstance(t.exception.error_code,"mockException")
        self.objManager.retrieveJSONFrom.assert_called_once()

  class TestObjectPositionWithin(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_position_of_given_subOject(self):
      self.objManager.infoFor = Mock(return_value = {"contains":[{"id":1}]})
      self.assertEqual(self.objManager.objectPositionWithin(TestObjectManager.Obj(id=1),TestObjectManager.Obj(id=1)),0)

    def test_should_return_None(self):
      self.objManager.infoFor = Mock(return_value = {"contains":[{"id":1}]})
      self.assertEqual(self.objManager.objectPositionWithin(TestObjectManager.Obj(id=1),TestObjectManager.Obj(id=3)),None)

  class TestInfoFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_dict_containing_info_of_the_object(self):
      self.objManager.idFor = Mock()
      self.objManager.idFor.side_effect = ["id","objectID","newObjectID"]
      self.objManager.uidFor = Mock(return_value = "newUID")
      t = self.objManager.infoFor(TestObjectManager.Obj(id="objectID",info=
        {
          "includes":[{"type":"int","name":"nameObj"}],
          "subtype":"something",
          "inputs":["1"],
          "outputs":["2"],
          "build": {"tex":"1.4.4"},
          "run": {"cmake":"1.5.8"},
          "configurations": "high-level",
          "include":["g++"]
        }))
      self.objManager.idFor.assert_called()
      self.objManager.uidFor.assert_called()
      self.assertIsInstance(t,dict)

    def test_should_return_None_when_obj_is_None(self):
      self.assertEqual(self.objManager.infoFor(None),None)

    def test_should_return_None_when_obj_info_is_None(self):
      self.objManager.ownerInfoFor = Mock(return_value = None)
      self.assertEqual(self.objManager.infoFor(TestObjectManager.Obj(info=None)),None)

    def test_should_return_obj_info_when_revision_equals_infoRevision(self):
      self.objManager.ownerInfoFor = Mock(return_value = None)
      self.assertEqual(self.objManager.infoFor(TestObjectManager.Obj(infoRevision="revision")),TestObjectManager.Obj().info)

  class TestUidFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_string_containg_the_reference_uid(self):
      self.objManager.uidTokenFor = Mock(return_value = "mockToken")
      self.assertIsInstance(self.objManager.uidFor(TestObjectManager.Obj()),str)
      self.objManager.uidTokenFor.assert_called()

  class TestIdTokenFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_the_identifier_for_the_given_object_and_identity(self):
      self.objManager.uidTokenFor = Mock(return_value = str.encode("mockUidTokenFor"))
      self.objManager.uidFor = Mock(return_value = "uidForMock")
      self.assertIsInstance(self.objManager.idTokenFor(TestObjectManager.Obj(), "identity"), bytes)
      self.objManager.uidTokenFor.assert_called()
      self.objManager.uidFor.assert_called()

  class TestIdFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.objManager = ObjectManager()

    def test_should_return_the_identifier_for_the_given_object_and_identity(self):
      self.objManager.idTokenFor = Mock(return_value = "mockIdTokenFor")
      self.assertIsInstance(self.objManager.idFor(TestObjectManager.Obj(), "identity"), str)

  class TestObjectJSONError(unittest.TestCase):
    def test_should_return_string(self):
      objJSONe = ObjectJSONError(TestObjectManager.t())
      x = objJSONe.explanation()
      self.assertIsInstance(x, str)

    def test_should_return_string2(self):
      import re
      objJSONe = ObjectJSONError(TestObjectManager.t(pos=5))
      x = objJSONe.explanation()
      self.assertIsInstance(x, str)

    def test_should_return_string3(self):
      import re
      objJSONe = ObjectJSONError(TestObjectManager.t(pos=5, lineno=2))
      x = objJSONe.explanation()
      self.assertIsInstance(x, str)

    def test_should_return_string3(self):
      import re
      objJSONe = ObjectJSONError(TestObjectManager.t(pos=5, lineno=1, msg="Expecting property name enclosed in double quotes", doc="jsonD}ecodeError.doc"))
      x = objJSONe.explanation()
      self.assertIsInstance(x, str)

    def test_should_return_string4(self):
      import re
      objJSONe = ObjectJSONError(TestObjectManager.t(pos=5, lineno=1, msg="Expecting property name enclosed in double quotes", doc="jsonD]ecodeError.doc"))
      x = objJSONe.explanation()
      self.assertIsInstance(x, str)

    def test_should_return_string5(self):
      objJSONe = ObjectJSONError(TestObjectManager.t(pos=5, lineno=2))
      x = objJSONe.__str__()
      self.assertIsInstance(x, str)

    def test_should_return_string6(self):
      import re
      email = "tony@tiremove_thisger.net"
      m = re.search("remove_this", email)
      self.re = Mock()
      self.re.return_value.search.return_value = None
      objJSONe = ObjectJSONError(TestObjectManager.t(pos=5, lineno=2, msg="Expecting property name enclosed in double quotes", doc="jsonD]ecodeError.doc\nsomething\nanother"))
      x = objJSONe.explanation()
      self.assertIsInstance(x, str)

  class TestGetObjectIdentifierRegex(unittest.TestCase):

    regex = ObjectManager.objectIdentifierRegex()


    def test_should_identify_object_identifier_uuid(self):
      obj_id, identifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier()
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in identifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_repository_uuid(self):
      obj_id, identifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({"uuid": "+"})
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in identifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_localdir_uuid(self):
      obj_id, identifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({"uuid": "."})
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in identifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision(self):
      obj_id, identifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in identifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_version(self):
      obj_id, identifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "version": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in identifier.items():
          self.assertEqual(result.get(k), v)
