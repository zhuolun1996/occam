from unittest.mock import patch, Mock, MagicMock, PropertyMock

from occam.object import Object
from occam.objects.manager import ObjectManager

from tests.helper import multihash

class ObjectMock(Mock):
  def __init__(self, info, *args, **kwargs):
    super(Mock, self).__init__(*args, spec_set=Object, **kwargs)
    self.info = info
    self.id = info.get('id')
    self.uid = info.get('uid')
    self.revision = info.get('revision')
    self.infoRevision = self.revision

class ObjectManagerMock(Mock):
  def __init__(self, objs=[], *args, **kwargs):
    # Format info
    for obj in objs:
      objectInfo = obj
      if isinstance(obj, list) or isinstance(obj, tuple):
        objectInfo = obj[0]

      if not 'id' in objectInfo:
        objectInfo['id'] = multihash()

      if not 'uid' in objectInfo:
        objectInfo['uid'] = multihash()

      if not 'revision' in objectInfo:
        objectInfo['revision'] = multihash()

    # Mock each object
    objectList = list(map(lambda obj:
                            ObjectMock(obj) if not isinstance(obj, list) and not isinstance(obj, tuple) else ObjectMock(obj[0]),
                          objs))
    fileList = list(map(lambda obj:
                          {} if not isinstance(obj, list) and not isinstance(obj, tuple) or len(obj) < 2 else obj[1],
                        objs))

    super(Mock, self).__init__(*args, spec_set=ObjectManager, **kwargs)
    def retrieve(id, *args, **kwargs):
      for obj in objectList:
        if obj.id == id:
          return obj
      return None

    self.retrieve = MagicMock(side_effect=retrieve)

    def resolve(namespace, *args, **kwargs):
      for obj in objectList:
        if obj.id == namespace.id:
          return obj
      return None

    self.resolve = MagicMock(side_effect=resolve)

    def infoFor(o):
      for obj in objectList:
        if obj is o:
          return o.info

      return {}

    self.infoFor = MagicMock(side_effect=infoFor)

    def retrieveFileFrom(o, path, *args, **kwargs):
      for i, obj in enumerate(objectList):
        if obj is o:
          for k, v in fileList[i].items():
            if k == path:
              return v.encode('utf8')

      raise IOError("File Not Found")

    self.retrieveFileFrom = MagicMock(side_effect=retrieveFileFrom)

    def retrieveFileStatFrom(o, path, *args, **kwargs):
      for i, obj in enumerate(objectList):
        if obj is o:
          for k, v in fileList[i].items():
            if k == path:
              return {
                "type": "file",
                "name": k,
                "size": len(v)
              }

      raise IOError("File Not Found")

    self.retrieveFileStatFrom = MagicMock(side_effect=retrieveFileStatFrom)

    def retrieveJSONFrom(o, path, *args, **kwargs):
      import json
      for i, obj in enumerate(objectList):
        if obj is o:
          for k, v in fileList[i].items():
            if k == path:
              return json.loads(v)

      raise IOError("File Not Found")

    self.retrieveJSONFrom = MagicMock(side_effect=retrieveJSONFrom)

    def ownerInfoFor(o):
      for obj in objectList:
        if obj is o:
          return o.info

      return {}

    self.ownerInfoFor = MagicMock(side_effect=ownerInfoFor)
