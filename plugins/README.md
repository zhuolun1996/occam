# Occam Plugins

Just add directories here of Occam plugins and components you wish to add to your Occam instance.

These will automatically be placed in your PYTHONPATH, effectively, and added to the global module path.

You still need to place them in your `config.yml` under the components section. For instance, for `occam-social`, you will place occam.social in that list.

Once this is done, this component will be visible to the Occam system and parsed and used to supply new commands and features and component plugins.
